package be.dkv.fw.test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

import javassist.util.proxy.MethodFilter;
import javassist.util.proxy.MethodHandler;
import javassist.util.proxy.ProxyFactory;
import be.dkv.fw.exception.runtime.TechnicalException;

/**
 * @author mahieus
 *
 */
public class TestResourceUtil {

    private static boolean testClassloaderLoaded = false;
    /**
     * Enables re-allocating resources for test-only purposes.
     * @param mapping
     */
    public static void enableTestResourceLoader(final Map<String, String> mapping) {
        if (!testClassloaderLoaded) {
            final Thread currentThread = Thread.currentThread();
            final ClassLoader saveClassLoader = currentThread.getContextClassLoader();

            ProxyFactory factory = new ProxyFactory();
            factory.setSuperclass(ClassLoader.class);
            factory.setFilter(
                    new MethodFilter() {
                        @Override
                        public boolean isHandled(Method method) {
                            return "getResources".equals(method.getName()) && method.getGenericParameterTypes().length == 1 && method.getGenericParameterTypes()[0].equals(String.class);
                        }
                    }
                );

            MethodHandler handler = new MethodHandler() {
                @Override
                public Object invoke(Object proxy, Method method, Method proceed, Object[] args) throws Throwable {
                    String usedResourceName = (String)args[0];
                    String redirect = mapping.get(usedResourceName);
                    if (redirect != null) {
                        usedResourceName = redirect;
                    }
                    return method.invoke(saveClassLoader, usedResourceName);
                }
            };
            try {
                currentThread.setContextClassLoader((ClassLoader) factory.create(new Class<?>[0], new Object[0], handler));
            } catch (IllegalArgumentException e) {
                throw new TechnicalException(e);
            } catch (NoSuchMethodException e) {
                throw new TechnicalException(e);
            } catch (InstantiationException e) {
                throw new TechnicalException(e);
            } catch (IllegalAccessException e) {
                throw new TechnicalException(e);
            } catch (InvocationTargetException e) {
                throw new TechnicalException(e);
            }
            testClassloaderLoaded = true;
        }
    }

}
