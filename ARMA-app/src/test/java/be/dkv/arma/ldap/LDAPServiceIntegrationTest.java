package be.dkv.arma.ldap;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import be.dkv.arma.exception.GroupNotFoundException;
import be.dkv.arma.model.GroupDTO;
import be.dkv.arma.model.UserDTO;
import be.dkv.arma.settings.SettingManager;
import be.dkv.arma.test.fw.ArmaTestBeanFactory;
import be.dkv.fw.util.InjectionUtil;

/**
 *
 *
 * @author vanhout
 */
public class LDAPServiceIntegrationTest {

    private static final String ENVIRONMENT = "TEST";

    /**
     * The Constant USER1. TODO: Use a system loginName when ready instead of an
     * employee.
     */
    private static final String USER1 = "dobbelae";

    /**
     * The Constant USER2. TODO: Use a system loginName when ready instead of an
     * employee.
     */
    private static final String USER2 = "pierreux";
    private static final String GROUP1 = "dl4.user";
    private static final String GROUP2 = "evv.admin";
    private static final String GROUP3 = "evv.claims.officer";
    private static final String UNKNOWN = "unknown";

    private static final boolean DO_LOGGING = true;

    private LDAPService ldapService;

    /**
     * @throws IOException
     *             An exception
     */
    // @Before
    public void setUp() throws IOException {
        ldapService = new LDAPService();
        LDAPDelegate ldapDelegate = new LDAPDelegate();

        Properties ldapProperties = new Properties();
        ldapProperties.load(LDAPService.class.getResourceAsStream("/ARMA/ldap.properties"));

        Properties armaProperties = new Properties();
        armaProperties.load(LDAPService.class.getResourceAsStream("/ARMA/arma.properties"));

        InjectionUtil.inject(ldapDelegate, ldapProperties);
        InjectionUtil.inject(ldapService, armaProperties);
        InjectionUtil.inject(ldapService, ldapDelegate);
        InjectionUtil.inject(ldapService, ArmaTestBeanFactory.getSettingManager());
    }

    /**
     * @throws Exception
     *             An exception
     */
    // @Test
    public void testAllUsers() throws Exception {
        List<String> users = ldapService.findAllUserNames();
        Assert.assertNotNull(users);
        Assert.assertTrue(!users.isEmpty());
        if (DO_LOGGING) {
            System.out.println("number of users: " + users.size());
            System.out.println("all users: " + users.toString());
        }
    }

    /**
     * @throws Exception
     *             An exception
     */
    // @Test
    public void testFindUserByLogin1() throws Exception {
        UserDTO userDTO = ldapService.findUserByLogin(USER1);
        Assert.assertNotNull(userDTO);
        Assert.assertNotNull(userDTO.getLoginName());
        Assert.assertNotNull(userDTO.getUserName());
        Assert.assertNotNull(userDTO.getManagerUserNumber());
        Assert.assertNotNull(userDTO.getGroups());
        Assert.assertTrue(!userDTO.getGroups().isEmpty());
        GroupDTO group = userDTO.getGroups().get(0);
        Assert.assertNotNull(group.getId());
        if (DO_LOGGING) {
            System.out.println("user by login " + USER1 + ": " + userDTO);
        }
    }

    /**
     * @throws Exception
     *             An exception
     */
    // @Test
    public void testFindUserByLoginCase2() throws Exception {
        UserDTO userDTO = ldapService.findUserByLogin(USER2);
        Assert.assertNotNull(userDTO);
        Assert.assertNotNull(userDTO.getLoginName());
        Assert.assertNotNull(userDTO.getUserName());
        if (DO_LOGGING) {
            System.out.println("user by login " + USER2 + ": " + userDTO);
        }
    }

    /**
     * @throws Exception
     *             An exception
     */
    // @Test
    public void testUsersByGroup1() throws Exception {
        List<UserDTO> users = ldapService.findUsersInGroup(GROUP1);
        Assert.assertNotNull(users);
        Assert.assertTrue(!users.isEmpty());
        if (DO_LOGGING) {
            System.out.println("number of users in " + GROUP1 + ": " + users.size());
            System.out.println("all users in " + GROUP1 + ": " + users.toString());
        }
    }

    /**
     * @throws Exception
     *             An exception
     */
    // @Test
    public void testUsersByGroupCase2() throws Exception {
        List<UserDTO> users = ldapService.findUsersInGroup(GROUP2);
        Assert.assertNotNull(users);
        Assert.assertTrue(!users.isEmpty());
        if (DO_LOGGING) {
            System.out.println("number of users in " + GROUP2 + ": " + users.size());
            System.out.println("all users in " + GROUP2 + ": " + users.toString());
        }
    }

    /**
     * @throws Exception
     *             An exception
     */
    // @Test
    public void testUsersByGroupCase3() throws Exception {
        List<UserDTO> users = ldapService.findUsersInGroup(GROUP3);
        Assert.assertNotNull(users);
        Assert.assertTrue(!users.isEmpty());
        if (DO_LOGGING) {
            System.out.println("number of users in " + GROUP3 + ": " + users.size());
            System.out.println("all users in " + GROUP3 + ": " + users.toString());
        }
    }

    /**
     * @throws Exception
     *             An exception
     */
    // @Test(expected = GroupNotFoundException.class)
    public void testUsersByGroupCaseNotFound() throws Exception {
        ldapService.findUsersInGroup(UNKNOWN);
    }

    /**
     * Unknown Users should be returned as null values.
     */
    // @Test
    public void testFindUserByLoginNotFound() throws Exception {
        Assert.assertNull(ldapService.findUserByLogin(UNKNOWN));
    }
}
