package be.dkv.arma.migration;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import be.dkv.arma.migration.model.UserGroupMigrationDTO;

public class UserGroupMigrationDTOTest {

    @Test
    public void defaultMigrationActionTest() {
        UserGroupMigrationDTO migDataEntry = new UserGroupMigrationDTO();
        migDataEntry.setUserName("TEST");
        assertFalse(migDataEntry.needsMigrationActions());
    }

}
