package be.dkv.arma.ws;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import be.dkv.arma.common.ARMAErrorCodes;
import be.dkv.arma.exception.GroupNotFoundException;
import be.dkv.arma.exception.UserNotFoundException;
import be.dkv.arma.model.IdRequest;
import be.dkv.arma.model.UserDTO;
import be.dkv.arma.model.VoidRequest;
import be.dkv.arma.result.UserListResult;
import be.dkv.arma.result.UserResult;
import be.dkv.arma.service.impl.UserServiceBean;
import be.dkv.fw.exception.DKVBusinessException;
import be.dkv.fw.util.InjectionUtil;

/**
 * The Class ArmaCoreWebServiceTest.
 * 
 * @author vanhout
 */
public class ArmaCoreWebServiceTest {

    /** The Constant ID_NONE. */
    private static final String ID_NONE = "NONE";

    /** The Constant ID_REQUEST_NONE. */
    private static final IdRequest ID_REQUEST_NONE = new IdRequest();

    /** The Constant ID. */
    private static final String ID = "foo";

    /** The Constant ID_REQUEST. */
    private static final IdRequest ID_REQUEST = new IdRequest();

    /** The web service. */
    private ArmaCoreWebServiceBean webService;

    /**
     * Setups test
     * 
     * @throws DKVBusinessException
     *             An exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @Before
    public void setup() throws DKVBusinessException, IOException {
        UserServiceBean userService = mock(UserServiceBean.class);

        UserDTO userDTO = new UserDTO();
        userDTO.setLoginName(ID);

        List<UserDTO> userDTOs = Arrays.asList(new UserDTO(), new UserDTO());

        doThrow(new UserNotFoundException(ID_NONE)).when(userService).findUserByLogin(ID_NONE);
        doReturn(userDTO).when(userService).findUserByLogin(ID);
        doThrow(new GroupNotFoundException(ID_NONE)).when(userService).findUsersInGroup(ID_NONE);
        doReturn(userDTOs).when(userService).findUsersInGroup(ID);

        webService = new ArmaCoreWebServiceBean();
        InjectionUtil.inject(webService, userService);

        ID_REQUEST_NONE.setId(ID_NONE);
        ID_REQUEST.setId(ID);
    }

    /**
     * Test find user by login.
     * 
     * @throws DKVBusinessException
     *             the DKV business exception
     */
    @Test
    public void testFindUserByLogin() throws DKVBusinessException {
        UserResult result = webService.findUserByLogin(ID_REQUEST);
        Assert.assertEquals(result.getValue().getLoginName(), ID);
    }

    /**
     * Test find users in group.
     * 
     * @throws DKVBusinessException
     *             the DKV business exception
     */
    @Test
    public void testFindUsersInGroup() throws DKVBusinessException {
        UserListResult result = webService.findUsersInGroup(ID_REQUEST);
        Assert.assertEquals(2, result.getValue().size());
    }

    /**
     * Test synchronize users.
     * 
     * @throws DKVBusinessException
     *             the DKV business exception
     */
    @Test
    public void testSynchronizeUsers() throws DKVBusinessException {
        webService.synchronizeUsers(new VoidRequest());
    }

    /**
     * Test find user by login not found.
     * 
     * @throws DKVBusinessException
     *             the DKV business exception
     */
    @Test
    public void testFindUserByLoginNotFound() throws DKVBusinessException {
        UserResult result = webService.findUserByLogin(ID_REQUEST_NONE);
        Assert.assertEquals(result.getResultCodes().get(0).getCode(), ARMAErrorCodes.ARMA_ARMA_USER_NOT_FOUND);
    }

    /**
     * Test find users in group not found.
     * 
     * @throws DKVBusinessException
     *             the DKV business exception
     */
    @Test
    public void testFindUsersInGroupNotFound() throws DKVBusinessException {
        UserListResult result = webService.findUsersInGroup(ID_REQUEST_NONE);
        Assert.assertEquals(result.getResultCodes().get(0).getCode(), ARMAErrorCodes.ARMA_ARMA_GROUP_NOT_FOUND);
    }
}
