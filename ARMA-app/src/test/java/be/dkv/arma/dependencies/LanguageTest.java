package be.dkv.arma.dependencies;
import org.junit.Assert;
import org.junit.Test;

import be.dkv.as400.common.enums.Language;


public class LanguageTest {

    @Test
    public void languageEnumChangeTest() {
        Language importedLdapLanguage;
        importedLdapLanguage =
                Language.getLanguage("DUTCH",Language.ENGLISH);
        Assert.assertEquals(importedLdapLanguage.toString(),"DUTCH");
        importedLdapLanguage =
                Language.getByLanguageCode("FR");
        Assert.assertEquals(importedLdapLanguage.toString(),"FRENCH");
    }
    
}
