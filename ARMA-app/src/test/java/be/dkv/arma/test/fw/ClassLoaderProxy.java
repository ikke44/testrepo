package be.dkv.arma.test.fw;

import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;

// TODO: Auto-generated Javadoc
/**
 * The Class ClassLoaderProxy.
 *
 * @author geenens
 */
public class ClassLoaderProxy extends ClassLoader {

    /**
     * Instantiates a new class loader proxy.
     *
     * @param aParent
     *            the a parent
     */
    public ClassLoaderProxy(final ClassLoader aParent) {
        super();
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.ClassLoader#getResources(java.lang.String)
     */
    @Override
    public Enumeration<URL> getResources(final String aName) throws IOException {
        if (!"META-INF/persistence.xml".equals(aName)) {
            System.out.println("NOT Redirecting '" + aName + "");
            return super.getResources(aName);
        } else {
            Enumeration<URL> resources = super.getResources("test/META-INF/persistence.xml");
            System.out.println("Redirecting '" + aName + "' to: 'test/META-INF/persistence.xml'");
            return resources;
        }
    }
    
    

}
