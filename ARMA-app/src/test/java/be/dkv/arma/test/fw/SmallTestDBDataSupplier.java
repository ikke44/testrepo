package be.dkv.arma.test.fw;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;

import be.dkv.arma.entity.Group;
import be.dkv.arma.entity.User;
import be.dkv.arma.model.GroupDTO;
import be.dkv.arma.model.ImportSource;
import be.dkv.arma.model.UserDTO;
import be.dkv.arma.model.UserKey;

public class SmallTestDBDataSupplier {

    public static void preloadDB(EntityManager em) {
        User u = new User();
        u.setLoginName("DOBBELAE");
        u.setFullName("Rolf Dobbelaere");
        u.setMaximumApprovalAmount(new BigDecimal(100));
        u.setMaximumPaymentAmount(new BigDecimal(100));
        u.setUserNumber("1234");
        u.setVerificatorLoginName("JUDONG");
        u.setVerificatorUserNumber("9999");

        em.persist(u);

        Group g1 = new Group();
        g1.setDescription("My description");
        g1.setName("arma.meister");
        g1.setSource(ImportSource.SQL_AND_LDAP);
        u.getGroups().add(g1);

        em.persist(g1);
        em.persist(u);

    }

    public static List<UserKey> getTestUserBatch1() {
        UserKey u1 = new UserKey();
        u1.setLoginName("DECLEVE");
        u1.setUserNumber("2662");
        UserKey u2 = new UserKey();
        u2.setLoginName("VERMEERI");
        u2.setUserNumber("2450");
        UserKey u3 = new UserKey();
        u3.setLoginName("MANCA");
        u3.setUserNumber("2048");
        return Arrays.asList(u1, u2, u3);
    }

    public static UserDTO getTestUser(String loginName, String userNumber, GroupDTO... g) {
        UserDTO udto = new UserDTO();
        udto.setLoginName(loginName);
        udto.setUserNumber(userNumber);
        udto.setGroups(Arrays.asList(g));
        return udto;
    }

    public static GroupDTO getTestGroup(String groupName) {
        GroupDTO gdto = new GroupDTO();
        gdto.setId(groupName);
        gdto.setDescription("This is a test description.");
        return gdto;
    }

}
