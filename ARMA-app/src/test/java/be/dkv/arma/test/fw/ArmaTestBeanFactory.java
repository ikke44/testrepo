package be.dkv.arma.test.fw;

import java.io.IOException;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.easymock.EasyMock;

import be.dkv.arma.dao.GenericDaoBean;
import be.dkv.arma.dao.UserDAO;
import be.dkv.arma.ldap.LDAPDelegate;
import be.dkv.arma.ldap.LDAPService;
import be.dkv.arma.service.impl.UserServiceBean;
import be.dkv.arma.settings.SettingManager;
import be.dkv.fw.util.InjectionUtil;

// TODO: Auto-generated Javadoc
/**
 * A factory for creating ArmaTestBean objects.
 * 
 * @author vanhout
 */
public final class ArmaTestBeanFactory {

    /**
     * Instantiates a new arma test bean factory.
     */
    private ArmaTestBeanFactory() {
    }

    /**
     * Gets the user dao.
     * 
     * @return the user dao
     */
    public static UserDAO getUserDAO() {
        final Thread currentThread = Thread.currentThread();
        final ClassLoader saveClassLoader = currentThread.getContextClassLoader();
        currentThread.setContextClassLoader(new ClassLoaderProxy(saveClassLoader));
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("ARMA-test");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        UserDAO userDAO = new UserDAO();
        userDAO.setEm(entityManager);
        return userDAO;
    }

    /**
     * Gets the user service.
     * 
     * @return the user service
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static UserServiceBean getUserService() throws IOException {
        UserServiceBean userService = new UserServiceBean();
        InjectionUtil.inject(userService, getLDAPService());
        InjectionUtil.inject(userService, getUserDAO());
        return userService;
    }

    /**
     * Gets the LDAP service.
     * 
     * @return the LDAP service
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static LDAPService getLDAPService() throws IOException {
        Properties armaProperties = getARMAProperties();

        LDAPService ldapService = new LDAPService();

        InjectionUtil.inject(ldapService, EasyMock.createNiceMock(SettingManager.class));
        InjectionUtil.inject(ldapService, getARMAProperties());
        InjectionUtil.inject(ldapService, getLDAPDelegate());
        return ldapService;
    }

    /**
     * Gets the LDAP delegate.
     * 
     * @return the LDAP delegate
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static LDAPDelegate getLDAPDelegate() throws IOException {
        LDAPDelegate ldapDelegate = new LDAPDelegate();
        InjectionUtil.inject(ldapDelegate, getLDAPProperties());
        return ldapDelegate;
    }
    
    public static SettingManager getSettingManager(){
        SettingManager settingsManager = new SettingManager();
        GenericDaoBean mockedGDB = EasyMock.createNiceMock(GenericDaoBean.class);
        EasyMock.replay(mockedGDB);
        InjectionUtil.inject(settingsManager,mockedGDB);
        return settingsManager;
    }

    /**
     * Gets the ARMA properties.
     * 
     * @return the ARMA properties
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static Properties getARMAProperties() throws IOException {
        Properties armaProperties = new Properties();
        armaProperties.load(ArmaTestBeanFactory.class.getResourceAsStream("/ARMA/arma.properties"));
        return armaProperties;
    }

    /**
     * Gets the LDAP properties.
     * 
     * @return the LDAP properties
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static Properties getLDAPProperties() throws IOException {
        Properties armaProperties = new Properties();
        armaProperties.load(ArmaTestBeanFactory.class.getResourceAsStream("/ARMA/ldap.properties"));
        return armaProperties;
    }

}
