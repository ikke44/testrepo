package be.dkv.arma.test.fw;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import junit.framework.TestCase;

import org.apache.commons.collections.ListUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.cxf.common.util.CollectionUtils;
import org.apache.log4j.Logger;

import be.dkv.arma.dao.AbstractARMADAO;
import be.dkv.fw.exception.DKVBusinessException;

/**
 * The Class AbstractPersistenceUnitTest can be used to perform Unit testing
 * with In-Memory databases corresponding to the supplied DAOs .
 * 
 * @author dobbelae
 */
public abstract class AbstractPersistenceUnitTest extends TestCase {

    private static Logger logger = Logger.getLogger(AbstractPersistenceUnitTest.class.getName());

    private EntityManagerFactory emFactory;

    protected EntityManager em;

    /**
     * Expects a list of the DAOs that will utilize the in-memory test-database.
     * The internal entitymanagers will be injected with the
     *
     * @return the daos
     */
    public abstract List<AbstractARMADAO> getDaos();

    public abstract List<String> getTestDataScripts();

    public AbstractPersistenceUnitTest() {
        super("ArmaTest");
    }

    @Override
    protected void setUp() throws DKVBusinessException {
        try {
            super.setUp();
        } catch (Exception e) {
            e.printStackTrace();
            fail("Could not setup test: " + super.getClass().getName());
        }
        try {
            logger.info("Building JPA EntityManager for unit tests");
            emFactory = Persistence.createEntityManagerFactory("testArmaPU");
            em = emFactory.createEntityManager();
            List<AbstractARMADAO> daosToConnectToInMemoryDB = getDaos();
            if (daosToConnectToInMemoryDB.isEmpty()) {
                fail("No DAOs were given to the AbstractPersistenceUnitTest, use a normal Unit-test if you don't need DAOs");
            }
            for (AbstractARMADAO dao : daosToConnectToInMemoryDB) {
                dao.setEm(em);
            }
            List<String> sqlFileNames = getTestDataScripts();
            if (!CollectionUtils.isEmpty(sqlFileNames)) {
                for (String sqlFileName : sqlFileNames) {
                    loadSqlFile(sqlFileName);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            fail("Exception during JPA EntityManager instantiation.");
        }
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        logger.info("Shuting down Hibernate JPA layer.");
        if (em != null) {
            em.close();
        }
        if (emFactory != null) {
            emFactory.close();
        }
    }

    /**
     * Loads the SQL file in the DB.
     *
     * @param aTestClass
     *            The test class.
     * @param aFileName
     *            The file name.
     * @throws IOException
     *             Failed to read the file.
     */
    public void loadSqlFile(String aFileName) throws IOException {
        EntityTransaction transaction = em.getTransaction();
        transaction.begin();
        BufferedReader bufferedReader =
                new BufferedReader(new InputStreamReader(new DataInputStream(
                        AbstractPersistenceUnitTest.class.getResourceAsStream(aFileName))));
        String sqlLine;
        while ((sqlLine = bufferedReader.readLine()) != null) {
            String sqlLineTrimmed = StringUtils.trim(sqlLine);
            if (StringUtils.isNotBlank(sqlLineTrimmed)) {
                Query query = em.createNativeQuery(sqlLineTrimmed);
                query.executeUpdate();
            }
        }
        bufferedReader.close();
        transaction.commit();
    }
}