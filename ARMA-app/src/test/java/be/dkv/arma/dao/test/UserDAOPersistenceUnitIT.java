package be.dkv.arma.dao.test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import be.dkv.arma.dao.AbstractARMADAO;
import be.dkv.arma.dao.GroupDAO;
import be.dkv.arma.dao.UserDAO;
import be.dkv.arma.entity.Group;
import be.dkv.arma.entity.User;
import be.dkv.arma.model.ImportSource;
import be.dkv.arma.test.fw.AbstractPersistenceUnitTest;

public class UserDAOPersistenceUnitIT extends AbstractPersistenceUnitTest {

    private static Logger logger = Logger.getLogger(UserDAOPersistenceUnitIT.class.getName());

    private UserDAO userDAO = new UserDAO();
    private GroupDAO groupDAO = new GroupDAO();

    public UserDAOPersistenceUnitIT() {
        super();
    }

    @Override
    public List<AbstractARMADAO> getDaos() {
        List<AbstractARMADAO> daoList = new ArrayList<AbstractARMADAO>();
        daoList.add(userDAO);
        daoList.add(groupDAO);
        return daoList;
    }

    @Override
    public List<String> getTestDataScripts() {
        return null;//no preloaded data needed for this test
    }

    public void testPersistence() {
        try {
            em.getTransaction().begin();

            User u = new User();
            u.setLoginName("OBELIX");
            u.setFullName("Obelix");
            u.setMaximumApprovalAmount(new BigDecimal(100));
            u.setMaximumPaymentAmount(new BigDecimal(100));
            u.setUserNumber("8880");
            u.setVerificatorLoginName("ASTERIX");
            u.setVerificatorUserNumber("8881");

            em.persist(u);
            assertTrue(em.contains(u));

            Group g1 = new Group();
            g1.setDescription("My description");
            g1.setName("arma.meister");
            g1.setSource(ImportSource.SQL_AND_LDAP);
            u.getGroups().add(g1);

            em.persist(g1);
            em.persist(u);

            assertTrue(em.contains(g1));

            System.out.println(userDAO.countAll());
            List<User> users = userDAO.findAll();
            
            for (User user : users) {
                System.out.println("userDAO.findAll() -- " + user.getFullName());
            }
            
            u.getGroups().remove(g1);
            em.remove(u);
            em.merge(g1);
            assertFalse(em.contains(u));

            em.getTransaction().commit();

        } catch (Exception ex) {
            em.getTransaction().rollback();
            ex.printStackTrace();
            fail("Exception during testPersistence");
        }
    }

}