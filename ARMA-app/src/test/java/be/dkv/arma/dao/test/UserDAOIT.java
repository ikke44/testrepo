package be.dkv.arma.dao.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityTransaction;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import be.dkv.arma.dao.AbstractARMADAO;
import be.dkv.arma.dao.UserDAO;
import be.dkv.arma.entity.User;
import be.dkv.arma.test.fw.AbstractPersistenceUnitTest;

public class UserDAOIT extends AbstractPersistenceUnitTest {

    private static final String LOGIN_NAME = "DOBBELAE";

    private static final String NOT_FOUND = "XXXXXXXX";

    private static Logger logger = Logger.getLogger(UserDAOIT.class.getName());

    private UserDAO userDAO = new UserDAO();

    public UserDAOIT() {
        super();
    }

    @Override
    public List<AbstractARMADAO> getDaos() {
        List<AbstractARMADAO> daoList = new ArrayList<AbstractARMADAO>();
        daoList.add(userDAO);
        return daoList;
    }

    @Override
    public List<String> getTestDataScripts() {
        return Arrays.asList("UserDAOTestData.sql");//Use preloaded data in database
    }

    @Test
    public void testPersistence() {
        try {
            em.getTransaction().begin();

            System.out.println(userDAO.countAll());
            List<User> users = userDAO.findAll();
            for (User user : users) {
                System.out.println("userDAO.findAll() -- " + user.getFullName());
            }

            em.getTransaction().commit();

        } catch (Exception ex) {
            em.getTransaction().rollback();
            ex.printStackTrace();
            fail("Exception during testPersistence");
        }
    }

    @Test
    public void testFindUserByLogin() {
        User user = userDAO.findUserByLoginName(LOGIN_NAME);
        Assert.assertNotNull(user);
    }

    @Test
    public void testExistsByLogin() {
        Boolean userExists = userDAO.existsByLoginName(LOGIN_NAME);
        Assert.assertTrue(userExists);
    }

    @Test
    public void testExistsByLoginNotFound() {
        Boolean userExists = userDAO.existsByLoginName(NOT_FOUND);
        Assert.assertFalse(userExists);
    }

    @Test
    public void testFindUserByLoginNotFound() {
        User user = userDAO.findUserByLoginName(NOT_FOUND);
        Assert.assertNull(user);
    }

    @Test
    public void testRemoveAllNotInList() {
        EntityTransaction transaction = em.getTransaction();
        transaction.begin();
        Long countBefore = userDAO.countAll();
        Long countRemoved = (long) userDAO.removeUnlistedLoginNames(Arrays.asList("DOBBELAE"));
        transaction.commit();
        Assert.assertEquals(1, countBefore - countRemoved);
        Long countAfter = userDAO.countAll();
        Assert.assertEquals(1, (long) countAfter);
    }

}