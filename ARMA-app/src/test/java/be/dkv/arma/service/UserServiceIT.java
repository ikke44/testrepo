package be.dkv.arma.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import be.dkv.arma.dao.AbstractARMADAO;
import be.dkv.arma.dao.As400DAO;
import be.dkv.arma.dao.GroupDAO;
import be.dkv.arma.dao.UserDAO;
import be.dkv.arma.entity.Group;
import be.dkv.arma.entity.User;
import be.dkv.arma.exception.GroupNotFoundException;
import be.dkv.arma.exception.UserNotFoundException;
import be.dkv.arma.ldap.LDAPService;
import be.dkv.arma.model.UserDTO;
import be.dkv.arma.service.impl.UserServiceBean;
import be.dkv.arma.settings.SettingManager;
import be.dkv.arma.test.fw.AbstractPersistenceUnitTest;
import be.dkv.arma.test.fw.SmallTestDBDataSupplier;
import be.dkv.fw.exception.DKVBusinessException;
import be.dkv.fw.util.InjectionUtil;


/**
 * Tests whether the throwing of LDAPExceptions by LDAPService is handled
 * correctly by UserServiceBean.
 *
 * @author vanhout
 */
@RunWith(JUnit4.class)
public class UserServiceIT extends AbstractPersistenceUnitTest {

    /** The Constant NUMBER_OF_UPDATED_USERS. */
    private static final int NUMBER_OF_UPDATED_USERS = 20;

    /** The Constant ARMA_USERNUMBER. */
    private static final String ARMA_USERNUMBER = "6296";

    /** The Constant ID_NONE. */
    private static final String ID_NONE = "NONE";

    /** The Constant ID. */
    private static final String ID = "foo";

    /** The Constant ARMA_LOGINNAME. */
    private static final String ARMA_LOGINNAME = "DOBBELAE";

    /** The Constant ARMA_NOT_A_LOGINNAME. */
    private static final String ARMA_NOT_A_LOGINNAME = "ASTERIX";

    /** The Constant ARMA_NOT_A_GROUP. */
    private static final String ARMA_NOT_A_GROUP = "rse.officer";

    /** The Constant ARMA_ADMIN_GROUP. */
    private static final String ARMA_ADMIN_GROUP = "arma.admin";

    /** The user service. */
    private UserServiceBean userService = new UserServiceBean();

    /** The user dao. This DAO will use in-memory test-DB. */
    private UserDAO userDAO = new UserDAO();

    /** The group dao. This DAO will use in-memory test-DB. */
    private GroupDAO groupDAO = new GroupDAO();

    /** The as400 dao. This DAO will be mocked (only executes stored procedures) */
    private As400DAO as400DAO;

    /**
     * Instantiates a new user service test.
     */
    public UserServiceIT() {
        super();
    }

    @SuppressWarnings("rawtypes")
    @Override
    public List<AbstractARMADAO> getDaos() {
        List<AbstractARMADAO> daoList = new ArrayList<AbstractARMADAO>();
        daoList.add(userDAO);
        daoList.add(groupDAO);
        return daoList;
    }

    @Override
    public List<String> getTestDataScripts() {
        return Arrays.asList("UserDAOTestData.sql", "GroupDAOTestData.sql");
    }

    /**
     * Sets the up.
     *
     * @throws DKVBusinessException
     *             An exception
     */
    @Before
    public void setUp() throws DKVBusinessException {
        try {
            super.setUp();
        } catch (DKVBusinessException e) {
            e.printStackTrace();
            fail("Could not setup test: " + super.getClass().getName());
        }

        // Mock of the LDAPService
        LDAPService ldapService = EasyMock.createNiceMock(LDAPService.class);
        EasyMock.expect(ldapService.findAllUserKeys()).andReturn(SmallTestDBDataSupplier.getTestUserBatch1());
        EasyMock.expect(ldapService.isValidGroup(ARMA_ADMIN_GROUP)).andReturn(true);
        EasyMock.expect(ldapService.isValidGroup(ARMA_NOT_A_GROUP)).andReturn(false);
        EasyMock.expect(ldapService.findUsersInGroup(ARMA_ADMIN_GROUP)).andReturn(
                Arrays.asList(SmallTestDBDataSupplier.getTestUser(ARMA_LOGINNAME, ARMA_USERNUMBER,
                        SmallTestDBDataSupplier.getTestGroup(ARMA_ADMIN_GROUP))));
        EasyMock.expect(ldapService.findUserByLogin(ARMA_LOGINNAME)).andReturn(
                SmallTestDBDataSupplier.getTestUser(ARMA_LOGINNAME, ARMA_USERNUMBER,
                        SmallTestDBDataSupplier.getTestGroup(ARMA_ADMIN_GROUP)));
        EasyMock.expect(ldapService.findAllUserKeys()).andReturn(SmallTestDBDataSupplier.getTestUserBatch1());
        EasyMock.replay(ldapService);

        // Mock Stored Procedures in DAO
        as400DAO = EasyMock.createNiceMock(As400DAO.class);
        EasyMock.expect(as400DAO.synchronizeAllUsersWithAS400()).andReturn(NUMBER_OF_UPDATED_USERS);
        EasyMock.replay(as400DAO);
        // ------------------------------------

        InjectionUtil.inject(userService, ldapService);
        InjectionUtil.inject(userService, userDAO);
        InjectionUtil.inject(userService, groupDAO);
        InjectionUtil.inject(userService, as400DAO);
        InjectionUtil.inject(userService, EasyMock.createNiceMock(SettingManager.class));

    }

    /**
     * Test find user by login.
     *
     * @throws DKVBusinessException
     *             the DKV business exception
     */
    @Test
    public void testFindUserByLogin() throws DKVBusinessException {
        UserDTO userDTO = userService.findUserByLogin(ARMA_LOGINNAME);
        Assert.assertEquals(userDTO.getLoginName(), ARMA_LOGINNAME);
    }

    /**
     * Test find users in group.
     *
     * @throws DKVBusinessException
     *             the DKV business exception
     */
    @Test
    public void testFindUsersInGroup() throws DKVBusinessException {
        em.getTransaction().begin();

        // find a group
        Group group = groupDAO.findByName(ARMA_ADMIN_GROUP);
        Assert.assertTrue(ARMA_ADMIN_GROUP.equalsIgnoreCase(group.getName()));
        // find a user
        User user = userDAO.findUserByLoginName(ARMA_LOGINNAME);
        Assert.assertTrue(ARMA_LOGINNAME.equalsIgnoreCase(user.getLoginName()));
        // add group to user
        user.getGroups().add(group);
        em.persist(group);
        em.persist(user);
        em.getTransaction().commit();
        // now at least one user is part of that group
        List<UserDTO> userDTOs = userService.findUsersInGroup(ARMA_ADMIN_GROUP);
        Assert.assertTrue(userDTOs.size() > 0);
        user.setGroups(new HashSet<Group>());
        em.persist(user);
    }

    /**
     * Test synchronize users.
     *
     * @throws DKVBusinessException
     *             the DKV business exception
     */
    @Test
    public void testSynchronizeUsers() throws DKVBusinessException {
        userService.synchronizeUsers();
    }

    /**
     * Test find user by login not found.
     *
     * @throws DKVBusinessException
     *             the DKV business exception
     */
    @Test(expected = UserNotFoundException.class)
    public void testFindUserByLoginNotFound() throws DKVBusinessException {
        userService.findUserByLogin(ARMA_NOT_A_LOGINNAME);
    }

    /**
     * Test find users in group not found.
     *
     * @throws DKVBusinessException
     *             the DKV business exception
     */
    @Test(expected = GroupNotFoundException.class)
    public void testFindUsersInGroupNotFound() throws DKVBusinessException {
        List<UserDTO> userDTOs = userService.findUsersInGroup(ARMA_NOT_A_GROUP);
    }

}
