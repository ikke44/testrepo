package be.dkv.arma;

//import java.io.BufferedReader;
//import java.io.FileNotFoundException;
//import java.io.FileReader;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Map;
//import java.util.Properties;
//import java.util.Set;
//
//import org.apache.commons.lang.builder.ToStringBuilder;
//import org.apache.commons.lang.builder.ToStringStyle;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//
//import be.dkv.arma.ldap.LDAPDelegate;
//import be.dkv.arma.ldap.LDAPService;
//import be.dkv.arma.model.GroupDTO;
//import be.dkv.arma.model.UserDTO;
//import be.dkv.fw.exception.DKVBusinessException;
//import be.dkv.fw.util.InjectionUtil;

/**
 * Examines whether the user/group relations migration to AD was performed
 * correctly.
 *
 * @author vanhout
 */
public class ADTransferIntegrationTest {
//
//    private static String dir = "src/test/resources/ADTransferValidation/";
//
//    private LDAPService ldapService;
//    private final Map<String, UserDTO> users = new HashMap<String, UserDTO>();
//
//    /**
//     * Sets the up.
//     *
//     * @throws IOException
//     *             Signals that an I/O exception has occurred.
//     */
//    @Before
//    public void setUp() throws IOException {
//
//        ldapService = new LDAPService();
//        LDAPDelegate ldapDelegate = new LDAPDelegate();
//
//        Properties ldapProperties = new Properties();
//        ldapProperties.load(LDAPService.class.getResourceAsStream("/ARMA/ldap.properties"));
//
//        Properties armaProperties = new Properties();
//        armaProperties.load(LDAPService.class.getResourceAsStream("/ARMA/arma.properties"));
//
//        /*ldapService.setFilterAllUsers(armaProperties.getProperty(LDAPService.FILTER_ALL_USERS));
//        ldapService.setFilterUserBySAMAccountName(armaProperties
//                .getProperty(LDAPService.FILTER_USER_BY_SAM_ACCOUNT_NAME));
//        ldapService.setFilterGroupByName(armaProperties.getProperty(LDAPService.FILTER_GROUP_BY_NAME));
//        ldapService.setOuIsUsers(armaProperties.getProperty(LDAPService.OU_IS_DKV_USERS));
//        ldapService.setOuIsGroups(armaProperties.getProperty(LDAPService.OU_IS_GROUPS));*/
//
//        InjectionUtil.inject(ldapDelegate, ldapProperties);
//        InjectionUtil.inject(ldapService, ldapProperties);
//        InjectionUtil.inject(ldapService, ldapDelegate);
//    }
//
//    /**
//     * Read relation file.
//     *
//     * @param aFilename
//     *            the a filename
//     * @param aPrefix
//     *            the a prefix
//     * @return the list
//     */
//    public List<Relation> readRelationFile(final String aFilename, final String aPrefix) {
//        BufferedReader br = null;
//        String line = "";
//        String cvsSplitBy = ",";
//
//        List<Relation> relations = new ArrayList<Relation>();
//
//        try {
//
//            br = new BufferedReader(new FileReader(aFilename));
//            while ((line = br.readLine()) != null) {
//                Relation relation = new Relation(line.split(cvsSplitBy));
//                relations.add(relation);
//            }
//
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            if (br != null) {
//                try {
//                    br.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//        return relations;
//    }
//
//    /**
//	 *
//	 */
//    @Test
//    public void testDevList() {
////        checkList("DEV");
//    }
//
//    /**
//	 *
//	 */
//    @Test
//    public void testTestList() {
////        checkList("TEST");
//    }
//
//    /**
//	 *
//	 */
//    @Test
//    public void testAccList() {
////        checkList("ACC");
//    }
//
//    /**
//	 *
//	 */
//    @Test
//    public void testProdList() {
////        checkList("PROD");
//    }
//
//    /**
//     * @param aPrefix
//     */
//    private void checkList(final String aPrefix) {
//        System.out.println("Checking " + aPrefix + " user list");
//        List<Relation> relations = readRelationFile(dir + aPrefix + ".txt", aPrefix + "_");
//        System.out.println("Read CSV user list for " + aPrefix);
//        boolean validates = validateUserList(relations);
//        System.out.println("Finished checking  " + aPrefix + " user list");
//        System.out.println("-----------------");
//        Assert.assertTrue("The " + aPrefix + " user list failed validation.", validates);
//
//    }
//
//    /**
//     * Validate user list.
//     *
//     * @param aRelations
//     *            the a relations
//     * @return true, if successful
//     */
//    public boolean validateUserList(List<Relation> aRelations) {
//        boolean validates = true;
//        Set<String> unavailableUsers = new HashSet<String>();
//        for (Relation relation : aRelations) {
//            String loginName = relation.getUsername();
//
//            // getting user from LDAP (if not already cached)
//            if (!users.containsKey(loginName)) {
//                try {
//                    UserDTO user = ldapService.findUserByLogin(loginName);
//                    users.put(loginName, user);
//                } catch (DKVBusinessException e) {
//                    // processed below
//                }
//            }
//            UserDTO user = users.get(loginName);
//
//            if (user != null) {
//                boolean found = false;
//
//                // comparing relations to user groups
//
//                for (GroupDTO group : user.getGroups()) {
//                    if (!group.getId().equals(relation.getGroupname())) {
//                        found = true;
//                        // System.out.println(" --- " + loginName + " has " +
//                        // relation.getGroupname());
//                        break;
//                    }
//                }
//
//                if (!found) {
//                    System.out.println(" --- " + loginName + " lacks " + relation.getGroupname());
//                    validates = false;
//                }
//            } else {
//                unavailableUsers.add(loginName);
//            }
//        }
//
//        System.out.println("Users unavailable on LDAP: " + unavailableUsers);
//        return validates;
//    }
//
//    /**
//     *
//     *
//     * @author vanhout
//     */
//    private class Relation {
//
//        private String username;
//        private final String groupname;
//
//        public Relation(String[] aInput) {
//            setUsername(aInput[0]);
//            groupname = aInput[1];
//        }
//
//        public String getGroupname() {
//            return groupname;
//        }
//
//        @Override
//        public String toString() {
//            return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
//        }
//
//        public void setUsername(String aUsername) {
//            this.username = aUsername;
//        }
//
//        public String getUsername() {
//            return username;
//        }
//    }
}
