/******[FYI only, unittests are built upon this dataset, modify with caution] This is the SQL Script that you can use to generate the UserDAOTestData sql script  ******/
SELECT CONCAT (
		'INSERT INTO ARMAUser (loginName, userNumber, fullName, kulanzId, maximumApprovalAmount, maximumPaymentAmount, verificatorLoginName, verificatorUserNumber)VALUES( N'''
		,[loginName]
		,''', '
		,CASE WHEN ISNULL(NULLIF([userNumber], ''), [userNumber]) IS NULL THEN 'NULL' ELSE CONCAT ('N''',CAST([userNumber] as varchar),'''') END
		,', '
		,CASE WHEN ISNULL(NULLIF([verificatorUserNumber], ''), [verificatorUserNumber]) IS NULL THEN 'NULL' ELSE CONCAT ('N''',REPLACE([fullName], '''', ''''''),'''') END
		,', '
		,CASE WHEN [kulanzId] IS NULL THEN 'NULL' ELSE CAST([kulanzId] as varchar) END
		,', '
		,CASE WHEN [maximumApprovalAmount] IS NULL THEN 'NULL' ELSE CAST([maximumApprovalAmount] as varchar) END
		,', '
		,CASE WHEN [kulanzId] IS NULL THEN 'NULL' ELSE CAST([kulanzId] as varchar) END
		,', '
		,CASE WHEN [verificatorLoginName] IS NULL THEN 'NULL' ELSE CONCAT ('N''',CAST([verificatorLoginName] as varchar),'''') END
		,', '
		,CASE WHEN ISNULL(NULLIF([verificatorUserNumber], ''), [verificatorUserNumber]) IS NULL THEN 'NULL' ELSE CONCAT ('N''',CAST([verificatorUserNumber] as varchar),'''') END
		,');'
		)
FROM [TEST_ARMA].[dbo].[ARMAUser]
