package be.dkv.arma.result;

import org.apache.commons.lang.builder.ToStringBuilder;

import be.dkv.fw.dto.AbstractValueResult;

/**
 * 
 * @author geenens
 * 
 * @param <T>
 *            The Result values Class.
 */
public abstract class AbstractARMAResult<T> extends AbstractValueResult<T> {

    private static final long serialVersionUID = 1596703844715669705L;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
    
}
