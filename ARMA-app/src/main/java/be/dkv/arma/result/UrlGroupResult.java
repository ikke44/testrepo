package be.dkv.arma.result;

import be.dkv.arma.model.UrlGroupDTO;

public class UrlGroupResult extends AbstractARMAResult<UrlGroupDTO> {
    private static final long serialVersionUID = 103974878930326474L;

    @Override
    public UrlGroupDTO getValue() {
        return value;
    }

    @Override
    public void setValue(UrlGroupDTO aValue) {
        this.value = aValue;
    }

}
