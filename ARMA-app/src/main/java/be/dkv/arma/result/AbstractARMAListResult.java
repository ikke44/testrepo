package be.dkv.arma.result;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

import be.dkv.fw.dto.AbstractListResult;

/**
 * @author dries
 * 
 * @param <T>
 *            The Result values Class.
 */
public abstract class AbstractARMAListResult<T extends Serializable> extends AbstractListResult<T> {

    private static final long serialVersionUID = 6450494341769619040L;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
