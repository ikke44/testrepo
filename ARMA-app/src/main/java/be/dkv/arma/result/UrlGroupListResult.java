package be.dkv.arma.result;

import java.util.Collection;

import be.dkv.arma.model.UrlGroupDTO;

public class UrlGroupListResult extends AbstractARMAListResult<UrlGroupDTO> {
    private static final long serialVersionUID = 103974878930326474L;

    @Override
    public Collection<UrlGroupDTO> getValue() {
        return value;
    }

    @Override
    public void setValue(Collection<UrlGroupDTO> aValue) {
        this.value = aValue;
    }

}
