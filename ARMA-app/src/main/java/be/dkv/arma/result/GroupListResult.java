package be.dkv.arma.result;

import java.util.Collection;

import be.dkv.arma.model.GroupDTO;

/**
 * @author dobbelae
 * 
 */
public class GroupListResult extends AbstractARMAListResult<GroupDTO> {

    private static final long serialVersionUID = 2039799545949626733L;

    @Override
    public Collection<GroupDTO> getValue() {
        return this.value;
    }

    @Override
    public void setValue(Collection<GroupDTO> aValue) {
        this.value = aValue;
    }

}
