package be.dkv.arma.result;

import java.util.Collection;

import be.dkv.arma.model.UserDTO;

/**
 * @author vanhout
 * 
 */
public class UserListResult extends AbstractARMAListResult<UserDTO> {

    private static final long serialVersionUID = -3205181834821287677L;

    @Override
    public Collection<UserDTO> getValue() {
        return this.value;
    }

    @Override
    public void setValue(Collection<UserDTO> aValue) {
        this.value = aValue;
    }

}
