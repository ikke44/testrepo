package be.dkv.arma.result;

import java.util.Collection;

import be.dkv.arma.model.ApplicationUrlDTO;

public class ApplicationUrlListResult extends AbstractARMAListResult<ApplicationUrlDTO> {
    private static final long serialVersionUID = 4107827197342294785L;

    @Override
    public Collection<ApplicationUrlDTO> getValue() {
        return value;
    }

    @Override
    public void setValue(Collection<ApplicationUrlDTO> aValue) {
        this.value = aValue;
    }

}
