package be.dkv.arma.result;

import java.util.Collection;

/**
 * @author vanhout
 * 
 */
public class IdListResult extends AbstractARMAListResult<String> {

    private static final long serialVersionUID = -4843806563105868949L;

    @Override
    public Collection<String> getValue() {
        return this.value;
    }

    @Override
    public void setValue(Collection<String> aValue) {
        this.value = aValue;
    }

}
