package be.dkv.arma.result;

import be.dkv.arma.model.UserDTO;

/**
 * @author vanhout
 * 
 */
public class UserResult extends AbstractARMAResult<UserDTO> {

    private static final long serialVersionUID = -664231366423318667L;

    @Override
    public UserDTO getValue() {
        return this.value;
    }

    @Override
    public void setValue(UserDTO aValue) {
        this.value = aValue;
    }

}
