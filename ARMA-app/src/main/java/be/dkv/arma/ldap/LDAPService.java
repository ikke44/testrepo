package be.dkv.arma.ldap;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchResult;

import org.apache.log4j.Logger;

import be.dkv.arma.common.settings.ArmaSettingsConstant;
import be.dkv.arma.exception.GroupNotFoundException;
import be.dkv.arma.exception.LDAPException;
import be.dkv.arma.mapper.AttributeHandler;
import be.dkv.arma.mapper.GroupMapper;
import be.dkv.arma.mapper.UserMapper;
import be.dkv.arma.model.GroupDTO;
import be.dkv.arma.model.UserDTO;
import be.dkv.arma.model.UserKey;
import be.dkv.arma.model.UserLDAPSearchCriteria;
import be.dkv.arma.service.impl.AbstractArmaBean;
import be.dkv.fw.exception.DKVBusinessException;

// TODO: Auto-generated Javadoc
/**
 * Manages the retrieval and basic processing/mapping of user and group data
 * from the LDAP AD.
 * 
 * @author vanhout
 * 
 */
@Stateless
public class LDAPService extends AbstractArmaBean {

    private static final Logger LOG = Logger.getLogger(LDAPService.class);

    /** Group name separator inside LDAP. */
    public static final String LDAP_GROUP_NAME_SEPARATOR = "_";
    /** Group name separator outside LDAP. */
    public static final String GENERAL_GROUP_NAME_SEPARATOR = ".";

    /** Location of LDAP properties file. */
    public static final String LDAP_PROPS_FILE = "/ARMA/ldap.properties";

    /** The Constant MEMBER_OF. */
    public static final String MEMBER_OF = "memberof";

    /** The Constant USER_LOGIN. */
    public static final int USER_LOGIN = 0;

    /** The Constant USERNUMBER. */
    public static final int USERNUMBER = 1;

    /** The Constant LDAP_WILDCARD. */
    private static final char LDAP_WILDCARD = '*';

    public LDAPService() {
        super(LOG);
    }

    /** The ldap delegate. */
    @EJB
    private LDAPDelegate ldapDelegate;

    /**
     * Retrieves a list of all user names.
     * 
     * @return A list of all user names
     * @throws DKVBusinessException
     *             the DKV business exception
     */
    public List<String> findAllUserNames() throws DKVBusinessException {
        String filterAllUsers = this.get(ArmaSettingsConstant.FILTER_ALL_USERS);
        List<SearchResult> results = ldapDelegate.performQuery(filterAllUsers, null, new String[] { UserMapper.USER_LOGIN,
                UserMapper.DISTINGUISHED_NAME });

        List<String> users = new ArrayList<String>();
        for (SearchResult result : results) {
            Attributes attributes = result.getAttributes();
            String distinguishedName = AttributeHandler.valueAsString(attributes, UserMapper.DISTINGUISHED_NAME);

            String ouIsUsers = this.get(ArmaSettingsConstant.OU_IS_DKV_USERS);
            if (distinguishedName != null && distinguishedName.toUpperCase().contains(ouIsUsers)) {
                users.add(AttributeHandler.valueAsString(attributes, UserMapper.USER_LOGIN));
            }
        }
        return users;
    }

    /**
     * Retrieves a list of all userkeys.
     * 
     * @return A list of all userkeys containing loginName and userNumber.
     * @throws DKVBusinessException
     *             the DKV business exception
     */
    public List<UserKey> findAllUserKeys() throws DKVBusinessException {
        final String filterAllUsers = this.get(ArmaSettingsConstant.FILTER_ALL_USERS);
        List<SearchResult> results = ldapDelegate.performQuery(filterAllUsers, null, new String[] { UserMapper.USER_LOGIN,
                UserMapper.DISTINGUISHED_NAME, UserMapper.USERNUMBER });

        List<UserKey> userkeys = new ArrayList<UserKey>();
        for (SearchResult result : results) {
            Attributes attributes = result.getAttributes();
            String distinguishedName = AttributeHandler.valueAsString(attributes, UserMapper.DISTINGUISHED_NAME);

            String ouIsUsers = this.get(ArmaSettingsConstant.OU_IS_DKV_USERS);
            if (distinguishedName != null && distinguishedName.toUpperCase().contains(ouIsUsers)) {
                UserKey userkey = new UserKey();
                userkey.setLoginName(AttributeHandler.valueAsString(attributes, UserMapper.USER_LOGIN));
                userkey.setUserNumber(AttributeHandler.valueAsString(attributes, UserMapper.USERNUMBER));
                userkeys.add(userkey);
            }
        }
        return userkeys;
    }

    /**
     * Finds a UserDTO based on a given login name (LDAP sAMAccountName
     * attribute).
     * 
     * @param aLoginName
     *            The user name
     * @return A UserDTO
     * @throws DKVBusinessException
     *             the DKV business exception
     */
    public UserDTO findUserByLogin(String aLoginName) throws DKVBusinessException {
        final String filterUserBySAMAccountName = this.get(ArmaSettingsConstant.FILTER_USER_BY_SAM_ACCOUNT_NAME);
        List<SearchResult> results = ldapDelegate.performQuery(filterUserBySAMAccountName, new String[] { aLoginName },
                UserMapper.getAttributeNames());
        UserDTO user = null;

        if (results.size() == 1) {
            Attributes attributes = results.get(0).getAttributes();
            // Set regular attributes
            user = UserMapper.map(attributes);
            // Set the correct managerUserNumber
            String managerDistinguishedName = AttributeHandler.valueAsString(attributes, UserMapper.MANAGER);
            String managerUserNumber = findUserNumberByDistinguishedName(managerDistinguishedName);
            user.setManagerUserNumber(managerUserNumber);
            // Set groups
            user.setGroups(retrieveGroupsFromAttribute(attributes.get(UserMapper.MEMBER_OF)));
        } else if (results.size() == 0) {
        	LOG.warn("LDAP user " + aLoginName + " not found.");
            user = null;
        } else {
            throw new LDAPException("Expected a single search result instead of " + results.size() + " when performing query "
                    + filterUserBySAMAccountName + " for " + aLoginName);

        }
        return user;
    }

    /**
     * Find user groups by login.
     * 
     * @param aLoginName
     *            the a login name
     * @return the list
     * @throws DKVBusinessException
     *             the DKV business exception
     */
    public List<GroupDTO> findUserGroupsByLogin(String aLoginName) throws DKVBusinessException {
        List<SearchResult> results = ldapDelegate.performQuery(this.get(ArmaSettingsConstant.FILTER_USER_BY_SAM_ACCOUNT_NAME),
                new String[] { aLoginName }, UserMapper.getAttributeNames());

        List<GroupDTO> resultGroupDTOs = null;

        if (results.size() == 1) {
            Attributes attributes = results.get(0).getAttributes();
            resultGroupDTOs = retrieveGroupsFromAttribute(attributes.get(UserMapper.MEMBER_OF));
        } else {
            resultGroupDTOs = new ArrayList<GroupDTO>();
        }
        return resultGroupDTOs;
    }

    /**
     * Returns a set of the distinguished names of the users belonging to a
     * given group. Note that this is done recursively to include subgroups.
     * 
     * @param aGroupId
     *            The group id
     * @return A list of UserDTOs
     * @throws DKVBusinessException
     *             the DKV business exception
     */
    private Set<String> findUserNamesInGroup(String aGroupId) throws DKVBusinessException {
        String convertedGroupId = null;
        if (aGroupId != null) {
            convertedGroupId = aGroupId.toLowerCase().replace(GENERAL_GROUP_NAME_SEPARATOR, LDAP_GROUP_NAME_SEPARATOR);
        }
        String filterGroupByName = this.get(ArmaSettingsConstant.FILTER_GROUP_BY_NAME);
        List<SearchResult> results = ldapDelegate.performQuery(filterGroupByName, new String[] { convertedGroupId },
                GroupMapper.getAttributeNames());
        Set<String> userNameSet = null;

        if (results.size() == 1) {
            Attributes attributes = results.get(0).getAttributes();
            NamingEnumeration<? extends Attribute> attributesEnumeration = attributes.getAll();
            userNameSet = new HashSet<String>();
            while (attributesEnumeration.hasMoreElements()) {
                Attribute attribute = attributesEnumeration.nextElement();
                try {
                    if (GroupMapper.MEMBER.equals(attribute.getID())) {
                        extractUserNameSetFromAttributes(userNameSet, attribute);
                    }
                } catch (NamingException e) {
                    throw new LDAPException("LDAP naming exception when performing query " + filterGroupByName + " for " + aGroupId, e);
                }
            }
        } else if (results.size() == 0) {
            throw new GroupNotFoundException(aGroupId);
        } else {
            throw new LDAPException("Expected a single search result instead of " + results.size() + " when performing query "
                    + filterGroupByName + " for " + aGroupId);
        }
        return userNameSet;
    }

    /**
     * Extract user name set from attributes.
     * 
     * @param userNameSet
     *            the user name set
     * @param attribute
     *            the attribute
     * @throws NamingException
     *             the naming exception
     * @throws DKVBusinessException
     *             the DKV business exception
     */
    private void extractUserNameSetFromAttributes(Set<String> userNameSet, Attribute attribute) throws NamingException,
            DKVBusinessException {
        for (int i = 0; i < attribute.size(); i++) {
            String distinguishedName = String.valueOf(attribute.get(i));
            if (distinguishedName != null) {
                if (distinguishedName.contains(this.get(ArmaSettingsConstant.OU_IS_GROUPS))) {
                    Set<String> subgroupUserNames = findUserNamesInGroup(extractGroupId(distinguishedName));
                    userNameSet.addAll(subgroupUserNames);
                } else if (distinguishedName.contains(this.get(ArmaSettingsConstant.OU_IS_DKV_USERS))) {
                    userNameSet.add(distinguishedName);
                }
            }
        }
    }

    /**
     * Returns a list of users belonging to a given group. Note that this is
     * done recursively to include subgroups, and that group fields of the users
     * are not filled. Also note that this is done within the specified LDAP
     * environment.
     * 
     * @param aGroupId
     *            The group id
     * @return A list of UserDTOs
     * @throws DKVBusinessException
     *             An LDAPException
     */
    public List<UserDTO> findUsersInGroup(String aGroupId) throws DKVBusinessException {
        String environmentGroupId = this.get(ArmaSettingsConstant.ENVIRONMENT) + LDAP_GROUP_NAME_SEPARATOR + aGroupId;
        Set<String> userNameSet;
        try {
            userNameSet = findUserNamesInGroup(environmentGroupId);
        } catch (GroupNotFoundException e) {
            environmentGroupId = aGroupId;
            userNameSet = findUserNamesInGroup(environmentGroupId);
        }
        return findUsersByDistinguishedNames(userNameSet);
    }

    /**
     * Returns a list of users based on a list of distinguished names.
     * 
     * @param aDistinguishedNames
     *            A list of users' distinguished names
     * @return A UserDTO
     * @throws DKVBusinessException
     *             the DKV business exception
     */
    private List<UserDTO> findUsersByDistinguishedNames(Collection<String> aDistinguishedNames) throws DKVBusinessException {
        final String filterPrefix = "(&(objectcategory=user)(|";
        final String filterSuffix = "))";
        final String userPrefix = "(distinguishedName=";
        final String userSuffix = ")";
        StringBuilder filterBuilder = new StringBuilder();
        filterBuilder.append(filterPrefix);
        for (String distinguishedName : aDistinguishedNames) {
            filterBuilder.append(userPrefix);
            filterBuilder.append(distinguishedName);
            filterBuilder.append(userSuffix);
        }
        filterBuilder.append(filterSuffix);
        String filter = filterBuilder.toString();

        List<SearchResult> userResults = ldapDelegate.performQuery(filter, null, UserMapper.getMinimalAttributeNames());

        List<UserDTO> users = new ArrayList<UserDTO>();
        for (SearchResult result : userResults) {
            Attributes attributes = result.getAttributes();
            UserDTO user = UserMapper.map(attributes);
            users.add(user);
        }
        return users;
    }

    /**
     * Retrieves a list of groups based on group ids (LDAP cn attribute). Note
     * that only those groups within the specified LDAP environment are
     * retrieved.
     * 
     * @param aGroupIds
     *            The group ids
     * @return A list of groups
     * @throws DKVBusinessException
     *             the DKV business exception
     */
    private List<GroupDTO> findGroups(Collection<String> aGroupIds) throws DKVBusinessException {
        final String environment = this.get(ArmaSettingsConstant.ENVIRONMENT);
        // build a combined query filter
        final String filterPrefix = "(&(objectClass=group)(|";
        final String filterSuffix = "))";
        final String groupPrefix = "(cn=";
        final String groupSuffix = ")";
        StringBuilder filterBuilder = new StringBuilder();
        filterBuilder.append(filterPrefix);

        for (String groupId : aGroupIds) {
            String convertedGroupId = null;
            if (groupId != null) {
                convertedGroupId = groupId.toLowerCase().replace(GENERAL_GROUP_NAME_SEPARATOR, LDAP_GROUP_NAME_SEPARATOR);
            }

            filterBuilder.append(groupPrefix);
            filterBuilder.append(environment + LDAP_GROUP_NAME_SEPARATOR);
            filterBuilder.append(convertedGroupId);
            filterBuilder.append(groupSuffix);
        }
        filterBuilder.append(filterSuffix);
        String filter = filterBuilder.toString();

        List<SearchResult> userResults = ldapDelegate.performQuery(filter, null, GroupMapper.getAttributeNames());

        List<GroupDTO> groups = new ArrayList<GroupDTO>();
        for (SearchResult result : userResults) {
            Attributes attributes = result.getAttributes();
            groups.add(GroupMapper.map(attributes, environment + LDAP_GROUP_NAME_SEPARATOR));
        }
        return groups;
    }

    /**
     * Retrieves a list of groups, based on a user's membership list LDAP
     * attribute (i.e. MEMBER_OF).
     * 
     * @param aMembershipList
     *            An attribute which comprises a list of group (distinguished)
     *            names
     * @return A list of GroupDTOs
     * @throws DKVBusinessException
     *             the DKV business exception
     */
    private List<GroupDTO> retrieveGroupsFromAttribute(Attribute aMembershipList) throws DKVBusinessException {
        List<GroupDTO> groupDTOs = null;
        try {
            List<String> groupIds = new ArrayList<String>();
            if (aMembershipList != null) {
                for (int n = 0; n < aMembershipList.size(); ++n) {
                    String distinguishedName = String.valueOf(aMembershipList.get(n));
                    String groupId = extractGroupId(distinguishedName);
                    groupId = groupId.toLowerCase();
                    String environment = this.get(ArmaSettingsConstant.ENVIRONMENT);
                    if (groupId.startsWith(environment.toLowerCase() + LDAP_GROUP_NAME_SEPARATOR)) {
                        groupId = groupId.substring(environment.length() + 1);
                        groupId = groupId.replace(LDAP_GROUP_NAME_SEPARATOR, GENERAL_GROUP_NAME_SEPARATOR);
                        groupIds.add(groupId);
                    }
                }
            }
            groupDTOs = findGroups(groupIds);
        } catch (NamingException e) {
            throw new LDAPException("LDAP naming exception when retrieving groups from attribute data");
        }
        return groupDTOs;
    }

    /**
     * Produces the id (LDAP CN attribute) of a group based on its distinguished
     * name.
     * 
     * @param aDistinguishedName
     *            The group's distinguished name
     * @return A group id
     */
    private String extractGroupId(String aDistinguishedName) {
        int start = aDistinguishedName.indexOf(GroupMapper.ID) + GroupMapper.ID.length() + 1;
        int end = aDistinguishedName.indexOf(',', start);
        return aDistinguishedName.substring(start, end);
    }

    /**
     * <b>CURRENTLY IN DEVELOPMENT</b><br>
     * Does nothing.
     * 
     * @param mapToLDAP
     *            the map to ldap
     * @return An <b>empty</b> UserDTO ArrayList
     */
    public List<UserDTO> findUserBySearchCriteria(UserLDAPSearchCriteria mapToLDAP) {
        return new ArrayList<UserDTO>();
    }

    /**
     * Find all groups.
     * 
     * @return the list
     * @throws DKVBusinessException
     *             the DKV business exception
     */
    public List<GroupDTO> findAllGroups() throws DKVBusinessException {
        final String environment = this.get(ArmaSettingsConstant.ENVIRONMENT);
        final String filterPrefix = "(&(objectClass=group)(|";
        final String filterSuffix = "))";
        final String groupPrefix = "(cn=";
        final String groupSuffix = ")";
        StringBuilder filterBuilder = new StringBuilder();
        filterBuilder.append(filterPrefix);
        filterBuilder.append(groupPrefix);

        filterBuilder.append(environment + LDAP_GROUP_NAME_SEPARATOR);
        filterBuilder.append(LDAP_WILDCARD);
        filterBuilder.append(groupSuffix);
        filterBuilder.append(filterSuffix);
        String filter = filterBuilder.toString();

        List<SearchResult> userResults = ldapDelegate.performQuery(filter, null, GroupMapper.getAttributeNames());

        List<GroupDTO> groups = new ArrayList<GroupDTO>();
        for (SearchResult result : userResults) {
            Attributes attributes = result.getAttributes();
            groups.add(GroupMapper.map(attributes, environment + LDAP_GROUP_NAME_SEPARATOR));
        }
        return groups;
    }

    /**
     * Returns the userNumber based on distinguished name.
     * 
     * @param aDistinguishedName
     *            A users' distinguished name
     * @return A userNumber as a String
     * @throws DKVBusinessException
     *             the DKV business exception
     */
    private String findUserNumberByDistinguishedName(String aDistinguishedName) throws DKVBusinessException {
        String userNumber = null;

        final String filterPrefix = "(&(objectcategory=user)(|";
        final String filterSuffix = "))";
        final String userPrefix = "(distinguishedName=";
        final String userSuffix = ")";
        StringBuilder filterBuilder = new StringBuilder();
        filterBuilder.append(filterPrefix);

        filterBuilder.append(userPrefix);
        filterBuilder.append(aDistinguishedName);
        filterBuilder.append(userSuffix);

        filterBuilder.append(filterSuffix);
        String filter = filterBuilder.toString();

        List<SearchResult> userResults = ldapDelegate.performQuery(filter, null, UserMapper.getMinimalAttributeNames());
        if (!userResults.isEmpty() && userResults.get(0) != null) {
            SearchResult result = userResults.get(0);
            userNumber = AttributeHandler.valueAsString(result.getAttributes(), UserMapper.USERNUMBER);

        }
        return userNumber;
    }

    /**
     * Verifies that this group can be found on Active Directory.
     * 
     * @param aGroupId
     *            The group id
     * @return A list of UserDTOs
     * @throws DKVBusinessException
     *             the DKV business exception
     */
    public boolean isValidGroup(String aGroupId) throws DKVBusinessException {
        String convertedGroupId = null;
        if (aGroupId != null) {
            convertedGroupId = aGroupId.toLowerCase().replace(GENERAL_GROUP_NAME_SEPARATOR, LDAP_GROUP_NAME_SEPARATOR);
        }
        String filterGroupByName = this.get(ArmaSettingsConstant.FILTER_GROUP_BY_NAME);
        List<SearchResult> results = ldapDelegate.performQuery(filterGroupByName, new String[] { convertedGroupId },
                GroupMapper.getAttributeNames());
        if (results.size() == 1) {
            return true;
        } else if (results.size() == 0) {
            return false;
        } else {
            throw new LDAPException("Expected a single search result instead of " + results.size() + " when performing query "
                    + filterGroupByName + " for " + aGroupId);
        }
    }

}
