package be.dkv.arma.ldap;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.ejb.Stateless;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.Control;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import javax.naming.ldap.PagedResultsResponseControl;

import org.apache.log4j.Logger;

import be.dkv.arma.common.settings.ArmaSettingsConstant;
import be.dkv.arma.exception.LDAPException;
import be.dkv.arma.service.impl.AbstractArmaBean;
import be.dkv.fw.exception.DKVBusinessException;

/**
 * Utility class for performing LDAP queries.
 *
 * @author vanhout
 *
 */
@Stateless
public class LDAPDelegate extends AbstractArmaBean {

    private static final Logger LOG = Logger.getLogger(LDAPDelegate.class);

    /** The base LDAP DN. */
    private static final String BASE_DN = "baseDN";
    /**
     * Maximum number of entries returned by a single query, used as the number
     * of entries per page when employing paginated queries.
     */
    private static final int PAGE_SIZE = 1;

    public LDAPDelegate() {
    	super(LOG);
    }

    /**
     * Supplies the results of an LDAP query.
     *
     * @param aFilter
     *            The query filter
     * @param aFilterParameters
     *            The parameter values that are inserted into the query filter
     * @param aReturningAttributes
     *            The attributes that are to be returned (when null, all are
     *            returned)
     * @return A list of {@link SearchResult}
     * @throws DKVBusinessException
     *             the DKV business exception
     */
    public List<SearchResult> performQuery(final String aFilter, String[] aFilterParameters,
            String[] aReturningAttributes) throws DKVBusinessException {
        List<SearchResult> results = new ArrayList<SearchResult>();

        LdapContext context = null;

        try {
            byte[] cookie = null;
            Properties ldapProperties = getLDAPProperties();
            context =
                    new InitialLdapContext(ldapProperties, new Control[] { new javax.naming.ldap.PagedResultsControl(
                            PAGE_SIZE, cookie, Control.CRITICAL) });

            context.setRequestControls(new Control[] { new javax.naming.ldap.PagedResultsControl(PAGE_SIZE,
                    Control.CRITICAL) });

            do {
                SearchControls searchControls = new SearchControls();
                searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
                searchControls.setReturningAttributes(aReturningAttributes);
                String aBaseDN = ldapProperties.getProperty(BASE_DN);
                NamingEnumeration<SearchResult> pageResults =
                        context.search(aBaseDN, aFilter, aFilterParameters, searchControls);

                while (pageResults != null && pageResults.hasMoreElements()) {
                    SearchResult result = pageResults.next();
                    results.add(result);
                }

                Control[] controls = context.getResponseControls();
                if (controls != null) {
                    for (Control control : controls) {
                        if (control instanceof PagedResultsResponseControl) {
                            PagedResultsResponseControl responseControl = (PagedResultsResponseControl) control;
                            cookie = responseControl.getCookie();
                            if (cookie != null) {
                                context.setRequestControls(new Control[] { new javax.naming.ldap.PagedResultsControl(
                                        PAGE_SIZE, cookie, Control.CRITICAL) });
                            }
                        }
                    }
                }
            } while (cookie != null);
        } catch (NamingException e) {
            throw new LDAPException("LDAP query has produced an exception: " + aFilter + " "
                    + Arrays.toString(aFilterParameters), e);
        } catch (IOException e) {
            throw new LDAPException("(Re)setting the RequestControls has produced an exception for query: " + aFilter
                    + " " + Arrays.toString(aFilterParameters), e);
        } catch(Exception e) {
        	throw new LDAPException("LDAP query has produced an exception: " + aFilter + " "
                    + Arrays.toString(aFilterParameters), e);
        }finally {
            try {
                context.close();
            } catch (NamingException e) {
                throw new LDAPException("LDAP query has produced an exception while closing LDAP context", e);
            } catch (NullPointerException ex) {
                throw new LDAPException("LDAP query has produced an exception while closing LDAP context", ex);
            }
        }
        return results;
    }

	private Properties getLDAPProperties() {
		Properties properties = new Properties();
		properties.put("baseDN", this.get(ArmaSettingsConstant.BASEDN));
		properties.put("searchFilter.user.login", this.get(ArmaSettingsConstant.FILTER_USER_BY_SAM_ACCOUNT_NAME));
		properties.put("searchFilter.user.distinguishedName", this.get(ArmaSettingsConstant.SEARCHFILTER_USER_DISTINGUISHEDNAME));
		properties.put("searchFilter.group.cn", this.get(ArmaSettingsConstant.SEARCHFILTER_GROUP_CN));
		properties.put("java.naming.provider.url", this.get(ArmaSettingsConstant.JAVA_NAMING_PROVIDER_URL));
		properties.put("java.naming.factory.initial", this.get(ArmaSettingsConstant.JAVA_NAMING_FACTORY_INITIAL));
		properties.put("java.naming.factory.object", this.get(ArmaSettingsConstant.JAVA_NAMING_FACTORY_OBJECT));
		properties.put("java.naming.factory.state", this.get(ArmaSettingsConstant.JAVA_NAMING_FACTORY_STATE));
		properties.put("java.naming.security.principal", this.get(ArmaSettingsConstant.JAVA_NAMING_SECURITY_PRINCIPAL));
		properties.put("java.naming.security.credentials", this.get(ArmaSettingsConstant.JAVA_NAMING_SECURITY_CREDENTIALS));

		return properties;
	}
}
