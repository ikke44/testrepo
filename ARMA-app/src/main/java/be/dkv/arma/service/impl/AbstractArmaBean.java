package be.dkv.arma.service.impl;

import javax.ejb.EJB;

import org.apache.log4j.Logger;

import be.dkv.arma.dao.GenericDaoBean;
import be.dkv.arma.settings.SettingManager;
import be.dkv.fw.dao.AbstractGenericDaoBean;
import be.dkv.fw.manager.AbstractSettingManager;
import be.dkv.fw.service.AbstractBean;

public class AbstractArmaBean extends AbstractBean {

	@EJB
	private GenericDaoBean genericDAO;
	
	@EJB
	private SettingManager settingManager;
	
	public AbstractArmaBean(Logger logger) {
		super(logger);
	}

	@Override
	protected AbstractGenericDaoBean getGenericDao() {
		return genericDAO;
	}

	@Override
	protected AbstractSettingManager getSettingManager() {
		return settingManager;
	}

}
