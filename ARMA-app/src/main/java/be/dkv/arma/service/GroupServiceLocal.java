package be.dkv.arma.service;

import java.util.List;

import javax.ejb.Local;

import be.dkv.arma.entity.Group;
import be.dkv.arma.model.GroupDTO;
import be.dkv.fw.exception.DKVBusinessException;

/**
 * Provides an interface of managing Groups in SQL DB as well as importing and
 * syncing Groups from ActiveDirectory with the SQL database.
 * See also: {@link GroupDTO},{@link Group}.
 *
 * @author dobbelae
 *
 */
@Local
public interface GroupServiceLocal extends GroupServiceRemote {

    /**
     * Finds all unique groups on AD and the SQL database as a {@link List} of
     * {@link GroupDTO}s. Internally, two {@link List}s of {@link GroupDTO}s are
     * merged them into a new {@link List} based on their GroupIds. When doubles
     * are found, this method will prioritize the {@link GroupDTO} Objects found
     * in the SQL database as they are able to contain a description.
     *
     * @return A {@link List} of {@link GroupDTO}s
     */
    List<GroupDTO> findAllGroups();

    /**
     * Retrieves a list of all the possible groupnames.
     *
     * @return A list of groupnames.
     */
    List<String> findAllGroupNames();

    /**
     * Synchronizes the groups of the SQL database with those on AD. This will
     * remove unused groups if their import source was AD
     *
     * @throws DKVBusinessException
     *             Synchronization produced an exception
     */
    void synchronizeGroups() throws DKVBusinessException;

    void addGroupInSQL(GroupDTO aGroup) throws DKVBusinessException;

    void removeGroupsFromSQL(List<String> groupIds) throws DKVBusinessException;
}
