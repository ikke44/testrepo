package be.dkv.arma.service;

import java.util.List;

import javax.ejb.Local;

import be.dkv.arma.model.UrlGroupDTO;
import be.dkv.fw.exception.DKVBusinessException;

// TODO: Auto-generated Javadoc
/**
 * The Interface UrlGroupServiceLocal.
 */
@Local
public interface UrlGroupServiceLocal {

    /**
     * Gets the url groups for current user.
     *
     * @return the url groups for current user
     * @throws DKVBusinessException the DKV business exception
     */
    public List<UrlGroupDTO> getUrlGroupsForCurrentUser() throws DKVBusinessException;

    /**
     * Gets the url groups for modification.
     *
     * @return the url groups for modification
     * @throws DKVBusinessException the DKV business exception
     */
    public List<UrlGroupDTO> getUrlGroupsForModification() throws DKVBusinessException;

    /**
     * Adds the url group.
     *
     * @param aUrlGroupDTO the a url group dto
     * @return the url group dto
     * @throws DKVBusinessException the DKV business exception
     */
    public UrlGroupDTO addUrlGroup(UrlGroupDTO aUrlGroupDTO) throws DKVBusinessException;

    /**
     * Update url group.
     *
     * @param aUrlGroupDTO the a url group dto
     */
    public void updateUrlGroup(UrlGroupDTO aUrlGroupDTO);

    /**
     * Removes the url group.
     *
     * @param aId the a id
     */
    public void removeUrlGroup(Long aId);

    /**
     * Find all.
     *
     * @return the list
     */
    public List<UrlGroupDTO> findAll();

}
