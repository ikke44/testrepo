package be.dkv.arma.service;

import javax.ejb.Local;

import be.dkv.arma.entity.Group;
import be.dkv.arma.model.GroupDTO;
import be.dkv.arma.model.ImportSource;
import be.dkv.fw.exception.DKVBusinessException;

/**
 * Provides an interface of importing
 * Groups from ActiveDirectory with the SQL database.
 * See also: {@link GroupDTO},{@link Group}.
 *
 * @author dobbelae
 *
 */
@Local
public interface ImportServiceLocal {

    /**
     * This method imports the supplied group-data into the SQL database. When
     * importing an existing group, it's data will be merged and a soft import
     * will be performed. i.e. Data imported from {@link ImportSource#LDAP LDAP}
     * while existing in {@link ImportSource#SQL SQL} will result in the shared
     * {@link ImportSource}: "{@link ImportSource#SQL_AND_LDAP SQL_AND_LDAP}".
     *
     * @param groupDTO
     *            the data of the group to be imported to the SQL database.
     * @param importSourceDestination
     *            the {@link ImportSource} where the groupDTO data came from.
     * @throws DKVBusinessException
     *             the DKV business exception
     * @return Group created based on the given input
     */
    public Group importGroup(GroupDTO groupDTO, ImportSource importSourceDestination) throws DKVBusinessException;
  
}
