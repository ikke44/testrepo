package be.dkv.arma.service.impl;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;

import be.dkv.arma.dao.GroupDAO;
import be.dkv.arma.entity.Group;
import be.dkv.arma.ldap.LDAPService;
import be.dkv.arma.mapper.GroupMapper;
import be.dkv.arma.model.GroupDTO;
import be.dkv.arma.model.ImportSource;
import be.dkv.arma.service.ImportServiceLocal;
import be.dkv.fw.exception.DKVBusinessException;

/**
 * Returns group data which combines the group data on LDAP and on the SQL
 * database.
 *
 * @author dobbelae
 */
@Stateless
public class ImportServiceBean extends AbstractArmaBean implements ImportServiceLocal {

    private static final Logger LOG = Logger.getLogger(ImportServiceBean.class);

    @EJB
    private GroupDAO groupDAO;

    @EJB
    private LDAPService ldapService;

    public ImportServiceBean() {
        super(LOG);
    }

    @Override
    public Group importGroup(GroupDTO groupDTO, ImportSource importSourceDestination) throws DKVBusinessException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("importGroup");
        }
        if (groupDAO.existsByName(groupDTO.getId())) {
            // If the group already exist, perform a soft Import.
            Group existingGroup = groupDAO.findByName(groupDTO.getId());
            if (groupDTO.getDescription() != null && !groupDTO.getDescription().isEmpty()) {
                existingGroup.setDescription(groupDTO.getDescription());
            }
            switch (importSourceDestination) {
                case SQL_AND_LDAP:
                    // group already fully imported
                    break;
                case SQL:
                    if (existingGroup.getSource().equals(ImportSource.LDAP)) {
                        existingGroup.setSource(ImportSource.SQL_AND_LDAP);
                    }
                    break;
                case LDAP:
                    if (existingGroup.getSource().equals(ImportSource.SQL)) {
                        existingGroup.setSource(ImportSource.SQL_AND_LDAP);
                    }
                    break;

                default:
                    break;

            }
            return existingGroup;
        } else {
            Group newGroup = GroupMapper.mapToEntity(groupDTO);
            newGroup.setSource(importSourceDestination);
            groupDAO.persist(newGroup);
            return newGroup;
        }
    }
  
    

}
