package be.dkv.arma.service.impl;

import java.util.HashSet;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;

import be.dkv.arma.dao.ApplicationUrlDAO;
import be.dkv.arma.dao.GroupDAO;
import be.dkv.arma.dao.UrlGroupDAO;
import be.dkv.arma.entity.ApplicationUrl;
import be.dkv.arma.entity.Group;
import be.dkv.arma.entity.UrlGroup;
import be.dkv.arma.exception.ApplicationUrlNotFoundException;
import be.dkv.arma.mapper.ApplicationUrlMapper;
import be.dkv.arma.model.ApplicationUrlDTO;
import be.dkv.arma.service.ApplicationUrlServiceLocal;
import be.dkv.fw.exception.DKVBusinessException;

@Stateless
public class ApplicationUrlServiceBean extends AbstractArmaBean implements ApplicationUrlServiceLocal {
    @EJB
    private ApplicationUrlDAO applicationUrlDAO;

    @EJB
    private UrlGroupDAO urlGroupDAO;

    @EJB
    private GroupDAO groupDAO;

    public ApplicationUrlServiceBean() {
        super(Logger.getLogger(ApplicationUrlServiceBean.class));
    }

    @Override
    public void updateApplicationURL(ApplicationUrlDTO aApplicationUrlDTO) throws DKVBusinessException {
        if (aApplicationUrlDTO != null) {
            ApplicationUrl applicationUrl = applicationUrlDAO.findById(aApplicationUrlDTO.getId());
            if (applicationUrl != null) {
                // Groups theirselves in the AccessRight field in the
                // ApplicationURL
                // should not be modifiable via this method.
                Set<Group> groupSet = new HashSet<Group>(groupDAO.findByDTOList(aApplicationUrlDTO.getAccessRight()));

                applicationUrl.setAccessRight(groupSet);
                applicationUrl.setApplicationDescription(aApplicationUrlDTO.getApplicationDescription());
                applicationUrl.setApplicationName(aApplicationUrlDTO.getApplicationName());
                applicationUrl.setColor(aApplicationUrlDTO.getColor());
                applicationUrl.setDisplayName(aApplicationUrlDTO.getDisplayName());
                applicationUrl.setDisplayOrder(aApplicationUrlDTO.getDisplayOrder());
                applicationUrl.setUrl(aApplicationUrlDTO.getUrl());

                applicationUrlDAO.persist(applicationUrl);
            } else {
                throw new ApplicationUrlNotFoundException(aApplicationUrlDTO.getId());
            }
        } else {
            throw new ApplicationUrlNotFoundException();
        }
    }

    @Override
    public void addApplicationURL(ApplicationUrlDTO aApplicationUrlDTO, Long aUrlGroupId) throws DKVBusinessException {
        ApplicationUrl applicationUrl = ApplicationUrlMapper.mapOntoEntity(aApplicationUrlDTO, new ApplicationUrl());
        Set<Group> groupSet = new HashSet<Group>(groupDAO.findByDTOList(aApplicationUrlDTO.getAccessRight()));
        applicationUrl.setAccessRight(groupSet);
        UrlGroup urlGroup = urlGroupDAO.findById(aUrlGroupId);
        applicationUrl.setUrlGroup(urlGroup);
        applicationUrlDAO.persist(applicationUrl);
    }

    @Override
    public void removeApplicationUrl(Long aId) {
        ApplicationUrl applicationUrl = applicationUrlDAO.findById(aId);
        if (applicationUrl != null) {
            applicationUrlDAO.remove(applicationUrl);
        }
    }

}
