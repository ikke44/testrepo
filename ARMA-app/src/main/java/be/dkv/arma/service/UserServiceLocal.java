package be.dkv.arma.service;

import java.util.List;

import javax.ejb.Local;

import be.dkv.arma.migration.model.UserGroupMigrationDTO;
import be.dkv.arma.model.UserDTO;
import be.dkv.arma.model.UserSearchCriteriaDTO;
import be.dkv.fw.exception.DKVBusinessException;

/**
 * The Interface UserServiceLocal.
 */
/**
 * @author dries
 *
 */
@Local
public interface UserServiceLocal extends UserServiceRemote {

    /**
     * Synchronizes the users of the SQL database with those on AD.
     *
     * @throws DKVBusinessException
     *             Synchronization produced an exception
     */
    void synchronizeUsers() throws DKVBusinessException;

    /**
     * Retrieves a list of all the users that satisfy the constraints.
     *
     * @param aUserSearchCriteria
     *            the search criteria
     * @return A list of user names
     * @throws DKVBusinessException
     *             the DKV business exception
     */
    List<UserDTO> findUsersBySearchCriteria(UserSearchCriteriaDTO aUserSearchCriteria) throws DKVBusinessException;

    /**
     * Adds the User to the specified groups.
     *
     * @param loginName
     *            the login name
     * @param groupNames
     *            the group names
     * @throws DKVBusinessException
     *             the DKV business exception
     */
    void addUserToGroups(String loginName, List<String> groupNames) throws DKVBusinessException;

    /**
     * Removes the User from the specified groups.
     *
     * @param loginName
     *            the login name
     * @param groupNames
     *            the group names
     * @throws DKVBusinessException
     *             the DKV business exception
     */
    void removeUserFromGroups(String loginName, List<String> groupNames) throws DKVBusinessException;

    /**
     * Find user group migration data.
     *
     * @param loginNames
     *            the login names
     * @param ignoredGroups List of groups to be ignored
     * @param findToBeRemovedFromLDAP boolean
     * @param findToBeAddedToLDAP boolean
     * @return the migration list
     * @throws DKVBusinessException
     *             the DKV business exception
     */
    List<UserGroupMigrationDTO> findUserGroupMigrationData(List<String> loginNames, List<String> ignoredGroups, boolean findToBeAddedToLDAP,
            boolean findToBeRemovedFromLDAP) throws DKVBusinessException;

    /**
     * @return The status of the user if archive is enabled (true) or not
     *         (false).
     * @throws DKVBusinessException
     *             the DKV business exception
     */
    Boolean isArchiveEnabled() throws DKVBusinessException;

    void acceptArchiving();

}
