package be.dkv.arma.service;

import javax.ejb.Local;

import be.dkv.arma.model.ApplicationUrlDTO;
import be.dkv.fw.exception.DKVBusinessException;

/**
 * The Interface ApplicationUrlServiceLocal.
 */
@Local
public interface ApplicationUrlServiceLocal {

    /**
     * Update an applicationUrl using a DTO.
     *
     * @param aApplicationUrlDTO the ApplicationUrlDTO
     * @throws DKVBusinessException the DKV business exception
     */
    void updateApplicationURL(ApplicationUrlDTO aApplicationUrlDTO) throws DKVBusinessException;

    /**
     * Adds an applicationUrl using a DTO.
     *
     * @param aApplicationUrlDTO the ApplicationUrlDTO
     * @param aUrlGroupId the urlGroup id
     * @throws DKVBusinessException the DKV business exception
     */
    void addApplicationURL(ApplicationUrlDTO aApplicationUrlDTO, Long aUrlGroupId) throws DKVBusinessException;

    /**
     * Removes the applicationUrl from the database.
     *
     * @param aId the id of an applicationUrl
     */
    void removeApplicationUrl(Long aId);
}
