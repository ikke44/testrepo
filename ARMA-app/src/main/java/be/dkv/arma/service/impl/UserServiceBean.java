package be.dkv.arma.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.cxf.common.util.StringUtils;
import org.apache.log4j.Logger;

import be.dkv.arma.common.ARMAErrorMessages;
import be.dkv.arma.common.Constants;
import be.dkv.arma.dao.As400DAO;
import be.dkv.arma.dao.GroupDAO;
import be.dkv.arma.dao.UserDAO;
import be.dkv.arma.entity.Group;
import be.dkv.arma.entity.User;
import be.dkv.arma.exception.GroupNotFoundException;
import be.dkv.arma.exception.UserNotFoundException;
import be.dkv.arma.ldap.LDAPService;
import be.dkv.arma.mapper.GroupMapper;
import be.dkv.arma.mapper.SearchCriteriaMapper;
import be.dkv.arma.mapper.UserMapper;
import be.dkv.arma.migration.model.UserGroupMigrationDTO;
import be.dkv.arma.model.GroupDTO;
import be.dkv.arma.model.ImportSource;
import be.dkv.arma.model.UserDTO;
import be.dkv.arma.model.UserKey;
import be.dkv.arma.model.UserSearchCriteriaDTO;
import be.dkv.arma.service.ImportServiceLocal;
import be.dkv.arma.service.UserServiceLocal;
import be.dkv.arma.service.UserServiceRemote;
import be.dkv.fw.exception.DKVBusinessException;

/**
 * Returns user data which combines the user data on LDAP and on the SQL
 * database.
 *
 * @author vanhout
 */
@Stateless
public class UserServiceBean extends AbstractArmaBean implements UserServiceLocal, UserServiceRemote {

    private static final Logger LOG = Logger.getLogger(UserServiceBean.class);

    @EJB
    private UserDAO userDAO;

    @EJB
    private As400DAO as400DAO;

    @EJB
    private GroupDAO groupDAO;

    @EJB
    private LDAPService ldapService;

    @EJB
    private ImportServiceLocal importService;

    public UserServiceBean() {
        super(LOG);
    }

    /**
     * Returns a {@link UserDTO} which combines the user data on LDAP and on the
     * SQL database.
     *
     * @param aLoginName
     *            The user login
     * @return A UserDTO
     * @throws DKVBusinessException
     *             An exception
     */
    @Override
    public UserDTO findUserByLogin(final String aLoginName) throws DKVBusinessException {

        UserDTO returnedUser = null;
        UserDTO userDTOfromLDAP = null;
        User userFromSQL = null;
        if (aLoginName != null) {
            // find LDAP user
            userDTOfromLDAP = ldapService.findUserByLogin(aLoginName);
            // find SQL user
            userFromSQL = userDAO.findUserByLoginName(aLoginName);

            if (userFromSQL == null) {
                // User NOT FOUND in SQL and LDAP
                if (userDTOfromLDAP == null) {
                    throw new UserNotFoundException(aLoginName);
                }// User NOT FOUND in SQL, but FOUND in LDAP
                else {
                    // New empty user introduced in database
                    returnedUser = importNewUser(userDTOfromLDAP);
                }
            } else {
                // User FOUND in SQL, but NOT FOUND in LDAP
                if (userDTOfromLDAP == null) {
                    // remove the outdated User from SQL
                    LOG.warn("Removing User '" + aLoginName + "' from the SQL Database because it is outdated in Active Directory");
                    userDAO.remove(userFromSQL);
                    throw new UserNotFoundException(aLoginName);
                }// User FOUND in SQL and LDAP
                else {
                    returnedUser = userDTOfromLDAP;
                    UserMapper.mapFromUser(returnedUser, userFromSQL);
                }
            }
        } else {
            throw new UserNotFoundException(ARMAErrorMessages.NULL_ARG_PROVIDED);
        }
        return returnedUser;
    }

    /**
     * Returns a list of {@link UserDTO} which combines the user data on LDAP
     * and on the SQL database.
     *
     * @param aGroupId
     *            The group name
     * @return A list of UserDTOs
     * @throws DKVBusinessException
     *             An exception
     */
    @Override
    public List<UserDTO> findUsersInGroup(final String aGroupId) throws DKVBusinessException {
        if (LOG.isInfoEnabled()) {
            LOG.info("Finding users in group " + aGroupId);
        }
        if (aGroupId != null) {
            // If group exists, it will be present in the SQL database
            boolean isValidSQLGroup = groupDAO.existsByName(aGroupId);
            boolean isValidLDAPGroup = ldapService.isValidGroup(aGroupId);

            if (!isValidLDAPGroup) {
                if (isValidSQLGroup) {
                    LOG.fatal("ACTIVE DIRECTORY IS NOT UP TO DATE: The Group '" + aGroupId + "' is unknown via LDAP, but is present in role mapping on SQL!");
                } else {
                    throw new GroupNotFoundException(aGroupId);
                }
            }
            Set<UserDTO> foundUsers = new HashSet<UserDTO>();
            if (isValidSQLGroup) {
                // find SQL users
                foundUsers.addAll(enrichUserWithLDAPInformation(userDAO.findUsersByGroupIds(aGroupId)));
            }
            if (isValidLDAPGroup) {
                // find LDAP users
                foundUsers.addAll(enrichUserWithSQLInformation(ldapService.findUsersInGroup(aGroupId)));
            }
            return new ArrayList<UserDTO>(foundUsers);
        } else {
            throw new GroupNotFoundException(ARMAErrorMessages.NULL_ARG_PROVIDED);
        }
    }

    /**
     * @param userDTOfromLDAP
     * @return
     * @throws DKVBusinessException
     */
    private UserDTO importNewUser(UserDTO userDTOfromLDAP) throws DKVBusinessException {
        if (LOG.isInfoEnabled()) {
            LOG.info("Importing Unknown ARMAUser: " + userDTOfromLDAP.getLoginName());
        }
        UserDTO returnedUser;
        // New empty user introduced in database
        User userFromSQL = userDAO.createNewEmptyUser(userDTOfromLDAP.getLoginName(), userDTOfromLDAP.getUserNumber());

        // Maps all AS400 data onto the newly created user
        as400DAO.synchronizeUserWithAS400(userDTOfromLDAP.getLoginName());

        if (userFromSQL != null) {
            userDAO.refresh(userFromSQL);
            // userDAO.merge(userFromSQL);
        }
        // maps to the DTO
        returnedUser = UserMapper.mapFromUser(userDTOfromLDAP, userFromSQL);
        return returnedUser;
    }

    /**
     * Synchronizes the users of the SQL database with those on AD (with data
     * mapping from AS400).
     *
     * @throws DKVBusinessException
     *             Synchronization produced an exception
     */
    @Override
    public void synchronizeUsers() throws DKVBusinessException {

        // Fase 1: Remove Users, not in Active Directory.
        List<UserKey> userkeys = ldapService.findAllUserKeys();
        userDAO.removeUnlistedUsers(userkeys);

        // Fase 2: Create new users that didn't exist.
        for (UserKey userkey : userkeys) {
            Boolean userExists = userDAO.existsByLoginName(userkey.getLoginName());
            if (!userExists) {
                userDAO.createNewEmptyUser(userkey.getLoginName(), userkey.getUserNumber());
            }
        }
        // Fase 3: Maps AS400 data onto all ARMA users.
        as400DAO.synchronizeAllUsersWithAS400();

        // Fase 4: Add Public group to each user
        Group thePublicGroup = groupDAO.findByName(Constants.PUBLIC_GROUP_NAME);
        if (thePublicGroup == null) {
            GroupDTO publicGroupCreationDTO = new GroupDTO();
            publicGroupCreationDTO.setId(Constants.PUBLIC_GROUP_NAME);
            publicGroupCreationDTO.setDescription(Constants.PUBLIC_GROUP_DESCRIPTION);
            thePublicGroup = importService.importGroup(publicGroupCreationDTO, ImportSource.SQL);
        }

        for (User user : userDAO.findAll()) {
            Set<Group> userGroups = user.getGroups();
            if (userGroups == null) {
                userGroups = new HashSet<Group>(0);
            }
            userGroups.add(thePublicGroup);
            user.setGroups(userGroups);
        }

    }

    @Override
    public UserDTO findCurrentUser() throws DKVBusinessException {
        return findUserByLogin(getUsername());
    }

    @Override
    public Boolean isArchiveEnabled() throws DKVBusinessException {
        return userDAO.findUserByLoginName(getUsername()).isArchivingEnabled();
    }

    @Override
    public void acceptArchiving() {
        User u = userDAO.findUserByLoginName(getUsername());
        u.setArchiveAcceptedOn(Calendar.getInstance());
        u.setArchivingEnabled(true);
        userDAO.persist(u);
    }

    /**
     * Returns a {@link UserDTO} which combines the user data on LDAP and on the
     * SQL database.
     *
     * @param aUserNumber
     *            The user UserNumber
     * @return A UserDTO
     * @throws DKVBusinessException
     *             An exception
     */
    @Override
    public UserDTO findUserByUserNumber(String aUserNumber) throws DKVBusinessException {
        if (aUserNumber != null) {
            UserDTO userDTO = null;
            User user = userDAO.findUserByUserNumber(aUserNumber);
            if (user != null) {
                userDTO = findUserByLogin(user.getLoginName());
            } else {
                throw new UserNotFoundException(aUserNumber);
            }
            return userDTO;
        } else {
            throw new UserNotFoundException(ARMAErrorMessages.NULL_ARG_PROVIDED);
        }
    }

    /**
     * Returns a List of {@link UserDTO}s which combines the user data on LDAP
     * and on the SQL database.
     *
     * @return A UserDTO
     * @throws DKVBusinessException
     *             An exception
     */
    @Override
    public List<UserDTO> findUsersBySearchCriteria(UserSearchCriteriaDTO crit) throws DKVBusinessException {

        List<User> daoResults = userDAO.findUserBySearchCriteria(SearchCriteriaMapper.mapToDAO(crit));
        List<UserDTO> ldapResults = ldapService.findUserBySearchCriteria(SearchCriteriaMapper.mapToLDAP(crit));

        List<UserDTO> results = UserMapper.merge(daoResults, ldapResults, false);

        return results;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * be.dkv.arma.service.UserServiceLocal#addUserToGroups(java.lang.String,
     * java.util.List)
     */
    @Override
    public void addUserToGroups(String loginName, List<String> requestedGroupNames) throws DKVBusinessException {
        User aUser = userDAO.findUserByLoginName(loginName);
        if (aUser != null && !requestedGroupNames.isEmpty()) {
            Set<String> presentGroupNames = GroupMapper.mapToAccessRightList(GroupMapper.mapToDTO(aUser.getGroups()));
            presentGroupNames.addAll(requestedGroupNames);
            List<Group> newGroupList = groupDAO.findByNameList(presentGroupNames);
            aUser.setGroups(new HashSet<Group>(newGroupList));
            if (presentGroupNames.size() != newGroupList.size()) {
                // not all requested groups have been added!
                for (Group group : newGroupList) {
                    requestedGroupNames.remove(group.getName());
                }
            }
        } else {
            throw new UserNotFoundException(loginName);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * be.dkv.arma.service.UserServiceLocal#removeUserFromGroups(java.lang.String
     * , java.util.List)
     */
    @Override
    public void removeUserFromGroups(String loginName, List<String> requestedGroupNames) throws DKVBusinessException {
        User aUser = userDAO.findUserByLoginName(loginName);
        if (aUser != null && !requestedGroupNames.isEmpty()) {
            List<Group> groupsToBeRemoved = groupDAO.findByNameList(requestedGroupNames);
            aUser.getGroups().removeAll(groupsToBeRemoved);
        } else {
            throw new UserNotFoundException(loginName);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * be.dkv.arma.service.UserServiceRemote#findUsersbyLoginNames(java.util
     * .List, java.lang.Boolean)
     */
    @Override
    public List<UserDTO> findUsersbyLoginNames(List<String> userNameList, Boolean displayGroupInfo) throws DKVBusinessException {
        // Remove duplicate user names from request
        Set<String> uniqueUserIds = new LinkedHashSet<String>(userNameList);
        userNameList.clear();
        userNameList.addAll(uniqueUserIds);

        // By using a Set, duplicates are eliminated
        Set<UserDTO> foundUsers = new HashSet<UserDTO>();
        for (String aLoginName : userNameList) {
            if (!StringUtils.isEmpty(aLoginName)) {
                UserDTO foundUser = findUserByLogin(aLoginName);
                foundUsers.add(foundUser);
            }
        }
        if (!displayGroupInfo) {
            removeGroupInfo(foundUsers);
        }
        return new ArrayList<UserDTO>(foundUsers);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * be.dkv.arma.service.UserServiceRemote#findUsersInGroups(java.util.List,
     * java.lang.Boolean)
     */
    @Override
    public List<UserDTO> findUsersInGroups(List<String> groupIdList, Boolean displayGroupInfo) throws DKVBusinessException {
        // Remove duplicate group names from request
        Set<String> uniqueGroupIds = new LinkedHashSet<String>(groupIdList);
        groupIdList.clear();
        groupIdList.addAll(uniqueGroupIds);

        // By using a Set, user duplicates are eliminated
        Set<UserDTO> foundUsers = new HashSet<UserDTO>();
        for (String aGroupId : groupIdList) {
            if (!StringUtils.isEmpty(aGroupId)) {
                List<UserDTO> foundUserlist = findUsersInGroup(aGroupId);
                if (foundUserlist != null && !foundUserlist.isEmpty()) {
                    foundUsers.addAll(foundUserlist);
                }
            }
        }
        if (!displayGroupInfo) {
            removeGroupInfo(foundUsers);
        }
        return new ArrayList<UserDTO>(foundUsers);
    }

    private void removeGroupInfo(Collection<UserDTO> userList) {
        for (UserDTO userDTO : userList) {
            userDTO.setGroups(new ArrayList<GroupDTO>());
        }
        return;
    }

    private List<UserDTO> enrichUserWithLDAPInformation(List<User> aSQLUserList) throws DKVBusinessException {
        List<UserDTO> enrichedUsers = new ArrayList<UserDTO>();
        for (User user : aSQLUserList) {
            UserDTO aUserDTO = ldapService.findUserByLogin(user.getLoginName());
            enrichedUsers.add(UserMapper.mapFromUser(aUserDTO, user));
        }
        return enrichedUsers;
    }

    private List<UserDTO> enrichUserWithSQLInformation(List<UserDTO> anLDAPUserList) throws DKVBusinessException {
        List<UserDTO> enrichedUsers = new ArrayList<UserDTO>();
        if (anLDAPUserList != null) {
            for (UserDTO userDTOfromLDAP : anLDAPUserList) {
                final String loginName = userDTOfromLDAP.getLoginName();
                User user = userDAO.findUserByLoginName(loginName);
                if (user != null) {
                    userDTOfromLDAP = UserMapper.mapFromUser(userDTOfromLDAP, user);
                } else {
                    // New empty user introduced in database
                    userDTOfromLDAP = importNewUser(userDTOfromLDAP);
                }
                enrichedUsers.add(userDTOfromLDAP);
            }
        }
        return enrichedUsers;

    }

    @Override
    public List<UserGroupMigrationDTO> findUserGroupMigrationData(List<String> loginNames, List<String> ignoredGroups, boolean findToBeAddedToLDAP,
            boolean findToBeRemovedFromLDAP) throws DKVBusinessException {

        List<UserGroupMigrationDTO> migData = new ArrayList<UserGroupMigrationDTO>();

        List<String> requestedLoginNames;
        if (loginNames != null && !loginNames.isEmpty()) {
            requestedLoginNames = loginNames;
        } else {
            LOG.info("findUserGroupMigrationData - no loginNames provided, requesting all of them.");
            requestedLoginNames = ldapService.findAllUserNames();
        }

        for (String aLoginName : requestedLoginNames) {
            UserGroupMigrationDTO migDataEntry = getUserMigrationData(ignoredGroups, findToBeAddedToLDAP, findToBeRemovedFromLDAP, migData, aLoginName);
            if (migDataEntry.needsMigrationActions()) {
                migData.add(migDataEntry);
            }
        }
        return migData;
    }

    private UserGroupMigrationDTO getUserMigrationData(List<String> ignoredGroups, boolean findToBeAddedToLDAP, boolean findToBeRemovedFromLDAP,
            List<UserGroupMigrationDTO> migData, String aLoginName) throws DKVBusinessException {

        UserGroupMigrationDTO migDataEntry = new UserGroupMigrationDTO();
        migDataEntry.setUserName(aLoginName);
        UserDTO userDTOfromLDAP = null;
        User userFromSQL = null;
        // find LDAP user
        userDTOfromLDAP = ldapService.findUserByLogin(aLoginName);
        if (userDTOfromLDAP != null) {
            // find SQL user
            userFromSQL = userDAO.findUserByLoginName(aLoginName);
            if (userFromSQL != null) {

                Set<String> currentLDAPGroups = getCurrentLDAPGroups(userDTOfromLDAP);
                Set<String> currentSQLGroups = getCurrentSQLGroups(userFromSQL);

                if (findToBeRemovedFromLDAP) {
                    migDataEntry.setGroupsToBeRemovedFromLDAP(findRemovableFromLDAP(ignoredGroups, currentLDAPGroups, currentSQLGroups, migDataEntry));
                }

                if (findToBeAddedToLDAP) {
                    migDataEntry.setGroupsToBeAddedToLDAP(findAddableToLDAP(ignoredGroups, currentLDAPGroups, currentSQLGroups, migDataEntry));
                }
            }

        }
        return migDataEntry;
    }

    private List<String> findAddableToLDAP(List<String> ignoredGroups, Set<String> currentLDAPGroups, Set<String> currentSQLGroups,
            UserGroupMigrationDTO migDataEntry) {
        Set<String> toBeAdded = new HashSet<String>();
        for (String sqlGroup : currentSQLGroups) {
            if (!currentLDAPGroups.contains(sqlGroup) && ignoredGroups != null && !ignoredGroups.contains(sqlGroup)) {
                toBeAdded.add(sqlGroup);
            }
        }
        return new ArrayList<String>(toBeAdded);
    }

    private List<String> findRemovableFromLDAP(List<String> ignoredGroups, Set<String> currentLDAPGroups, Set<String> currentSQLGroups,
            UserGroupMigrationDTO migDataEntry) {
        Set<String> toBeRemoved = new HashSet<String>();
        for (String ldapGroup : currentLDAPGroups) {
            if (!currentSQLGroups.contains(ldapGroup) && ignoredGroups != null && !ignoredGroups.contains(ldapGroup)) {
                toBeRemoved.add(ldapGroup);
            }
        }
        return new ArrayList<String>(toBeRemoved);
    }

    private Set<String> getCurrentLDAPGroups(UserDTO userDTOfromLDAP) {
        Set<String> currentLDAPGroups = new HashSet<String>();
        for (GroupDTO ldapGroupDTO : userDTOfromLDAP.getGroups()) {
            currentLDAPGroups.add(ldapGroupDTO.getId());
        }
        return currentLDAPGroups;
    }

    private Set<String> getCurrentSQLGroups(User userFromSQL) {
        Set<String> currentSQLGroups = new HashSet<String>();
        for (Group sqlGroup : userFromSQL.getGroups()) {
            currentSQLGroups.add(sqlGroup.getName());
        }
        return currentSQLGroups;
    }
}
