package be.dkv.arma.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;

import be.dkv.arma.dao.ApplicationUrlDAO;
import be.dkv.arma.dao.GenericDaoBean;
import be.dkv.arma.dao.GroupDAO;
import be.dkv.arma.dao.UrlGroupDAO;
import be.dkv.arma.dao.query.FindApplicationUrlByAccessRights;
import be.dkv.arma.dao.query.FindEmptyUrlGroups;
import be.dkv.arma.entity.ApplicationUrl;
import be.dkv.arma.entity.Group;
import be.dkv.arma.entity.UrlGroup;
import be.dkv.arma.ldap.LDAPService;
import be.dkv.arma.mapper.ApplicationUrlMapper;
import be.dkv.arma.mapper.GroupMapper;
import be.dkv.arma.mapper.UrlGroupMapper;
import be.dkv.arma.model.ApplicationUrlDTO;
import be.dkv.arma.model.UrlGroupDTO;
import be.dkv.arma.model.UserDTO;
import be.dkv.arma.service.GroupServiceLocal;
import be.dkv.arma.service.UrlGroupServiceLocal;
import be.dkv.arma.service.UserServiceLocal;
import be.dkv.fw.exception.DKVBusinessException;

@Stateless
public class UrlGroupServiceBean extends AbstractArmaBean implements UrlGroupServiceLocal {

    public UrlGroupServiceBean() {
        super(Logger.getLogger(UrlGroupServiceBean.class));
    }

    @EJB
    private LDAPService ldapService;

    @EJB
    private UserServiceLocal userService;

    @EJB
    private GroupServiceLocal groupService;

    @EJB
    private GroupDAO groupDAO;

    @EJB
    protected GenericDaoBean genericDao;
    
    @EJB
    private UrlGroupDAO urlGroupDAO;

    @EJB
    private ApplicationUrlDAO applicationUrlDAO;

    @Override
    public List<UrlGroupDTO> getUrlGroupsForCurrentUser() throws DKVBusinessException {
        UserDTO currentUser = userService.findUserByLogin(getUsername());
        List<UrlGroupDTO> urlGroupDTOs = new ArrayList<UrlGroupDTO>();

        if (currentUser != null && currentUser.getGroups() != null) {
            Set<String> accessRights = GroupMapper.mapToAccessRightList(currentUser.getGroups());
            if (!accessRights.isEmpty()) {
                for (ApplicationUrl applicationUrl : genericDao.get(new FindApplicationUrlByAccessRights(accessRights))) {
                    UrlGroupDTO urlGroupDTO = UrlGroupMapper.map(applicationUrl.getUrlGroup());
                    if (urlGroupDTOs.contains(urlGroupDTO)) {
                        urlGroupDTOs.get(urlGroupDTOs.indexOf(urlGroupDTO)).getApplicationUrlDTO()
                                .add(ApplicationUrlMapper.map(applicationUrl));
                    } else {
                        urlGroupDTO.setApplicationUrlDTO(new ArrayList<ApplicationUrlDTO>());
                        urlGroupDTO.getApplicationUrlDTO().add(ApplicationUrlMapper.map(applicationUrl));
                        urlGroupDTOs.add(urlGroupDTO);
                    }
                }
            }
        }

        return urlGroupDTOs;
    }

    @Override
    public List<UrlGroupDTO> getUrlGroupsForModification() throws DKVBusinessException {
        List<UrlGroupDTO> urlGroupDTOs = new ArrayList<UrlGroupDTO>();

        for (ApplicationUrl applicationUrl : applicationUrlDAO.findAll()) {
            UrlGroupDTO urlGroupDTO = UrlGroupMapper.map(applicationUrl.getUrlGroup());
            if (urlGroupDTOs.contains(urlGroupDTO)) {
                urlGroupDTOs.get(urlGroupDTOs.indexOf(urlGroupDTO)).getApplicationUrlDTO()
                        .add(ApplicationUrlMapper.map(applicationUrl));
            } else {
                urlGroupDTO.setApplicationUrlDTO(new ArrayList<ApplicationUrlDTO>());
                urlGroupDTO.getApplicationUrlDTO().add(ApplicationUrlMapper.map(applicationUrl));
                urlGroupDTOs.add(urlGroupDTO);
            }
        }

        // Also add unlinked groups so they can be edited
        List<UrlGroup> unlinkedUrlGroups =  genericDao.get(new FindEmptyUrlGroups());
        for (UrlGroup urlGroup : unlinkedUrlGroups) {
            urlGroupDTOs.add(UrlGroupMapper.map(urlGroup));
        }

        return urlGroupDTOs;
    }

    @Override
    public void updateUrlGroup(UrlGroupDTO aUrlGroupDTO) {
        UrlGroup urlGroup = urlGroupDAO.findById(aUrlGroupDTO.getId());
        urlGroup.setColor(aUrlGroupDTO.getColor());
        urlGroup.setDisplayName(aUrlGroupDTO.getDisplayName());
        urlGroup.setDisplayOrder(aUrlGroupDTO.getDisplayOrder());
        urlGroupDAO.persist(urlGroup);

        if (urlGroup.getApplicationUrls() != null && !urlGroup.getApplicationUrls().isEmpty()) {
            // update existing ApplicationUrls if already present on database
            List<Long> appUrlIds = new ArrayList<Long>();
            for (ApplicationUrl appUrl : urlGroup.getApplicationUrls()) {
                appUrlIds.add(appUrl.getId());
            }

            List<ApplicationUrl> appUrls = applicationUrlDAO.findAll(appUrlIds);

            for (ApplicationUrl appUrl : appUrls) {
                for (ApplicationUrlDTO appUDTO : aUrlGroupDTO.getApplicationUrlDTO()) {
                    if (appUDTO.getId() != null && appUDTO.getId().equals(appUrl.getId())) {
                        ApplicationUrlMapper.mapOntoEntity(appUDTO, appUrl);
                        applicationUrlDAO.persist(appUrl);
                    }
                }
            }
        }
    }

    @Override
    public UrlGroupDTO addUrlGroup(UrlGroupDTO aUrlGroupDTO) throws DKVBusinessException {
        UrlGroup urlGroup = UrlGroupMapper.map(aUrlGroupDTO);

        urlGroupDAO.persist(urlGroup);

        if (aUrlGroupDTO.getApplicationUrlDTO() != null) {
            List<ApplicationUrl> applicationUrls = new ArrayList<ApplicationUrl>();
            for (ApplicationUrlDTO appUDTO : aUrlGroupDTO.getApplicationUrlDTO()) {
                ApplicationUrl applicationUrl = ApplicationUrlMapper.mapOntoEntity(appUDTO, new ApplicationUrl());
                Set<Group> groupSet = new HashSet<Group>(groupDAO.findByDTOList(appUDTO.getAccessRight()));
                applicationUrl.setAccessRight(groupSet);
                applicationUrl.setUrlGroup(urlGroup);
                applicationUrlDAO.persist(applicationUrl);
                applicationUrls.add(applicationUrl);
                urlGroup.getApplicationUrls().add(applicationUrl);
                
            }
        }
        urlGroupDAO.flush();
        return UrlGroupMapper.map(urlGroup);
    }

    @Override
    public void removeUrlGroup(Long aId) {
        UrlGroup urlGroup = urlGroupDAO.findById(aId);
        for (ApplicationUrl applicationUrl : urlGroup.getApplicationUrls()) {
            applicationUrlDAO.remove(applicationUrl);
        }
        urlGroupDAO.remove(urlGroup);
    }

    @Override
    public List<UrlGroupDTO> findAll() {
        return UrlGroupMapper.mapAllToDTO(urlGroupDAO.findAll());
    }

}
