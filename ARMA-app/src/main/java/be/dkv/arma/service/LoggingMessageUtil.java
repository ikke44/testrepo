package be.dkv.arma.service;
/**
 * Represents a utility class for creating logging messages.
 *
 * @author geenens
 */
public final class LoggingMessageUtil {

    private LoggingMessageUtil() {
    }

    /**
     * Creates the logging message for the elapsed time.
     *
     * @param aStartTime
     *            The start time in milliseconds.
     * @param aEndTime
     *            The end time in milliseconds.
     * @return The logging message.
     */
    public static String createElapsedTimeMessage(long aStartTime, long aEndTime) {
        return (aEndTime - aStartTime) + "ms";
    }
}
