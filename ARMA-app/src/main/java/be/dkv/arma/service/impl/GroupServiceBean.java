package be.dkv.arma.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;

import be.dkv.arma.dao.GroupDAO;
import be.dkv.arma.entity.Group;
import be.dkv.arma.ldap.LDAPService;
import be.dkv.arma.mapper.GroupMapper;
import be.dkv.arma.model.GroupDTO;
import be.dkv.arma.model.ImportSource;
import be.dkv.arma.service.GroupServiceLocal;
import be.dkv.arma.service.GroupServiceRemote;
import be.dkv.arma.service.ImportServiceLocal;
import be.dkv.fw.exception.DKVBusinessException;

/**
 * Returns group data which combines the group data on LDAP and on the SQL
 * database.
 *
 * @author dobbelae
 */
@Stateless
public class GroupServiceBean extends AbstractArmaBean implements GroupServiceLocal, GroupServiceRemote {

    private static final Logger LOG = Logger.getLogger(GroupServiceBean.class);

    /** The Constant GET_GROUPS_FROM_SQL. */
    private static final boolean GET_GROUPS_FROM_SQL = true;

    /** The Constant GET_GROUPS_FROM_LDAP. */
    private static final boolean GET_GROUPS_FROM_LDAP = true;

    @EJB
    private GroupDAO groupDAO;

    @EJB
    private LDAPService ldapService;
    
    @EJB
    private ImportServiceLocal importService;

    public GroupServiceBean() {
        super(LOG);
    }

    /**
     * Finds all unique groups on AD and the SQL database as a {@link List} of
     * {@link GroupDTO}s. Internally, two {@link List}s of {@link GroupDTO}s are
     * merged them into a new {@link List} based on their GroupIds. When doubles
     * are found, this method will prioritize the {@link GroupDTO} Objects found
     * in the SQL database as they are able to contain a description.
     *
     * @return A {@link List} of {@link GroupDTO}s
     */
    @Override
    public List<GroupDTO> findAllGroups() {
        if (LOG.isInfoEnabled()) {
            LOG.info("Finding all groups");
        }

        List<GroupDTO> groupDTOs = null;
        List<GroupDTO> ldapGroupDTOs = null;
        List<GroupDTO> sqlGroupDTOs = null;

        // BASED ON LDAP
        if (GET_GROUPS_FROM_LDAP) {
            ldapGroupDTOs = findAllGroupDTOsOnLDAP();
        } else {
            ldapGroupDTOs = new ArrayList<GroupDTO>();
        }
        // BASED ON SQL
        if (GET_GROUPS_FROM_SQL) {
            sqlGroupDTOs = findAllGroupDTOsOnSQLDatabase();
        } else {
            sqlGroupDTOs = new ArrayList<GroupDTO>();
        }
        // MERGE BOTH LISTS
        try {
            groupDTOs = GroupMapper.mergeToUniqueList(ldapGroupDTOs, sqlGroupDTOs);
        } catch (DKVBusinessException e) {
            LOG.warn("Not All Groups were obtained. Error: " + e.getErrorCode());
        }
        return groupDTOs;
    }

    /**
     * Retrieves a list of all the possible groupnames.
     *
     * @return A list of groupnames.
     */
    @Override
    public List<String> findAllGroupNames() {
        List<String> uniqueGroupNames = new ArrayList<String>();
        for (GroupDTO uniqueGroup : findAllGroups()) {
            uniqueGroupNames.add(uniqueGroup.getId());
        }
        return uniqueGroupNames;
    }

    private List<GroupDTO> findAllGroupDTOsOnLDAP() {
        List<GroupDTO> groupDTOs = new ArrayList<GroupDTO>();
        try {
            groupDTOs = ldapService.findAllGroups();
        } catch (DKVBusinessException e) {
            LOG.fatal("The ldapService couldn't retrieve a valid result for the findAllGroups query.");
        }
        return groupDTOs;
    }

    private List<GroupDTO> findAllGroupDTOsOnSQLDatabase() {
        List<GroupDTO> groupDTOs = new ArrayList<GroupDTO>();
        List<Group> groups = null;
        try {
            groups = groupDAO.findAllGroups();
        } catch (Exception e) {
            LOG.fatal("The groupDAO couldn't retrieve a valid result for the findAllGroups query.");
        }
        if (groups != null && !groups.isEmpty()) {
            for (Group group : groups) {
                groupDTOs.add(GroupMapper.map(group));
            }
        }

        return groupDTOs;
    }

    /**
     * Synchronizes the groups of the SQL database with those on AD. This will
     * remove unused groups if their import source was AD
     *
     * @throws DKVBusinessException
     *             Synchronization produced an exception
     */
    @Override
    public void synchronizeGroups() throws DKVBusinessException {

        List<GroupDTO> sqlGroupDTOs = null;
        List<GroupDTO> ldapGroupDTOs = null;
        // [Fase 1]: Find Groups in Active Directory (LDAP) and SQL.
        ldapGroupDTOs = findAllGroupDTOsOnLDAP();
        sqlGroupDTOs = findAllGroupDTOsOnSQLDatabase();

        //
        HashSet<String> sqlGroupNamesNotInLDAP = new HashSet<String>();
        for (GroupDTO sqlGroupDTO : sqlGroupDTOs) {
            sqlGroupNamesNotInLDAP.add(sqlGroupDTO.getId());
        }

        // [Fase 2]: Import the Data found on LDAP
        for (GroupDTO ldapGroupDTO : ldapGroupDTOs) {
            importService.importGroup(ldapGroupDTO, ImportSource.LDAP);
            if (sqlGroupNamesNotInLDAP.contains(ldapGroupDTO.getId())) {
                sqlGroupNamesNotInLDAP.remove(ldapGroupDTO.getId());
            }
        }
        // ------- Now, the variable groupIdentifiersFromGroupsInSQL contains
        // the groups in SQL that are not found in LDAP.
        // [Fase 3]: Remove Groups, that didn't exist in LDAP, but exist in SQL.
        // (only those previously imported from the LDAP import source)
        groupDAO.removeGroups(new ArrayList<String>(sqlGroupNamesNotInLDAP), ImportSource.LDAP);
    }

    @Override
    public void addGroupInSQL(GroupDTO aGroup) throws DKVBusinessException {
        importService.importGroup(aGroup, ImportSource.SQL);
    }

    @Override
    public void removeGroupsFromSQL(List<String> groupIds) throws DKVBusinessException {
        groupDAO.removeGroups(groupIds, ImportSource.SQL);
    }
    
    
    

}
