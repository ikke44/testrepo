package be.dkv.arma.ws;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebService;

import org.apache.log4j.Logger;

import be.dkv.arma.common.Constants;
import be.dkv.arma.settings.SettingManager;
import be.dkv.fw.manager.AbstractSettingManager;
import be.dkv.fw.ws.AbstractSettingWS;

@WebService(serviceName = Constants.SERVICE_NAME, targetNamespace = Constants.TARGET_NAMESPACE)
@Stateless
public class SettingWS extends AbstractSettingWS {

	@EJB
	private SettingManager settingManager;

    private static Logger LOG = Logger.getLogger(SettingWS.class);

	public SettingWS() {
        super(LOG);
	}

	@Override
	protected AbstractSettingManager getSettingManager() {
		return settingManager;
	}

}
