package be.dkv.arma.ws;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.apache.log4j.Logger;

import be.dkv.arma.common.ARMAErrorMessages;
import be.dkv.arma.common.Constants;
import be.dkv.arma.exception.UserNotFoundException;
import be.dkv.arma.model.GroupDTO;
import be.dkv.arma.model.IdListRequest;
import be.dkv.arma.model.IdRequest;
import be.dkv.arma.model.UserDTO;
import be.dkv.arma.model.UserSearchCriteriaDTO;
import be.dkv.arma.model.VoidRequest;
import be.dkv.arma.result.GroupListResult;
import be.dkv.arma.result.UserListResult;
import be.dkv.arma.result.UserResult;
import be.dkv.arma.service.GroupServiceLocal;
import be.dkv.arma.service.LoggingMessageUtil;
import be.dkv.arma.service.UserServiceLocal;
import be.dkv.fw.dto.VoidResult;
import be.dkv.fw.exception.DKVBusinessException;
import be.dkv.fw.ws.AbstractWebService;
import be.dkv.fw.ws.WebserviceCallHandler;

// TODO: Auto-generated Javadoc
/**
 * Service bean and web service for retrieval of Active Directory user and group
 * data.
 *
 * @author vanhout
 *
 */
@WebService(serviceName = Constants.SERVICE_NAME, targetNamespace = Constants.TARGET_NAMESPACE)
// @HandlerChain(file = "/META-INF/server-handlers.xml")
// @SchemaValidation
@Stateless
public class ArmaCoreWebServiceBean extends AbstractWebService {

    /** The name of a void request parameter. */
    public static final String VOID_REQUEST = "voidRequest";
    /** the name of the groupId parameter. */
    public static final String GROUP_ID = "GroupName";
    /** the name of the user loginName parameter. */
    public static final String LOGIN_NAME = "UserName";
    /** the name of the user loginName parameter. */
    public static final String USER_NUMBER = "UserNumber";
    /** the name of the searchCriteria parameter. */
    public static final String SEARCH_CRITERIA = "SearchCriteria";
    /** the name of the groupIds parameter. */
    public static final String GROUP_IDS = "GroupNames";
    /** the name of the user loginNames parameter. */
    public static final String LOGIN_NAMES = "UserNames";
    /** the name of the displayGroupInfo parameter. */
    public static final String DISPLAY_GROUP_INFO = "displayGroupInfo";

    /** The Constant LOG. */
    private static final Logger LOG = Logger.getLogger(ArmaCoreWebServiceBean.class);

    /** The user service. */
    @EJB
    private UserServiceLocal userService;

    /** The group service. */
    @EJB
    private GroupServiceLocal groupService;

    /**
     * Instantiates a new arma core web service bean.
     */
    public ArmaCoreWebServiceBean() {
        super(LOG);
    }

    /**
     * Find a user based on his user login.
     *
     * @param aIdRequest the id
     * @return the user result
     */
    @WebMethod
    public UserResult findUserByLogin(@WebParam(name = ArmaCoreWebServiceBean.LOGIN_NAME) final IdRequest aIdRequest) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("findUserByLogin");
        }
        long startTime = (new Date()).getTime();
        UserResult result =
                this.webserviceTemplate(UserResult.class, userService,
                        new WebserviceCallHandler<UserResult, UserServiceLocal>() {
                            @Override
                            public void handle(UserResult aResult, UserServiceLocal aService)
                                    throws DKVBusinessException {
                                if (aIdRequest == null) {
                                    throw new UserNotFoundException(ARMAErrorMessages.NULL_ARG_PROVIDED);
                                } else {
                                    try {
                                        UserDTO userDTO = userService.findUserByLogin(aIdRequest.getId());
                                        aResult.setValue(userDTO);
                                    } catch (UserNotFoundException e) {
                                        UserDTO aValue = null;
                                        aResult.setValue(aValue);
                                        aResult.setResultCode(e.getErrorResultCode());
                                    }
                                }
                            }
                        });
        if (LOG.isInfoEnabled()) {
            LOG.info("findUserByLogin.elapsedTime "
                    + LoggingMessageUtil.createElapsedTimeMessage(startTime, (new Date()).getTime()));
        }
        return result;
    }

    /**
     * Find a user based on his UserNumber.
     *
     * @param aIdRequest the id
     * @return the user result
     */
    @WebMethod
    public UserResult findUserByUserNumber(
            @WebParam(name = ArmaCoreWebServiceBean.USER_NUMBER) final IdRequest aIdRequest) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("findUserByUserNumber");
        }
        long startTime = (new Date()).getTime();
        UserResult result =
                this.webserviceTemplate(UserResult.class, userService,
                        new WebserviceCallHandler<UserResult, UserServiceLocal>() {
                            @Override
                            public void handle(UserResult aResult, UserServiceLocal aService)
                                    throws DKVBusinessException {
                                try {
                                    UserDTO userDTO = userService.findUserByUserNumber(aIdRequest.getId());
                                    aResult.setValue(userDTO);
                                } catch (UserNotFoundException e) {
                                    UserDTO aValue = null;
                                    aResult.setValue(aValue);
                                    aResult.setResultCode(e.getErrorResultCode());
                                }
                            }
                        });
        if (LOG.isInfoEnabled()) {
            LOG.info("findUserByUserNumber.elapsedTime "
                    + LoggingMessageUtil.createElapsedTimeMessage(startTime, (new Date()).getTime()));
        }
        return result;
    }

    /**
     * Find a list of users based on the group id.
     *
     * @param aIdRequest id
     * @return the user list result
     */
    @WebMethod
    public UserListResult
            findUsersInGroup(@WebParam(name = ArmaCoreWebServiceBean.GROUP_ID) final IdRequest aIdRequest) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("findUsersInGroup");
        }
        long startTime = (new Date()).getTime();
        UserListResult result =
                this.webserviceTemplate(UserListResult.class, userService,
                        new WebserviceCallHandler<UserListResult, UserServiceLocal>() {
                            @Override
                            public void handle(UserListResult aResult, UserServiceLocal aService)
                                    throws DKVBusinessException {
                                List<UserDTO> userDTOs = aService.findUsersInGroup(aIdRequest.getId());
                                aResult.setValue(userDTOs);
                            }
                        });
        if (LOG.isInfoEnabled()) {
            LOG.info("findUsersInGroup.elapsedTime "
                    + LoggingMessageUtil.createElapsedTimeMessage(startTime, (new Date()).getTime()));
        }
        return result;
    }

    /**
     * Find users by search criteria.
     *
     * @param userSearchCriteria the user search criteria
     * @return list of all the users that satisfy the constraints.
     */
    @WebMethod
    public UserListResult findUsersBySearchCriteria(
            @WebParam(name = ArmaCoreWebServiceBean.SEARCH_CRITERIA) final UserSearchCriteriaDTO userSearchCriteria) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("findUsersBySearchCriteria");
        }
        long startTime = (new Date()).getTime();
        UserListResult result =
                this.webserviceTemplate(UserListResult.class, userService,
                        new WebserviceCallHandler<UserListResult, UserServiceLocal>() {
                            @Override
                            public void handle(UserListResult aResult, UserServiceLocal aService)
                                    throws DKVBusinessException {
                                aResult.setValue(aService.findUsersBySearchCriteria(userSearchCriteria));
                            }
                        });
        if (LOG.isInfoEnabled()) {
            LOG.info("findUsersBySearchCriteria.elapsedTime "
                    + LoggingMessageUtil.createElapsedTimeMessage(startTime, (new Date()).getTime()));
        }
        return result;
    }

    /**
     * Finds all unique groups on AD and the SQL database as a {@link List} of
     * {@link GroupDTO}s. Internally, two {@link List}s of {@link GroupDTO}s are
     * merged them into a new {@link List} based on their GroupIds. When doubles
     * are found, this method will prioritize the {@link GroupDTO} Objects found
     * in the SQL database as they are able to contain a description.
     *
     * @param aVoidRequest the a void request
     * @return the group list result
     */
    @WebMethod
    public GroupListResult findAllGroups(
            @WebParam(name = ArmaCoreWebServiceBean.VOID_REQUEST) final VoidRequest aVoidRequest) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("findAllGroups");
        }
        long startTime = (new Date()).getTime();
        GroupListResult result =
                this.webserviceTemplate(GroupListResult.class, groupService,
                        new WebserviceCallHandler<GroupListResult, GroupServiceLocal>() {
                            @Override
                            public void handle(GroupListResult aResult, GroupServiceLocal aService)
                                    throws DKVBusinessException {
                                List<GroupDTO> groupDTOs = aService.findAllGroups();
                                aResult.setValue(groupDTOs);
                            }
                        });
        if (LOG.isInfoEnabled()) {
            LOG.info("findAllGroups.elapsedTime "
                    + LoggingMessageUtil.createElapsedTimeMessage(startTime, (new Date()).getTime()));
        }
        return result;
    }

    /**
     * Synchronizes the users of the SQL database with those on AD.
     *
     * @param aVoidRequest the a void request
     * @return the void result
     */
    @WebMethod
    public VoidResult synchronizeUsers(
            @WebParam(name = ArmaCoreWebServiceBean.VOID_REQUEST) final VoidRequest aVoidRequest) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("synchronizeUsers");
        }
        long startTime = (new Date()).getTime();
        VoidResult result =
                this.webserviceTemplate(VoidResult.class, userService,
                        new WebserviceCallHandler<VoidResult, UserServiceLocal>() {
                            @Override
                            public void handle(VoidResult aResult, UserServiceLocal aService)
                                    throws DKVBusinessException {
                                userService.synchronizeUsers();
                            }
                        });
        if (LOG.isInfoEnabled()) {
            LOG.info("synchronizeUsers.elapsedTime "
                    + LoggingMessageUtil.createElapsedTimeMessage(startTime, (new Date()).getTime()));
        }
        return result;
    }

    /**
     * Synchronizes the groups of the SQL database with those on AD. This will remove unused groups if their import source was AD
     *
     * @param aVoidRequest the a void request
     * @return the void result
     */
    @WebMethod
    public VoidResult synchronizeGroups(
            @WebParam(name = ArmaCoreWebServiceBean.VOID_REQUEST) final VoidRequest aVoidRequest) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("synchronizeGroups");
        }
        long startTime = (new Date()).getTime();
        VoidResult result =
                this.webserviceTemplate(VoidResult.class, groupService,
                        new WebserviceCallHandler<VoidResult, GroupServiceLocal>() {
                            @Override
                            public void handle(VoidResult aResult, GroupServiceLocal aService)
                                    throws DKVBusinessException {
                                groupService.synchronizeGroups();
                            }
                        });
        if (LOG.isInfoEnabled()) {
            LOG.info("synchronizeGroups.elapsedTime "
                    + LoggingMessageUtil.createElapsedTimeMessage(startTime, (new Date()).getTime()));
        }
        return result;
    }

    @WebMethod
    public UserListResult
    findUsersbyLoginNames(@WebParam(name = ArmaCoreWebServiceBean.LOGIN_NAMES) final IdListRequest aIdListRequest,
            @WebParam(name = ArmaCoreWebServiceBean.DISPLAY_GROUP_INFO) final boolean displayGroupInfo) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("findUsersbyLoginNames");
        }
        long startTime = (new Date()).getTime();
        UserListResult result =
                this.webserviceTemplate(UserListResult.class, userService,
                        new WebserviceCallHandler<UserListResult, UserServiceLocal>() {
                            @Override
                            public void handle(UserListResult aResult, UserServiceLocal aService)
                                    throws DKVBusinessException {
                                List<UserDTO> userDTOs = aService.findUsersbyLoginNames(aIdListRequest.getId(), displayGroupInfo);
                                aResult.setValue(userDTOs);
                            }
                        });
        if (LOG.isInfoEnabled()) {
            LOG.info("findUsersbyLoginNames.elapsedTime "
                    + LoggingMessageUtil.createElapsedTimeMessage(startTime, (new Date()).getTime()));
        }
        return result;
    }

    @WebMethod
    public UserListResult
    findUsersInGroups(@WebParam(name = ArmaCoreWebServiceBean.GROUP_IDS) final IdListRequest aIdListRequest,
            @WebParam(name = ArmaCoreWebServiceBean.DISPLAY_GROUP_INFO) final boolean displayGroupInfo) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("findUsersInGroups");
        }
        long startTime = (new Date()).getTime();
        UserListResult result =
                this.webserviceTemplate(UserListResult.class, userService,
                        new WebserviceCallHandler<UserListResult, UserServiceLocal>() {
                            @Override
                            public void handle(UserListResult aResult, UserServiceLocal aService)
                                    throws DKVBusinessException {
                                List<UserDTO> userDTOs = aService.findUsersInGroups(aIdListRequest.getId(), displayGroupInfo);
                                aResult.setValue(userDTOs);
                            }
                        });
        if (LOG.isInfoEnabled()) {
            LOG.info("findUsersInGroups.elapsedTime "
                    + LoggingMessageUtil.createElapsedTimeMessage(startTime, (new Date()).getTime()));
        }
        return result;
    }


}
