package be.dkv.arma.ws;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.apache.log4j.Logger;

import be.dkv.arma.common.Constants;
import be.dkv.arma.model.ApplicationUrlDTO;
import be.dkv.arma.model.UrlGroupDTO;
import be.dkv.arma.result.UrlGroupListResult;
import be.dkv.arma.result.UrlGroupResult;
import be.dkv.arma.service.ApplicationUrlServiceLocal;
import be.dkv.arma.service.GroupServiceLocal;
import be.dkv.arma.service.UrlGroupServiceLocal;
import be.dkv.fw.dto.StringListResult;
import be.dkv.fw.dto.VoidResult;
import be.dkv.fw.exception.DKVBusinessException;
import be.dkv.fw.ws.AbstractWebService;
import be.dkv.fw.ws.WebserviceCallHandler;

// TODO: Auto-generated Javadoc
/**
 * The Class DashBoardWebService.
 */
@WebService(serviceName = Constants.SERVICE_NAME, targetNamespace = Constants.TARGET_NAMESPACE)
@Stateless
public class DashBoardWebService extends AbstractWebService {

    /** The Constant SERVICE_NAME. */
    public static final String SERVICE_NAME = "ARMA";

    /**
     * The Constant USER_PARAM the parametername for a variable that contains a
     * userName.
     */
    public static final String USER_PARAM = "userName";

    /**
     * The Constant APPLICATION_URL_DTO the parametername for a variable that
     * contains an applicationUrl.
     */
    public static final String APPLICATION_URL_DTO = "applicationUrl";

    /**
     * The Constant APPLICATION_URL_ID the parametername for a variable that
     * contains an applicationUrlId.
     */
    public static final String APPLICATION_URL_ID = "applicationUrlId";

    /**
     * The Constant URL_GROUP_ID the parametername for a variable that contains
     * an urlGroupId.
     */
    public static final String URL_GROUP_ID = "urlGroupId";

    /**
     * The Constant URL_GROUP_DTO the parametername for a variable that contains
     * an urlGroup.
     */
    public static final String URL_GROUP_DTO = "urlGroup";

    /** The url group service local. */
    @EJB
    private UrlGroupServiceLocal urlGroupServiceLocal;

    /** The application url service local. */
    @EJB
    private ApplicationUrlServiceLocal applicationUrlServiceLocal;

    /** The group service. */
    @EJB
    private GroupServiceLocal groupService;

    /**
     * Instantiates a new dash board web service.
     */
    public DashBoardWebService() {
        super(Logger.getLogger(DashBoardWebService.class));
    }

    /**
     * Gets the url group for current user.
     *
     * @return the url group for current user
     */
    @WebMethod
    public UrlGroupListResult getUrlGroupForCurrentUser() {
        return this.webserviceTemplate(UrlGroupListResult.class, urlGroupServiceLocal,
                new WebserviceCallHandler<UrlGroupListResult, UrlGroupServiceLocal>() {
                    @Override
                    public void handle(UrlGroupListResult aResult, UrlGroupServiceLocal aService)
                            throws DKVBusinessException {
                        aResult.setValue(aService.getUrlGroupsForCurrentUser());
                    }
                });
    }

    /**
     * Gets the url groups for modification.
     *
     * @return the url groups for modification
     */
    @WebMethod
    public UrlGroupListResult getUrlGroupsForModification() {
        return this.webserviceTemplate(UrlGroupListResult.class, urlGroupServiceLocal,
                new WebserviceCallHandler<UrlGroupListResult, UrlGroupServiceLocal>() {
                    @Override
                    public void handle(UrlGroupListResult aResult, UrlGroupServiceLocal aService)
                            throws DKVBusinessException {
                        aResult.setValue(aService.getUrlGroupsForModification());
                    }
                });
    }

    /**
     * Update url group.
     *
     * @param aUrlGroupDTO
     *            the a url group dto
     * @return the void result
     */
    @WebMethod
    public VoidResult updateUrlGroup(@WebParam(name = URL_GROUP_DTO) final UrlGroupDTO aUrlGroupDTO) {
        return this.webserviceTemplate(VoidResult.class, urlGroupServiceLocal,
                new WebserviceCallHandler<VoidResult, UrlGroupServiceLocal>() {
                    @Override
                    public void handle(VoidResult aResult, UrlGroupServiceLocal aService) throws DKVBusinessException {
                        aService.updateUrlGroup(aUrlGroupDTO);
                    }
                });
    }

    /**
     * Update application url.
     *
     * @param aApplicationUrlDTO
     *            the a application url dto
     * @return the void result
     */
    @WebMethod
    public VoidResult updateApplicationUrl(
            @WebParam(name = APPLICATION_URL_DTO) final ApplicationUrlDTO aApplicationUrlDTO) {
        return this.webserviceTemplate(VoidResult.class, applicationUrlServiceLocal,
                new WebserviceCallHandler<VoidResult, ApplicationUrlServiceLocal>() {
                    @Override
                    public void handle(VoidResult aResult, ApplicationUrlServiceLocal aService)
                            throws DKVBusinessException {
                        aService.updateApplicationURL(aApplicationUrlDTO);
                    }
                });
    }

    /**
     * Gets the all access rights.
     *
     * @return the all access rights
     */
    @WebMethod
    public StringListResult getAllAccessRights() {
        return this.webserviceTemplate(StringListResult.class, groupService,
                new WebserviceCallHandler<StringListResult, GroupServiceLocal>() {
                    @Override
                    public void handle(StringListResult aResult, GroupServiceLocal aService)
                            throws DKVBusinessException {
                        aResult.setValue(aService.findAllGroupNames());
                    }
                });
    }

    /**
     * Adds the url group.
     *
     * @param aUrlGroupDTO
     *            the a url group dto
     * @return the url group result
     */
    @WebMethod
    public UrlGroupResult addUrlGroup(@WebParam(name = URL_GROUP_DTO) final UrlGroupDTO aUrlGroupDTO) {
        return this.webserviceTemplate(UrlGroupResult.class, urlGroupServiceLocal,
                new WebserviceCallHandler<UrlGroupResult, UrlGroupServiceLocal>() {
                    @Override
                    public void handle(UrlGroupResult aResult, UrlGroupServiceLocal aService)
                            throws DKVBusinessException {
                        aResult.setValue(aService.addUrlGroup(aUrlGroupDTO));
                    }
                });
    }

    /**
     * Adds the application url.
     *
     * @param aApplicationUrlDTO
     *            the a application url dto
     * @param aId
     *            the a id
     * @return the void result
     */
    @WebMethod
    public VoidResult addApplicationURL(
            @WebParam(name = APPLICATION_URL_DTO) final ApplicationUrlDTO aApplicationUrlDTO,
            @WebParam(name = URL_GROUP_ID) final long aId) {
        return this.webserviceTemplate(VoidResult.class, applicationUrlServiceLocal,
                new WebserviceCallHandler<VoidResult, ApplicationUrlServiceLocal>() {
                    @Override
                    public void handle(VoidResult aResult, ApplicationUrlServiceLocal aService)
                            throws DKVBusinessException {
                        aService.addApplicationURL(aApplicationUrlDTO, aId);
                    }
                });
    }

    /**
     * Removes the application url.
     *
     * @param aId
     *            the a id
     * @return the void result
     */
    @WebMethod
    public VoidResult removeApplicationUrl(@WebParam(name = APPLICATION_URL_ID) final Long aId) {
        return this.webserviceTemplate(VoidResult.class, applicationUrlServiceLocal,
                new WebserviceCallHandler<VoidResult, ApplicationUrlServiceLocal>() {
                    @Override
                    public void handle(VoidResult aResult, ApplicationUrlServiceLocal aService)
                            throws DKVBusinessException {
                        aService.removeApplicationUrl(aId);
                    }
                });
    }

    /**
     * Removes the url group.
     *
     * @param aId
     *            the a id
     * @return the void result
     */
    @WebMethod
    public VoidResult removeUrlGroup(@WebParam(name = URL_GROUP_ID) final Long aId) {
        return this.webserviceTemplate(VoidResult.class, urlGroupServiceLocal,
                new WebserviceCallHandler<VoidResult, UrlGroupServiceLocal>() {
                    @Override
                    public void handle(VoidResult aResult, UrlGroupServiceLocal aService) throws DKVBusinessException {
                        aService.removeUrlGroup(aId);
                    }
                });
    }

    /**
     * Gets the all url groups.
     *
     * @return the all url groups
     */
    @WebMethod
    public UrlGroupListResult getAllUrlGroups() {
        return this.webserviceTemplate(UrlGroupListResult.class, urlGroupServiceLocal,
                new WebserviceCallHandler<UrlGroupListResult, UrlGroupServiceLocal>() {
                    @Override
                    public void handle(UrlGroupListResult aResult, UrlGroupServiceLocal aService)
                            throws DKVBusinessException {
                        aResult.setValue(aService.findAll());
                    }
                });
    }

}
