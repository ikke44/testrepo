package be.dkv.arma.ws;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.apache.log4j.Logger;

import be.dkv.arma.common.ARMAErrorMessages;
import be.dkv.arma.entity.Group;
import be.dkv.arma.entity.User;
import be.dkv.arma.exception.GroupNotFoundException;
import be.dkv.arma.exception.UserNotFoundException;
import be.dkv.arma.mapper.WebServiceMapper;
import be.dkv.arma.migration.model.UserGroupMigrationDTO;
import be.dkv.arma.migration.result.UserGroupMigrationListResult;
import be.dkv.arma.model.GroupDTO;
import be.dkv.arma.model.IdListRequest;
import be.dkv.arma.model.IdRequest;
import be.dkv.arma.service.GroupServiceLocal;
import be.dkv.arma.service.LoggingMessageUtil;
import be.dkv.arma.service.UserServiceLocal;
import be.dkv.fw.dto.VoidResult;
import be.dkv.fw.exception.DKVBusinessException;
import be.dkv.fw.ws.AbstractWebService;
import be.dkv.fw.ws.WebserviceCallHandler;

/**
 * Service for managing Groups and User - Group relations.
 *
 * @author dobbelae
 *
 */
@WebService(serviceName = GroupWebService.SERVICE_NAME, targetNamespace = GroupWebService.TARGET_NAMESPACE)
// @HandlerChain(file = "/META-INF/server-handlers.xml")
// @SchemaValidation
@Stateless
public class GroupWebService extends AbstractWebService {

    /** The name of the web service. */
    public static final String SERVICE_NAME = "ARMA";
    /** the target namespace. */
    public static final String TARGET_NAMESPACE = "http://service.arma.dkv.be";
    /** The name of a void request parameter. */
    public static final String VOID_REQUEST = "voidRequest";
    /** the name of the groupId parameter. */
    public static final String GROUP_ID = "GroupName";
    /** the name of the GroupDTO parameter. */
    public static final String GROUP = "Group";
    /** the name of the user loginName parameter. */
    public static final String LOGIN_NAME = "UserName";
    /** the name of the user loginName parameter. */
    public static final String USER_NUMBER = "UserNumber";
    /** the name of the searchCriteria parameter. */
    public static final String SEARCH_CRITERIA = "SearchCriteria";

    private static final Logger LOG = Logger.getLogger(GroupWebService.class);

    @EJB
    private UserServiceLocal userService;
    @EJB
    private GroupServiceLocal groupService;

    public GroupWebService() {
        super(LOG);
    }

    /**
     * Adds the specified user to the specified groups. The action taken in this
     * method only takes effect on the SQL database and not Active Directory.
     *
     * @param aLoginId
     *            the loginName of the specified {@link User}
     * @param aGroupIdList
     *            a list of {@link Group}-names
     * @return the void result
     */
    @WebMethod
    public VoidResult addUserToGroups(@WebParam(name = GroupWebService.LOGIN_NAME) final IdRequest aLoginId,
            @WebParam(name = GroupWebService.GROUP_ID) final List<IdRequest> aGroupIdList) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("addUserToGroups");
        }
        long startTime = (new Date()).getTime();
        VoidResult result =
                this.webserviceTemplate(VoidResult.class, userService,
                        new WebserviceCallHandler<VoidResult, UserServiceLocal>() {
                            @Override
                            public void handle(VoidResult aResult, UserServiceLocal aService)
                                    throws DKVBusinessException {
                                if (aLoginId == null || aLoginId.getId() == null) {
                                    throw new UserNotFoundException(ARMAErrorMessages.NULL_ARG_PROVIDED);
                                }
                                if (aGroupIdList == null) {
                                    throw new GroupNotFoundException(ARMAErrorMessages.NULL_ARG_PROVIDED);
                                } else {
                                    try {
                                        List<String> groupNameList = WebServiceMapper.mapToString(aGroupIdList);
                                        userService.addUserToGroups(aLoginId.getId(), groupNameList);
                                    } catch (UserNotFoundException e) {
                                        aResult.setResultCode(e.getErrorResultCode());
                                    }
                                }
                            }
                        });
        if (LOG.isInfoEnabled()) {
            LOG.info("addUserToGroups.elapsedTime "
                    + LoggingMessageUtil.createElapsedTimeMessage(startTime, (new Date()).getTime()));
        }
        return result;
    }

    /**
     * Removes the specified user from the specified groups. The action taken in
     * this method only takes effect on the SQL database and not Active
     * Directory.
     *
     * @param aLoginId
     *            the loginName of the specified {@link User}
     * @param aGroupIdList
     *            a list of {@link Group}-names
     * @return the void result
     */
    @WebMethod
    public VoidResult removeUserFromGroups(@WebParam(name = GroupWebService.LOGIN_NAME) final IdRequest aLoginId,
            @WebParam(name = GroupWebService.GROUP_ID) final List<IdRequest> aGroupIdList) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("removeUserFromGroups");
        }
        long startTime = (new Date()).getTime();
        VoidResult result =
                this.webserviceTemplate(VoidResult.class, userService,
                        new WebserviceCallHandler<VoidResult, UserServiceLocal>() {
                            @Override
                            public void handle(VoidResult aResult, UserServiceLocal aService)
                                    throws DKVBusinessException {
                                if (aLoginId == null || aLoginId.getId() == null) {
                                    throw new UserNotFoundException(ARMAErrorMessages.NULL_ARG_PROVIDED);
                                }
                                if (aGroupIdList == null) {
                                    throw new GroupNotFoundException(ARMAErrorMessages.NULL_ARG_PROVIDED);
                                } else {
                                    try {
                                        List<String> groupNameList = WebServiceMapper.mapToString(aGroupIdList);
                                        userService.removeUserFromGroups(aLoginId.getId(), groupNameList);
                                    } catch (UserNotFoundException e) {
                                        aResult.setResultCode(e.getErrorResultCode());
                                    }
                                }
                            }
                        });
        if (LOG.isInfoEnabled()) {
            LOG.info("removeUserFromGroups.elapsedTime "
                    + LoggingMessageUtil.createElapsedTimeMessage(startTime, (new Date()).getTime()));
        }
        return result;
    }

    /**
     * Creates a new group in the SQL database. The action taken in this method
     * only takes effect on the SQL database and not Active Directory.
     *
     * @param aGroup
     *            a {@link GroupDTO}
     * @return the void result
     */
    @WebMethod
    public VoidResult addGroup(@WebParam(name = GroupWebService.GROUP) final GroupDTO aGroup) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("addGroup");
        }
        long startTime = (new Date()).getTime();
        VoidResult result =
                this.webserviceTemplate(VoidResult.class, groupService,
                        new WebserviceCallHandler<VoidResult, GroupServiceLocal>() {
                            @Override
                            public void handle(VoidResult aResult, GroupServiceLocal aService)
                                    throws DKVBusinessException {
                                if (aGroup == null) {
                                    throw new GroupNotFoundException(ARMAErrorMessages.NULL_ARG_PROVIDED);
                                } else {
                                    groupService.addGroupInSQL(aGroup);

                                }
                            }
                        });
        if (LOG.isInfoEnabled()) {
            LOG.info("addGroup.elapsedTime "
                    + LoggingMessageUtil.createElapsedTimeMessage(startTime, (new Date()).getTime()));
        }
        return result;
    }

    /**
     * Removes the groups from the SQL database. The action taken in this method
     * only takes effect on the SQL database and not Active Directory.
     *
     * @param aGroupIdList
     *            a list of {@link Group}-names
     * @return the void result
     */
    @WebMethod
    public VoidResult removeGroups(@WebParam(name = GroupWebService.GROUP_ID) final List<IdRequest> aGroupIdList) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("removeGroups");
        }
        long startTime = (new Date()).getTime();
        VoidResult result =
                this.webserviceTemplate(VoidResult.class, groupService,
                        new WebserviceCallHandler<VoidResult, GroupServiceLocal>() {
                            @Override
                            public void handle(VoidResult aResult, GroupServiceLocal aService)
                                    throws DKVBusinessException {
                                if (aGroupIdList == null) {
                                    throw new GroupNotFoundException(ARMAErrorMessages.NULL_ARG_PROVIDED);
                                } else {
                                    try {
                                        List<String> groupNameList = WebServiceMapper.mapToString(aGroupIdList);
                                        groupService.removeGroupsFromSQL(groupNameList);
                                    } catch (UserNotFoundException e) {
                                        aResult.setResultCode(e.getErrorResultCode());
                                    }
                                }
                            }
                        });
        if (LOG.isInfoEnabled()) {
            LOG.info("removeGroups.elapsedTime "
                    + LoggingMessageUtil.createElapsedTimeMessage(startTime, (new Date()).getTime()));
        }
        return result;
    }

    @WebMethod
    public UserGroupMigrationListResult findUserGroupMigrationDatabyLoginNames(
            @WebParam(name = ArmaCoreWebServiceBean.LOGIN_NAMES) final IdListRequest aIdListRequest,
            @WebParam(name = "Ignored" + ArmaCoreWebServiceBean.GROUP_IDS) final IdListRequest ignoredGroupNames,
            @WebParam(name = "findToBeAddedToLDAP") final boolean findToBeAddedToLDAP,
            @WebParam(name = "findToBeRemovedFromLDAP") final boolean findToBeRemovedFromLDAP) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("findUserGroupMigrationDatabyLoginNames");
        }
        long startTime = (new Date()).getTime();
        UserGroupMigrationListResult result =
                this.webserviceTemplate(UserGroupMigrationListResult.class, userService,
                        new WebserviceCallHandler<UserGroupMigrationListResult, UserServiceLocal>() {
                            @Override
                            public void handle(UserGroupMigrationListResult aResult, UserServiceLocal aService)
                                    throws DKVBusinessException {
                                List<UserGroupMigrationDTO> migrationList =
                                        aService.findUserGroupMigrationData(aIdListRequest.getId(),
                                                ignoredGroupNames.getId(), findToBeAddedToLDAP, findToBeRemovedFromLDAP);
                                aResult.setValue(migrationList);
                            }
                        });
        if (LOG.isInfoEnabled()) {
            LOG.info("findUserGroupMigrationDatabyLoginNames.elapsedTime "
                    + LoggingMessageUtil.createElapsedTimeMessage(startTime, (new Date()).getTime()));
        }
        return result;
    }

}
