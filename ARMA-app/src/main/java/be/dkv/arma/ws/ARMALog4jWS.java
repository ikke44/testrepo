package be.dkv.arma.ws;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebService;

import org.apache.log4j.Logger;

import be.dkv.arma.common.Constants;
import be.dkv.arma.common.settings.Log4jManager;
import be.dkv.fw.manager.AbstractLog4jManager;
import be.dkv.fw.ws.AbstractLog4jWS;
/**
 * Offers a SOAP web service to handle the tickets 
 *
 */
@Stateless
@WebService(serviceName = Constants.SERVICE_NAME, targetNamespace = Constants.TARGET_NAMESPACE)
public class ARMALog4jWS extends AbstractLog4jWS {

	private static Logger logger = Logger.getLogger(ARMALog4jWS.class);
	
	public ARMALog4jWS() {
		super(logger);
	}

	@EJB
	private Log4jManager log4jManager;	
	
	@Override
	protected AbstractLog4jManager getLog4jManager() {
		return log4jManager;
	}


}
