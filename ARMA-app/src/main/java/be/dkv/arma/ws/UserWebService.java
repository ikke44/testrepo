package be.dkv.arma.ws;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebService;

import org.apache.log4j.Logger;

import be.dkv.arma.common.Constants;
import be.dkv.arma.result.UserResult;
import be.dkv.arma.service.UserServiceLocal;
import be.dkv.fw.dto.BooleanResult;
import be.dkv.fw.dto.VoidResult;
import be.dkv.fw.exception.DKVBusinessException;
import be.dkv.fw.ws.AbstractWebService;
import be.dkv.fw.ws.WebserviceCallHandler;

@WebService(serviceName = Constants.SERVICE_NAME, targetNamespace = Constants.TARGET_NAMESPACE)
@Stateless
public class UserWebService extends AbstractWebService {

    private static final Logger LOG = Logger.getLogger(UserWebService.class);

    @EJB
    private UserServiceLocal userService;

    public UserWebService() {
        super(LOG);
    }

    @WebMethod
    public UserResult findCurrentUser() {
        return this.webserviceTemplate(UserResult.class, userService, new WebserviceCallHandler<UserResult, UserServiceLocal>() {
            @Override
            public void handle(UserResult aResult, UserServiceLocal aService) throws DKVBusinessException {
                aResult.setValue(userService.findCurrentUser());
            }
        });
    }

    @WebMethod
    public BooleanResult getArchiveStatus() {
        return this.webserviceTemplate(BooleanResult.class, userService, new WebserviceCallHandler<BooleanResult, UserServiceLocal>() {
            @Override
            public void handle(BooleanResult aResult, UserServiceLocal aService) throws DKVBusinessException {
                aResult.setValue(userService.isArchiveEnabled());
            }
        });
    }

    @WebMethod
    public VoidResult acceptArchiving() {
        return this.webserviceTemplate(VoidResult.class, userService, new WebserviceCallHandler<VoidResult, UserServiceLocal>() {
            @Override
            public void handle(VoidResult aResult, UserServiceLocal aService) throws DKVBusinessException {
                userService.acceptArchiving();
            }
        });
    }

}
