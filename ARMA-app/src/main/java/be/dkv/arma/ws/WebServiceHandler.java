package be.dkv.arma.ws;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.apache.log4j.Logger;

/**
 * Web service handler for the core web service.
 * 
 * @author geenens
 */
public class WebServiceHandler implements SOAPHandler<SOAPMessageContext> {

    private static final Logger LOG = Logger.getLogger(WebServiceHandler.class.getName());

    @Override
    public void close(MessageContext aMessageContext) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("close " + aMessageContext);
        }
    }

    @Override
    public boolean handleFault(SOAPMessageContext aSoapMessageContext) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("handleFault");
        }
        if (LOG.isInfoEnabled()) {
            logSoapMessagecontect(aSoapMessageContext);
        }
        return true;
    }

    private void logSoapMessagecontect(SOAPMessageContext aSoapMessageContext) {
        Boolean outBoundProperty = (Boolean) aSoapMessageContext.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
        SOAPMessage soapMessage = aSoapMessageContext.getMessage();
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            soapMessage.writeTo(outputStream);
            String soapRawXmlMessage = new String(outputStream.toByteArray());
            if (outBoundProperty) {
                LOG.info("Outbound SOAPMessage : " + soapRawXmlMessage);
            } else {
                LOG.info("Inbound SOAPMessage : " + soapRawXmlMessage);
            }
        } catch (SOAPException e) {
            LOG.fatal("Unable to log SOAP message due to SOAPException ", e);
        } catch (IOException e) {
            LOG.fatal("Unable to log SOAP message due to IOException ", e);
        }
    }

    @Override
    public boolean handleMessage(SOAPMessageContext aSoapMessageContext) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("handleMessage");
        }
        if (LOG.isInfoEnabled()) {
            logSoapMessagecontect(aSoapMessageContext);
        }
        return true;
    }

    @Override
    public Set<QName> getHeaders() {
        return null;
    }
}
