package be.dkv.arma.model;

/**
 * Defines a single user using two unique id fields
 * 
 */
public class UserKey {
    private String loginName;
    private String userNumber;

    public UserKey() {
        this.loginName = null;
        this.userNumber = null;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        if (loginName != null) {
            loginName.toUpperCase();
        }
        this.loginName = loginName;
    }

    public String getUserNumber() {
        return userNumber;
    }

    public void setUserNumber(String userNumber) {
        this.userNumber = userNumber;
    }

    @Override
    public String toString() {
        return loginName;
    }

}
