package be.dkv.arma.model;

import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import be.dkv.fw.model.AbstractDTO;

public class ApplicationUrlDTO extends AbstractDTO<Long> {
    private static final long serialVersionUID = 4084254205142708075L;

    private String displayName;
    private String url;
    private String applicationName;
    private List<GroupDTO> accessRight;

    public List<GroupDTO> getAccessRight() {
        return accessRight;
    }

    public void setAccessRight(List<GroupDTO> accessRight) {
        this.accessRight = accessRight;
    }

    private String applicationDescription;
    private Integer displayOrder;
    private String color;

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String aDisplayName) {
        displayName = aDisplayName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String aUrl) {
        url = aUrl;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String aApplicationName) {
        applicationName = aApplicationName;
    }

    public String getApplicationDescription() {
        return applicationDescription;
    }

    public void setApplicationDescription(String aApplicationDescription) {
        applicationDescription = aApplicationDescription;
    }

    public Integer getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(Integer aDisplayOrder) {
        displayOrder = aDisplayOrder;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String aColor) {
        color = aColor;
    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(Long aId) {
        this.id = aId;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hcb =
                new HashCodeBuilder().append(applicationName).append(color).append(applicationDescription)
                        .append(displayOrder).append(url).append(id);
        for (GroupDTO groupDTO : accessRight) {
            hcb.append(groupDTO.hashCode());
        }
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals = false;
        if (this == obj) {
            equals = true;
        } else if (obj == null || getClass() != obj.getClass()) {
            equals = false;
        } else {
            ApplicationUrlDTO other = (ApplicationUrlDTO) obj;
            equals =
                    new EqualsBuilder().append(applicationName, other.applicationName)
                            .append(accessRight, other.accessRight).append(color, other.color)
                            .append(applicationDescription, other.applicationDescription)
                            .append(displayOrder, other.displayOrder).append(url, other.url).append(id, other.id)
                            .isEquals();
        }
        return equals;

    }
}
