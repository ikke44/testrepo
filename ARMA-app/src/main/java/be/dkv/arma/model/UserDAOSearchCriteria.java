package be.dkv.arma.model;

import java.math.BigDecimal;

public class UserDAOSearchCriteria {

    private String loginName;
    private String userNumber;

    private String firstName;
    private String lastName;

    private BigDecimal fromMaximumApprovalAmount;
    private BigDecimal toMaximumApprovalAmount;
    private BigDecimal fromMaximumPaymentAmount;
    private BigDecimal toMaximumPaymentAmount;

    private Integer kulanzId;

    private String verificatorLoginName;
    private String verificatorUserNumber;

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getUserNumber() {
        return userNumber;
    }

    public void setUserNumber(String userNumber) {
        this.userNumber = userNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public BigDecimal getFromMaximumApprovalAmount() {
        return fromMaximumApprovalAmount;
    }

    public void setFromMaximumApprovalAmount(BigDecimal fromMaximumApprovalAmount) {
        this.fromMaximumApprovalAmount = fromMaximumApprovalAmount;
    }

    public BigDecimal getToMaximumApprovalAmount() {
        return toMaximumApprovalAmount;
    }

    public void setToMaximumApprovalAmount(BigDecimal toMaximumApprovalAmount) {
        this.toMaximumApprovalAmount = toMaximumApprovalAmount;
    }

    public BigDecimal getFromMaximumPaymentAmount() {
        return fromMaximumPaymentAmount;
    }

    public void setFromMaximumPaymentAmount(BigDecimal fromMaximumPaymentAmount) {
        this.fromMaximumPaymentAmount = fromMaximumPaymentAmount;
    }

    public BigDecimal getToMaximumPaymentAmount() {
        return toMaximumPaymentAmount;
    }

    public void setToMaximumPaymentAmount(BigDecimal toMaximumPaymentAmount) {
        this.toMaximumPaymentAmount = toMaximumPaymentAmount;
    }

    public Integer getKulanzId() {
        return kulanzId;
    }

    public void setKulanzId(Integer kulanzId) {
        this.kulanzId = kulanzId;
    }

    public String getVerificatorLoginName() {
        return verificatorLoginName;
    }

    public void setVerificatorLoginName(String verificatorLoginName) {
        this.verificatorLoginName = verificatorLoginName;
    }

    public String getVerificatorUserNumber() {
        return verificatorUserNumber;
    }

    public void setVerificatorUserNumber(String verificatorUserNumber) {
        this.verificatorUserNumber = verificatorUserNumber;
    }

}
