package be.dkv.arma.model;

import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import be.dkv.fw.model.AbstractDTO;

public class UrlGroupDTO extends AbstractDTO<Long> {
    private static final long serialVersionUID = 1389150583275921626L;

    private String displayName;
    private String displayOrder;
    private String color;
    private List<ApplicationUrlDTO> ApplicationUrlDTO;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long aId) {
        id = aId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String aDisplayName) {
        displayName = aDisplayName;
    }

    public String getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(String aDisplayOrder) {
        displayOrder = aDisplayOrder;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String aColor) {
        color = aColor;
    }

    public List<ApplicationUrlDTO> getApplicationUrlDTO() {
        return ApplicationUrlDTO;
    }

    public void setApplicationUrlDTO(List<ApplicationUrlDTO> aApplicationUrlDTO) {
        ApplicationUrlDTO = aApplicationUrlDTO;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(color).append(displayName).append(displayOrder).append(id).toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals = false;
        if (this == obj) {
            equals = true;
        } else if (obj == null || getClass() != obj.getClass()) {
            equals = false;
        } else {
            UrlGroupDTO other = (UrlGroupDTO) obj;
            equals =
                    new EqualsBuilder().append(color, other.color).append(displayName, other.displayName)
                            .append(displayOrder, other.displayOrder).append(id, other.id).isEquals();
        }
        return equals;
    }

}
