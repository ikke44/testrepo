package be.dkv.arma.model;

/**
 * The ImportSource specifies the sources where ARMA user- or group-data can be
 * imported from.
 * 
 * @author dobbelae
 * 
 */
public enum ImportSource {
    /**
     * The SQL source.
     */
    SQL,
    /**
     * The LDAP source.
     */
    LDAP,
    /**
     * The shared source SQL_AND_LDAP meaning data was imported from Both SQL
     * and LDAP contexts.
     */
    SQL_AND_LDAP,
}
