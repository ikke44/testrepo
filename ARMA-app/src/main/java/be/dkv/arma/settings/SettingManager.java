package be.dkv.arma.settings;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import be.dkv.arma.dao.GenericDaoBean;
import be.dkv.fw.dao.AbstractGenericDaoBean;
import be.dkv.fw.manager.AbstractSettingManager;

@Stateless
public class SettingManager extends AbstractSettingManager {

	@EJB
	private GenericDaoBean genericDao;

	@Override
	protected AbstractGenericDaoBean getGenericDao() {
		return genericDao;
	}

}
