package be.dkv.arma.common;

/**
 * @author vansever
 *
 */
public class Constants {

    /**
     * No constructor for utility class.
     */
    public Constants() {
    }

    /** The Constant SERVICE_NAME. */
    public final static String SERVICE_NAME = "ARMA";

    /** The Constant PUBLIC_GROUP_NAME. */
    public final static String PUBLIC_GROUP_NAME = "public";
    /** The Constant PUBLIC_GROUP_DESCRIPTION. */
    public final static String PUBLIC_GROUP_DESCRIPTION = "The public DKV Group";

    /** The Constant TARGET_NAMESPACE. */
    public final static String TARGET_NAMESPACE = "http://service.arma.dkv.be";
}
