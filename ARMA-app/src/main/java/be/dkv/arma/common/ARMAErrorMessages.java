package be.dkv.arma.common;

/**
 * final class enclosing specific error messages from ARMA.
 * 
 * @author dobbelae
 * 
 */
public final class ARMAErrorMessages {
    public static final String NULL_ARG_PROVIDED = "argument 'NULL' provided";
}
