package be.dkv.arma.common.settings;

import be.dkv.fw.model.setting.SettingConstant;

/**
 * @author vansever
 *
 */
public class ArmaSettingsConstant {

    public static final SettingConstant<String> ENVIRONMENT = new SettingConstant<String>("arma.environment",
            String.class, "");
    /** LDAP query filter for all users. */
    public static final SettingConstant<String> FILTER_ALL_USERS = new SettingConstant<String>(
            "searchFilter.user.findAll", String.class, "(&(objectCategory=person)(objectClass=user)(department=*))");
    /** LDAP query filter for user by sAMAccountName. */
    public static final SettingConstant<String> FILTER_USER_BY_SAM_ACCOUNT_NAME = new SettingConstant<String>(
            "searchFilter.user.login", String.class, "(&(objectClass=user)(sAMAccountName={0}))");
    /** LDAP query filter for group by name. */
    public static final SettingConstant<String> FILTER_GROUP_BY_NAME = new SettingConstant<String>(
            "searchFilter.group.byName", String.class, "(&(objectClass=group)(cn={0}))");
    /** LDAP organizational unit for dkv users. */
    public static final SettingConstant<String> OU_IS_DKV_USERS = new SettingConstant<String>("ou.users", String.class,
            "_DKV_USERS");
    /** LDAP organizational unit for groups. */
    public static final SettingConstant<String> OU_IS_GROUPS = new SettingConstant<String>("ou.groups", String.class,
            "OU=GROUPS");

    public static final SettingConstant<String> BASEDN = new SettingConstant<String>("basedn", String.class,
            "DC=DKV,DC=ERGO-SERVICE,DC=BELGIUM,DC=IO,DC=ERGO");

    public static final SettingConstant<String> SEARCHFILTER_USER_DISTINGUISHEDNAME = new SettingConstant<String>(
            "searchFilter.user.distinguishedName", String.class, "(&(objectClass=user)(distinguishedName={0}))");
    public static final SettingConstant<String> SEARCHFILTER_GROUP_CN = new SettingConstant<String>(
            "searchFilter.group.cn", String.class, "(&(objectClass=group)(cn={0}))");
    public static final SettingConstant<String> JAVA_NAMING_PROVIDER_URL = new SettingConstant<String>(
            "java.naming.provider.url", String.class, "");
    public static final SettingConstant<String> JAVA_NAMING_FACTORY_INITIAL = new SettingConstant<String>(
            "java.naming.factory.initial", String.class, "");
    public static final SettingConstant<String> JAVA_NAMING_FACTORY_OBJECT = new SettingConstant<String>(
            "java.naming.factory.object", String.class, "");
    public static final SettingConstant<String> JAVA_NAMING_FACTORY_STATE = new SettingConstant<String>(
            "java.naming.factory.state", String.class, "");
    public static final SettingConstant<String> JAVA_NAMING_SECURITY_PRINCIPAL = new SettingConstant<String>(
            "java.naming.security.principal", String.class, "");
    public static final SettingConstant<String> JAVA_NAMING_SECURITY_CREDENTIALS = new SettingConstant<String>(
            "java.naming.security.credentials", String.class, "");

}
