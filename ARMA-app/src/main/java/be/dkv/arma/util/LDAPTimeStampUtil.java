package be.dkv.arma.util;

import java.sql.Date;

/**
 * Util to convert LDAP time stamps.
 * 
 * @author mahieus
 * 
 */
public final class LDAPTimeStampUtil {

    private static final long CORRECTION = 11644473600000L;
    private static final long TIME_STAMP_DIVIDE_FACTOR = 1000;

    /**
     * convert LDAP time stamp to java.util.Date.
     * 
     * @param timeStamp
     *            LDAP time stamp
     * @return converted time stamp
     */
    public static Date convertToDate(long timeStamp) {
        return new Date(timeStamp / TIME_STAMP_DIVIDE_FACTOR - CORRECTION);
    }

    /**
     * convert LDAP time stamp to java.util.Date.
     * 
     * @param timeStamp
     *            LDAP time stamp
     * @return converted time stamp
     */
    public static Date convertToDate(String timeStamp) {
        return convertToDate(Long.valueOf(timeStamp));
    }

}
