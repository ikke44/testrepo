package be.dkv.arma.task;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;

import be.dkv.arma.service.UserServiceLocal;
import be.dkv.fw.exception.DKVBusinessException;

/**
 * 
 * 
 * @author vanhout
 */
@Stateless(mappedName = ArmaADSynchTask.JNDI_NAME)
public class ArmaADSynchTaskBean implements ArmaADSynchTask {

    private static final Logger LOG = Logger.getLogger(ArmaADSynchTaskBean.class);

    @EJB
    private UserServiceLocal userService;

    @Override
    public void run() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Starting ARMA user synchronizing");
        }
        try {
            userService.synchronizeUsers();
        } catch (DKVBusinessException e) {
            LOG.warn("DKVBusinessException excuting ARMA user synchronizing.", e);
        } catch (RuntimeException e) {
            LOG.fatal("RuntimeException excutingARMA user synchronizing.", e);
        }
    }
}
