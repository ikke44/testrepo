package be.dkv.arma.dao;

import javax.ejb.Local;
import javax.ejb.Stateless;

import be.dkv.arma.entity.ApplicationUrl;

/**
 * The Class ApplicationUrlDAO.
 */
@Local
@Stateless
public class ApplicationUrlDAO extends AbstractARMADAO<ApplicationUrl, Long> {

    /**
     * Instantiates the ApplicationUrlDAO.
     */
    public ApplicationUrlDAO() {
        super(ApplicationUrl.class);
    }

}
