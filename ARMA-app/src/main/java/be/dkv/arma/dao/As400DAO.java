package be.dkv.arma.dao;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.Query;

import org.apache.log4j.Logger;

import be.dkv.arma.entity.User;

/**
 * This domain access object is used to sync User entities with imported AS400
 * data, using stored procedures from the database.
 *
 * @author dobbelae
 */
@Local
@Stateless
public class As400DAO extends AbstractARMADAO<User, Long> {

    public As400DAO() {
        super(User.class);
    }

    private static final Logger LOG = Logger.getLogger(UserDAO.class);

    /**
     * Synchronizes the Arma User with data on the AS400 using the specified
     * LoginName.
     *
     * @param aLoginName
     *            the a login name
     * @return the number of entities updated or deleted
     */
    public int synchronizeUserWithAS400(String aLoginName) {
        int result = -1;
        if (LOG.isInfoEnabled()) {
            LOG.info("Synchronizing user from AS400 to ARMA SQL database:" + aLoginName);
        }
        if (aLoginName == null || aLoginName.isEmpty()) {
            result = 0;
        } else {
            Query storedProcedureQuery = getEm().createNativeQuery(User.QUERY_SYNCHRONIZE_USER);
            storedProcedureQuery.setParameter(User.PARAM_USERLOGIN, aLoginName);
            result = storedProcedureQuery.executeUpdate();
        }
        return result;
    }

    /**
     * Synchronizes the all existing ARMA Users with data on the AS400.
     *
     * @return the number of entities updated or deleted
     */
    public int synchronizeAllUsersWithAS400() {
        int result = -1;
        if (LOG.isInfoEnabled()) {
            LOG.info("Synchronizing all users from AS400 to ARMA SQL database.");
        }
        // Synchronizing is done with a stored procedure.
        Query storedProcedureQuery = getEm().createNativeQuery(User.QUERY_SYNCHRONIZE_ALL_USERS);
        result = storedProcedureQuery.executeUpdate();
        return result;
    }

}
