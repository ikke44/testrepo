package be.dkv.arma.dao;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

import be.dkv.fw.dao.AbstractGenericDaoBean;


@Stateless
public class GenericDaoBean extends AbstractGenericDaoBean {

    private static final String PU_UNIT_NAME = "ARMA-persistence";

    @PersistenceContext(unitName = PU_UNIT_NAME)
    private EntityManager em;

    @Resource(name = "jdbc/arma")
    private DataSource dataSource;

    @Override
    protected EntityManager getEm() {
        return em;
    }

    @Override
    protected DataSource getDataSource() {
        return dataSource;
    }
}
