package be.dkv.arma.dao;

import javax.ejb.Local;
import javax.ejb.Stateless;

import be.dkv.arma.entity.UrlGroup;

/**
 * The Class UrlGroupDAO.
 */
@Local
@Stateless
public class UrlGroupDAO extends AbstractARMADAO<UrlGroup, Long> {

    /**
     * Instantiates a new UrlGroupDAO.
     */
    public UrlGroupDAO() {
        super(UrlGroup.class);
    }

    /**
     * Executes a flush with the EntityManager.
     */
    public void flush() {
        this.getEm().flush();
    }

}
