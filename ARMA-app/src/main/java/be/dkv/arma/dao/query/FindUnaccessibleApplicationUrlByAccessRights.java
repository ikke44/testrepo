package be.dkv.arma.dao.query;

import java.util.Set;

import javax.persistence.TypedQuery;

import be.dkv.arma.entity.ApplicationUrl;
import be.dkv.arma.entity.Group;
import be.dkv.fw.dao.query.ListResultQuery;

/**
 * FindApplicationUrlByAccessRights query class.
 *
 * @author dries
 */
public class FindUnaccessibleApplicationUrlByAccessRights extends ListResultQuery<ApplicationUrl> {

    /** The Constant QP_ACCESS_RIGHTS. */
    public static final String QP_ACCESS_RIGHTS = "accessRights";

    /** The Constant QUERY_NAME. */
    public final static String QUERY_NAME = "ApplicationUrlDAO.FindUnaccessibleApplicationUrlByAccessRights";

    /** The Constant QUERY. */
    public final static String QUERY = "SELECT au FROM ApplicationUrl au" + " WHERE au.accessRight not in :"
            + QP_ACCESS_RIGHTS;

    private Set<String> accessRights;

    /**
     * Find unaccessible {@link ApplicationUrl} by its accessright. This will
     * search for the {@link ApplicationUrl}s who are linked to the
     * {@link Group} with its name equal to one of the specified accessright.
     * 
     * @param accessRights
     *            the {@link Group}-name of the {@link Group}, linked to the
     *            {@link ApplicationUrl}
     */
    public FindUnaccessibleApplicationUrlByAccessRights(Set<String> accessRights) {
        super(QUERY_NAME, ApplicationUrl.class);
        this.accessRights = accessRights;
    }

    public TypedQuery<ApplicationUrl> applyParameters(TypedQuery<ApplicationUrl> typedQuery) {
        return typedQuery.setParameter(QP_ACCESS_RIGHTS, accessRights);
    }

}
