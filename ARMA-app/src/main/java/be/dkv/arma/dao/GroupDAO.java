package be.dkv.arma.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;

import be.dkv.arma.common.ARMAErrorCodes;
import be.dkv.arma.entity.Group;
import be.dkv.arma.entity.metamodel.GroupEntityMetaModel;
import be.dkv.arma.exception.GroupNotFoundException;
import be.dkv.arma.exception.ImportSourceNotSpecifiedException;
import be.dkv.arma.model.GroupDTO;
import be.dkv.arma.model.ImportSource;
import be.dkv.fw.exception.DKVBusinessException;

/**
 * This domain access object is used to retrieve Group entities from the
 * database.
 *
 * @author dobbelae
 */
@Local
@Stateless
public class GroupDAO extends AbstractARMADAO<Group, Long> {

    private static final Logger LOG = Logger.getLogger(GroupDAO.class);

    public GroupDAO() {
        super(Group.class);
    }

    /**
     * Retrieves a {@link List} of all {@link Group}s, ordered by groupname
     * ascending. This method will return an Empty list when nothing is found.
     *
     * @return a {@link List} of all {@link Group}s.
     */
    public List<Group> findAllGroups() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("findAllGroups");
        }
        TypedQuery<Group> query = getEm().createNamedQuery(Group.FIND_ALL_GROUPS, Group.class);
        return query.getResultList();
    }

    /**
     * Removes the specified groupId's from the specified {@link ImportSource}.
     * If the specified groupId's have a shared {@link ImportSource}, only the
     * specified {@link ImportSource} will be removed.
     *
     * @param groupIds
     *            a {@link List} of the id's you want to remove.
     * @param importSource
     *            the {@link ImportSource}(s) you want the groups to be removed
     *            from.
     * @return the number of removed Groups
     * @throws ImportSourceNotSpecifiedException
     *             the import source not specified exception
     */
    public int removeGroups(List<String> groupIds, ImportSource importSource) throws ImportSourceNotSpecifiedException {
        int removedGroups = 0;
        if (groupIds != null && !groupIds.isEmpty()) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("removeGroups - removeAllInList: " + groupIds + " from ImportSource: '" + importSource + "'");
            }

            if (importSource == null) {
                throw new ImportSourceNotSpecifiedException();
            }

            // Remove the groups from that specific source
            Query queryRemoveAllInList = getEm().createNamedQuery(Group.REMOVE_ALL_IN_LIST_WITH_IMPORTSOURCE);
            queryRemoveAllInList.setParameter(Group.GROUP_NAME_LIST, groupIds);
            queryRemoveAllInList.setParameter(Group.IMPORTSOURCE, importSource);
            removedGroups = queryRemoveAllInList.executeUpdate();

            removedGroups += removeImportSourceFromGroups(groupIds, importSource);

        } else {
            if (LOG.isDebugEnabled()) {
                LOG.debug("removeGroups - no groups were removed");
            }
        }

        return removedGroups;
    }

    /**
     * Removes the import source from groups.
     *
     * @param groupIds
     *            the groupIds of the groups that need to be modified
     * @param importSource
     *            that needs to be removed or modified from the groups
     * @return the number of removed groups
     * @throws ImportSourceNotSpecifiedException
     *             if and invalid importsource was specified
     */
    private int removeImportSourceFromGroups(List<String> groupIds, ImportSource importSource)
            throws ImportSourceNotSpecifiedException {
        int removedGroups = 0;
        if (importSource != null) {
            switch (importSource) {
                case LDAP:
                    // Set SQL where SQL_AND_LDAP
                    removedGroups += changeImportSourceForGroups(groupIds, ImportSource.SQL_AND_LDAP, ImportSource.SQL);
                    break;
                case SQL:
                    // Set LDAP where SQL_AND_LDAP
                    removedGroups +=
                            changeImportSourceForGroups(groupIds, ImportSource.SQL_AND_LDAP, ImportSource.LDAP);
                    break;
                case SQL_AND_LDAP:
                    // SQL_AND_LDAP case does not require extra alterations.
                    // This step should not be interpreted as removing SQL and
                    // LDAP altogether.
                    // This would mean that all the data in the database would
                    // have to be dropped.
                    break;
                default:
                    throw new ImportSourceNotSpecifiedException();
            }
        }
        return removedGroups;
    }

    /**
     * Change import source for groups.
     *
     * @param groupIds
     *            the names of the {@link Group}s whereof the
     *            {@link ImportSource}s should be changed.
     * @param importSourceFrom
     *            the {@link ImportSource} where the {@link Group}s
     *            to-be-changed should belong to.
     * @param importSourceTo
     *            the {@link ImportSource} to change the {@link Group}s to.
     * @return the amount of rows adjusted
     * @throws ImportSourceNotSpecifiedException
     *             the import source not specified exception
     */
    private int changeImportSourceForGroups(List<String> groupIds, ImportSource importSourceFrom,
            ImportSource importSourceTo) throws ImportSourceNotSpecifiedException {
        if (importSourceFrom == null || importSourceTo == null) {
            throw new ImportSourceNotSpecifiedException();
        }
        Query queryChangeImportSourceInList = getEm().createNamedQuery(Group.CHANGE_IMPORTSOURCE_IN_LIST);
        queryChangeImportSourceInList.setParameter(Group.GROUP_NAME_LIST, groupIds);
        queryChangeImportSourceInList.setParameter(Group.IMPORTSOURCE, importSourceTo);
        queryChangeImportSourceInList.setParameter(Group.PREVIOUS_IMPORTSOURCE, importSourceFrom);
        return queryChangeImportSourceInList.executeUpdate();
    }

    /**
     * Exists by name.
     *
     * @param aName
     *            (GroupIdentifier) of a Group.
     * @return a boolean indicating the existence of the Group, regardless of
     *         the ImportSource.
     */
    public boolean existsByName(String aName) {
        CriteriaBuilder cb = this.getEm().getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Group> entity = cq.from(Group.class);
        cq.select(cb.count(entity));
        Path<String> groupName = entity.get(GroupEntityMetaModel.FIELD_NAME);
        cq.where(cb.equal(groupName, aName));
        TypedQuery<Long> tq = this.getEm().createQuery(cq);
        Long count = tq.getSingleResult();
        return count > 0;
    }

    /**
     * Find by name.
     *
     * @param aGroupId
     *            (GroupIdentifier) of a Group.
     * @return a Group.
     * @throws DKVBusinessException
     *             the DKV business exception
     */
    public Group findByName(String aGroupId) throws DKVBusinessException {
        CriteriaBuilder cb = this.getEm().getCriteriaBuilder();
        CriteriaQuery<Group> cq = cb.createQuery(Group.class);
        Root<Group> entity = cq.from(Group.class);
        cq.select(entity);
        Path<String> groupName = entity.get(GroupEntityMetaModel.FIELD_NAME);
        cq.where(cb.equal(groupName, aGroupId));
        TypedQuery<Group> tq = this.getEm().createQuery(cq);
        Group result = null;
        try {
            result = tq.getSingleResult();
        } catch (NoResultException e) {
            throw new GroupNotFoundException(aGroupId);
        } catch (NonUniqueResultException e) {
            throw new DKVBusinessException(ARMAErrorCodes.ARMA_RESULT_NOT_UNIQUE);
        }
        return result;
    }

    /**
     * Find by name list.
     *
     * @param aNameList
     *            (GroupIdentifier) of a Group.
     * @return a Group.
     * @throws DKVBusinessException
     *             the DKV business exception
     */
    public List<Group> findByNameList(Collection<String> aNameList) throws DKVBusinessException {
        CriteriaBuilder cb = this.getEm().getCriteriaBuilder();
        CriteriaQuery<Group> cq = cb.createQuery(Group.class);
        Root<Group> entity = cq.from(Group.class);
        cq.select(entity);
        Path<String> groupName = entity.get(GroupEntityMetaModel.FIELD_NAME);
        cq.where(groupName.in(aNameList));
        TypedQuery<Group> tq = this.getEm().createQuery(cq);
        List<Group> result = null;
        result = tq.getResultList();

        return result;
    }

    /**
     * Find by dto list.
     *
     * @param groupDTOList
     *            the group dto list
     * @return the list
     * @throws DKVBusinessException
     *             the DKV business exception
     */
    public List<Group> findByDTOList(List<GroupDTO> groupDTOList) throws DKVBusinessException {
        Set<String> aNameList = new HashSet<String>();
        if (groupDTOList != null) {
            for (GroupDTO groupDTO : groupDTOList) {
                aNameList.add(groupDTO.getId());
            }
            return findByNameList(aNameList);
        } else {
            return new ArrayList<Group>();
        }

    }

}
