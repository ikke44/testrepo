package be.dkv.arma.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;

import be.dkv.arma.entity.User;
import be.dkv.arma.model.UserDAOSearchCriteria;
import be.dkv.arma.model.UserKey;

/**
 * This domain access object is used to retrieve User entities from the
 * database.
 *
 * @author vanhout
 */
@Local
@Stateless
public class UserDAO extends AbstractARMADAO<User, Long> {

    private static final Logger LOG = Logger.getLogger(UserDAO.class);

    private static final String LOGIN_NAME = "aLoginName";
    private static final String USER_NUMBER = "aUserNumber";
    private static final String LOGIN_NAMES = "aLoginNames";
    private static final String GROUP_NAME = "aGroupName";

    /**
     * Instantiates a new user dao.
     */
    public UserDAO() {
        super(User.class);
    }

    /**
     * Retrieves a User based on a login name.
     *
     * @param aLoginName
     *            The login name
     * @return A user
     */
    public User findUserByLoginName(String aLoginName) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("findUserByLoginName " + aLoginName);
        }
        User result = null;
        TypedQuery<User> query = getEm().createNamedQuery(User.FIND_USER_BY_LOGIN, User.class);
        query.setParameter(LOGIN_NAME, aLoginName);
        List<User> users = query.getResultList();
        if (users != null && !users.isEmpty()) {
            result = users.get(0);
        }
        return result;
    }

    /**
     * Retrieves whether a user with the given login name exists.
     *
     * @param aLoginName
     *            The login name
     * @return True if the user exists
     */
    public Boolean existsByLoginName(String aLoginName) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("userExistsByLoginName " + aLoginName);
        }
        Query query = getEm().createNamedQuery(User.EXISTS_BY_LOGIN_NAME);
        query.setParameter(LOGIN_NAME, aLoginName);
        Long count = (Long) query.getSingleResult();
        boolean result = count > 0;
        if (LOG.isDebugEnabled()) {
            LOG.debug("userExistsByLoginName.result " + result);
        }
        return result;
    }

    /**
     * Retrieves a User based on a UserNumber.
     *
     * @param aUserNumber
     *            The UserNumber
     * @return A user
     */
    public User findUserByUserNumber(String aUserNumber) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("findUserByUserNumber " + aUserNumber);
        }
        User result = null;
        TypedQuery<User> query = getEm().createNamedQuery(User.FIND_USER_BY_USER_NUMBER, User.class);
        query.setParameter(USER_NUMBER, aUserNumber);
        List<User> users = query.getResultList();
        if (users != null && !users.isEmpty()) {
            result = users.get(0);
        }
        return result;
    }

    /**
     * Retrieves a users based on a GroupId.
     *
     * @param groupId
     *            the group id
     * @return A user
     */
    public List<User> findUsersByGroupIds(String groupId) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("findUsersByGroupIds " + groupId);
        }
        TypedQuery<User> query = getEm().createNamedQuery(User.FIND_USERS_IN_GROUP, User.class);
        query.setParameter(GROUP_NAME, groupId);
        return query.getResultList();
    }

    /**
     * Creates a new Arma User with the specified LoginName.
     *
     * @param aLoginName
     *            the a login name
     * @param aUserNumber
     *            the a user number
     * @return isSuccess
     */
    public User createNewEmptyUser(String aLoginName, String aUserNumber) {
        User user = null;

        if (LOG.isInfoEnabled()) {
            LOG.info("Adding user to ARMA SQL database:" + aLoginName);
        }
        if (aLoginName != null && !aLoginName.isEmpty()) {
            user = new User();
            user.setLoginName(aLoginName.toUpperCase());
            if (aUserNumber != null && !aUserNumber.isEmpty()) {
                user.setUserNumber(aUserNumber);
            }
            user.setKulanzId(0);
            user.setMaximumApprovalAmount(new BigDecimal(0));
            user.setMaximumPaymentAmount(new BigDecimal(0));
            user.setArchivingEnabled(false);
            persist(user);
            flush();
        }
        return user;
    }

    /**
     * Removes all users that are not present in the given list from the SQL
     * databases.
     *
     * @param userKeys
     *            the user keys
     * @return the number of deleted users
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public int removeUnlistedUsers(List<UserKey> userKeys) {
        // Get all loginNames from the userkeys
        List<String> loginNames = new ArrayList<String>();
        for (UserKey userKey : userKeys) {
            loginNames.add(userKey.getLoginName());
        }
        // Remove unlisted users with query
        return removeUnlistedLoginNames(loginNames);
    }

    /**
     * Removes all users that are not present in the given list from the SQL
     * databases.
     *
     * @param aLoginNames
     *            The list of names of users to be retained
     * @return the number of deleted users
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public int removeUnlistedLoginNames(List<String> aLoginNames) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("removeUnlistedLoginNames " + aLoginNames);
        }
        EntityManager em = getEm();
        TypedQuery<User> query = em.createNamedQuery(User.FIND_ALL_NOT_IN_LIST, User.class);
        query.setParameter(LOGIN_NAMES, aLoginNames);
        List<User> usersNotInList = query.getResultList();
        for (User userToBeRemoved : usersNotInList) {
            userToBeRemoved.setGroups(null);
            em.merge(userToBeRemoved);
            em.remove(userToBeRemoved);
        }
        return usersNotInList.size();
    }

    /**
     * <b>CURRENTLY IN DEVELOPMENT</b>, <br>
     * Returns <b>double users</b><br>
     * Only considers <b>LoginName</b> and <b>UserNumber</b> criteria.
     *
     * @param searchCrit
     *            specifically for SQL database queries.
     * @return a list of found users
     */
    public List<User> findUserBySearchCriteria(UserDAOSearchCriteria searchCrit) {
        // TODO Complete Implementation
        List<User> foundUsers = new ArrayList<User>();
        foundUsers.add(this.findUserByLoginName(searchCrit.getLoginName()));
        foundUsers.add(this.findUserByUserNumber(searchCrit.getUserNumber()));
        return foundUsers;
    }

    /**
     * Refreshes the given User entity.
     *
     * @param user
     *            the user
     */
    public void refresh(User user) {
        getEm().refresh(user);
    }

    /**
     * Merges the given User entity.
     *
     * @param user
     *            the user
     */
    public void merge(User user) {
        getEm().merge(user);
    }

    /**
     * Flushes the given User entity.
     */
    public void flush() {
        getEm().flush();
    }

}
