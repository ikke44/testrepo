package be.dkv.arma.dao.query;

import java.util.Set;

import javax.persistence.TypedQuery;

import be.dkv.arma.entity.ApplicationUrl;
import be.dkv.arma.entity.Group;
import be.dkv.fw.dao.query.ListResultQuery;

/**
 * FindApplicationUrlByAccessRights query class.
 *
 * @author dries
 */
public class FindApplicationUrlByAccessRights extends ListResultQuery<ApplicationUrl> {

    /** The Constant QP_ACCESS_RIGHTS. */
    public static final String QP_ACCESS_RIGHTS = "accessRights";

    /** The Constant QUERY_NAME. */
    public final static String QUERY_NAME = "ApplicationUrlDAO.findApplicationUrlByAccessRights";

    /** The Constant QUERY. */
    public final static String QUERY =
            "SELECT DISTINCT au FROM ApplicationUrl au LEFT JOIN au.accessRight ar WHERE ar.name in :"
                    + QP_ACCESS_RIGHTS;

    private Set<String> accessRights;

    /**
     * Find {@link ApplicationUrl} by its accessright. This will search for the
     * {@link ApplicationUrl}s who are linked to the {@link Group} with its name
     * equal to one of the specified accessright.
     * 
     * @param accessRights
     *            the {@link Group}-name of the {@link Group}, linked to the
     *            {@link ApplicationUrl}
     */
    public FindApplicationUrlByAccessRights(Set<String> accessRights) {
        super(QUERY_NAME, ApplicationUrl.class);
        this.accessRights = accessRights;
    }

    public TypedQuery<ApplicationUrl> applyParameters(TypedQuery<ApplicationUrl> typedQuery) {
        return typedQuery.setParameter(QP_ACCESS_RIGHTS, accessRights);
    }

}
