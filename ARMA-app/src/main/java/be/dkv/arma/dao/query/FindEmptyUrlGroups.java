package be.dkv.arma.dao.query;

import be.dkv.arma.entity.UrlGroup;
import be.dkv.fw.dao.query.ListResultQuery;

/**
 * FindApplicationUrlByAccessRights query class.
 *
 * @author dries
 */
public class FindEmptyUrlGroups extends ListResultQuery<UrlGroup>{

    public FindEmptyUrlGroups() {
        super(QUERY_NAME, UrlGroup.class);
    }

    /** The Constant QUERY_NAME. */
    public final static String QUERY_NAME = "UrlGroupDAO.FindEmptyUrlGroups";

    /** The Constant QUERY. */
    public final static String QUERY =
            "SELECT ug FROM UrlGroup ug left outer join ug.applicationUrls au WHERE au.urlGroup is null";

}
