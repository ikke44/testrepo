package be.dkv.arma.dao;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

import be.dkv.fw.dao.AbstractDAO;
import be.dkv.fw.entity.Entity;

/**
 * The abstract DAO for ARMA, which contains the entityManager and datasource.
 * 
 * @author geenens
 * @author vanhout
 * 
 * @param <T>
 *            The entity class.
 * @param <U>
 *            The id class.
 */
public abstract class AbstractARMADAO<T extends Entity<U>, U> extends AbstractDAO<T, U> {

    private static final String PU_UNIT_NAME = "ARMA-persistence";

    private EntityManager em;

    @Resource(name = "jdbc/arma")
    private DataSource dataSource;

    /**
     * The constructor.
     * 
     * @param aClazz
     *            the entity class.
     */
    protected AbstractARMADAO(Class<T> aClazz) {
        super(aClazz);
    }

    @Override
    protected EntityManager getEm() {
        return this.em;
    }

    @Override
    protected DataSource getDataSource() {
        return this.dataSource;
    }

    /**
     * Setter for the em.
     * 
     * @param aEm
     *            The em.
     */
    @PersistenceContext(unitName = PU_UNIT_NAME)
    public void setEm(EntityManager aEm) {
        this.em = aEm;
    }

    /**
     * Setter for dataSource.
     * 
     * @param aDataSource
     *            The data source.
     */
    public void setDataSource(DataSource aDataSource) {
        this.dataSource = aDataSource;
    }

}
