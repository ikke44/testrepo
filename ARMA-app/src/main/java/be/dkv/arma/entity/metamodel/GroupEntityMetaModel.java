package be.dkv.arma.entity.metamodel;

public abstract class GroupEntityMetaModel {

    /** GroupEntity name field label. */
    public static final String FIELD_NAME = "name";
}
