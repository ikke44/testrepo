package be.dkv.arma.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import be.dkv.arma.model.ImportSource;
import be.dkv.fw.entity.AbstractEntity;

/**
 * Group entity class.
 *
 * @author dobbelae
 */
@Entity
@Table(name = "ARMAGroups")
@NamedQueries({
        @NamedQuery(name = Group.FIND_ALL_GROUPS, query = "SELECT d FROM Group d ORDER BY d.name ASC"),
        @NamedQuery(
                    name = Group.REMOVE_ALL_IN_LIST_WITH_IMPORTSOURCE,
                    query = "DELETE FROM Group d WHERE (d.name IN (:names) AND d.source = :aImportSource)"),
        @NamedQuery(
                    name = Group.CHANGE_IMPORTSOURCE_IN_LIST,
                    query = "UPDATE Group d SET d.source = :aImportSource  WHERE (d.name IN (:names) AND d.source = :aPreviousImportSource)")

})
public class Group extends AbstractEntity {
    private static final long serialVersionUID = 88705112742905452L;

    /** The query name for group.findAllGroups. */
    public static final String FIND_ALL_GROUPS = "findAllGroups";
    /** The query name for group.removeAllInList. */
    public static final String REMOVE_ALL_IN_LIST_WITH_IMPORTSOURCE = "removeAllInList";
    /** The query name for group.removeAllInList. */
    public static final String CHANGE_IMPORTSOURCE_IN_LIST = "changeImportSourceInList";

    // PARAMETER NAMES
    /** The groupnames query parameter. */
    public static final String GROUP_NAME_LIST = "names";
    /** The importSource query parameter. */
    public static final String IMPORTSOURCE = "aImportSource";
    /** The importSource query parameter. */
    public static final String PREVIOUS_IMPORTSOURCE = "aPreviousImportSource";

    @Column(unique = true, nullable = false)
    private String name;
    @Column(unique = false, nullable = true)
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(unique = false, nullable = true)
    private ImportSource source;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    public ImportSource getSource() {
        return source;
    }

    public void setSource(ImportSource source) {
        this.source = source;
    }

}
