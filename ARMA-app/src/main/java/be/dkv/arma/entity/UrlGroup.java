package be.dkv.arma.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import be.dkv.arma.dao.query.FindEmptyUrlGroups;
import be.dkv.fw.entity.AbstractEntity;

/**
 * UrlGroup entity class.
 * 
 * @author dries
 */
@Entity
@NamedQueries({ @NamedQuery(name = FindEmptyUrlGroups.QUERY_NAME, query = FindEmptyUrlGroups.QUERY) })
public class UrlGroup extends AbstractEntity {
    private static final long serialVersionUID = -1006251715308236867L;

    private String displayName;
    private String displayOrder;
    private String color;

    @OneToMany(mappedBy = "urlGroup")
    private List<ApplicationUrl> applicationUrls =  new ArrayList<ApplicationUrl>(0);

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String aDisplayName) {
        displayName = aDisplayName;
    }

    public String getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(String aDisplayOrder) {
        displayOrder = aDisplayOrder;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String aColor) {
        color = aColor;
    }

    public List<ApplicationUrl> getApplicationUrls() {
        return applicationUrls;
    }

    public void setApplicationUrls(List<ApplicationUrl> aApplicationUrls) {
        applicationUrls = aApplicationUrls;
    }

}
