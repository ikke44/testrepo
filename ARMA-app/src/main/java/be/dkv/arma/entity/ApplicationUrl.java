package be.dkv.arma.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import be.dkv.arma.dao.query.FindApplicationUrlByAccessRights;
import be.dkv.arma.dao.query.FindUnaccessibleApplicationUrlByAccessRights;
import be.dkv.fw.entity.AbstractEntity;

/**
 * ApplicationUrl entity class.
 * 
 * @author dries
 */
@Entity
@NamedQueries({
        @NamedQuery(name = FindApplicationUrlByAccessRights.QUERY_NAME, query = FindApplicationUrlByAccessRights.QUERY),
        @NamedQuery(
                    name = FindUnaccessibleApplicationUrlByAccessRights.QUERY_NAME,
                    query = FindUnaccessibleApplicationUrlByAccessRights.QUERY) })
public class ApplicationUrl extends AbstractEntity {
    private static final long serialVersionUID = -802561433761802107L;

    private String displayName;
    private String url;
    @Column(length = 50)
    private String applicationName;
    @Column(length = 50)
    private String applicationDescription;
    private Integer displayOrder;
    private String color;

    @ManyToOne(fetch = FetchType.LAZY)
    private UrlGroup urlGroup;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
               name = "ApplicationUrl_ARMAGroups",
               joinColumns = { @JoinColumn(name = "ApplicationUrl_id", nullable = false) },
               inverseJoinColumns = { @JoinColumn(name = "ARMAGroups_id", nullable = false) })
    private Set<Group> accessRight = new HashSet<Group>(0);

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String aDisplayName) {
        displayName = aDisplayName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String aUrl) {
        url = aUrl;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String aApplicationName) {
        applicationName = aApplicationName;
    }

    public UrlGroup getUrlGroup() {
        return urlGroup;
    }

    public void setUrlGroup(UrlGroup aUrlGroup) {
        urlGroup = aUrlGroup;
    }

    public String getApplicationDescription() {
        return applicationDescription;
    }

    public void setApplicationDescription(String aApplicationDescription) {
        applicationDescription = aApplicationDescription;
    }

    public Integer getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(Integer aDisplayOrder) {
        displayOrder = aDisplayOrder;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String aColor) {
        color = aColor;
    }

    public Set<Group> getAccessRight() {
        return accessRight;
    }

    public void setAccessRight(Set<Group> accessRight) {
        this.accessRight = accessRight;
    }
}
