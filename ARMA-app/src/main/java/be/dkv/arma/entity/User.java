package be.dkv.arma.entity;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import be.dkv.fw.entity.AbstractEntity;

/**
 * User entity class.
 *
 * @author vanhout
 */
@Entity
@Table(name = "ARMAUser")
@NamedQueries({ @NamedQuery(name = User.FIND_USER_BY_LOGIN, query = "SELECT d FROM User d WHERE d.loginName = :aLoginName"),
        @NamedQuery(name = User.FIND_USER_BY_USER_NUMBER, query = "SELECT d FROM User d WHERE d.userNumber = :aUserNumber"),

        @NamedQuery(name = User.REMOVE_ALL_NOT_IN_LIST, query = "DELETE FROM User d WHERE NOT (d.loginName IN (:aLoginNames))"),
        @NamedQuery(name = User.FIND_ALL_NOT_IN_LIST, query = "SELECT d FROM User d WHERE NOT (d.loginName IN (:aLoginNames))"),
        @NamedQuery(name = User.EXISTS_BY_LOGIN_NAME, query = "SELECT COUNT(d) FROM User d WHERE d.loginName = :aLoginName"),
        @NamedQuery(name = User.FIND_USERS_IN_GROUP, query = "SELECT u FROM User u JOIN u.groups g WHERE g.name = :aGroupName") })
public class User extends AbstractEntity {
    /** The query name for user.findByLogin. */
    public static final String FIND_USER_BY_LOGIN = "findUserByLogin";
    /** The query name for user.removeExceptInList. */
    public static final String REMOVE_ALL_NOT_IN_LIST = "removeAllNotInList";
    /** The query name for user.FindAllNotInList. */
    public static final String FIND_ALL_NOT_IN_LIST = "findAllNotInList";
    /** The query name for user.existsByLoginName. */
    public static final String EXISTS_BY_LOGIN_NAME = "existsByLoginName";
    /** The query name for user.findByLogin. */
    public static final String FIND_USER_BY_USER_NUMBER = "findUserByUserNumber";
    /** The query name for user.findByLogin. */
    public static final String CREATE_NEW_USER = "createNewUser";
    /** The query name for user.findUsersInGroup. */
    public static final String FIND_USERS_IN_GROUP = "findUsersInGroup";
    /** The query name for SynchronizeArmaUser. */
    public static final String QUERY_SYNCHRONIZE_ALL_USERS = "{CALL SynchronizeAllArmaUsers()}";
    /** The query name for SynchronizeArmaUser. */
    public static final String QUERY_SYNCHRONIZE_USER = "{CALL SynchronizeArmaUser(?)}";
    // *STORED PROCEDURE INT PARAMS*//
    /** The Constant PARAM_USERLOGIN. */
    public static final int PARAM_USERLOGIN = 1;

    private static final long serialVersionUID = -1062602747166933418L;

    @Column(unique = true, nullable = false)
    private String loginName;
    @Column(unique = true, nullable = true)
    private String userNumber;
    @Column
    private String fullName;
    @Column
    private BigDecimal maximumApprovalAmount;
    @Column
    private BigDecimal maximumPaymentAmount;
    @Column
    private Integer kulanzId;
    @Column
    private String verificatorLoginName;
    @Column
    private String verificatorUserNumber;
    @Column
    private Boolean archivingEnabled;
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar archiveAcceptedOn;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinTable(name = "ARMAUsers_ARMAGroups", joinColumns = { @JoinColumn(name = "ARMAUser_id", nullable = false) },
               inverseJoinColumns = { @JoinColumn(name = "ARMAGroups_id", nullable = false) })
    private Set<Group> groups = new HashSet<Group>(0);

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getUserNumber() {
        return userNumber;
    }

    public void setUserNumber(String userNumber) {
        this.userNumber = userNumber;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public BigDecimal getMaximumApprovalAmount() {
        return maximumApprovalAmount;
    }

    public void setMaximumApprovalAmount(BigDecimal maximumApprovalAmount) {
        this.maximumApprovalAmount = maximumApprovalAmount;
    }

    public BigDecimal getMaximumPaymentAmount() {
        return maximumPaymentAmount;
    }

    public void setMaximumPaymentAmount(BigDecimal maximumPaymentAmount) {
        this.maximumPaymentAmount = maximumPaymentAmount;
    }

    public Integer getKulanzId() {
        return kulanzId;
    }

    public void setKulanzId(Integer kulanzId) {
        this.kulanzId = kulanzId;
    }

    public String getVerificatorLoginName() {
        return verificatorLoginName;
    }

    public void setVerificatorLoginName(String verificatorLoginName) {
        this.verificatorLoginName = verificatorLoginName;
    }

    public String getVerificatorUserNumber() {
        return verificatorUserNumber;
    }

    public void setVerificatorUserNumber(String verificatorUserNumber) {
        this.verificatorUserNumber = verificatorUserNumber;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    public Set<Group> getGroups() {
        return groups;
    }

    public void setGroups(Set<Group> groups) {
        this.groups = groups;
    }

    public Boolean isArchivingEnabled() {
        return archivingEnabled;
    }

    public void setArchivingEnabled(Boolean aArchivingEnabled) {
        archivingEnabled = aArchivingEnabled;
    }

    public Calendar getArchiveAcceptedOn() {
        return archiveAcceptedOn;
    }

    public void setArchiveAcceptedOn(Calendar aArchiveAcceptedOn) {
        archiveAcceptedOn = aArchiveAcceptedOn;
    }

}
