package be.dkv.arma.migration.result;

import java.util.Collection;

import be.dkv.arma.migration.model.UserGroupMigrationDTO;
import be.dkv.arma.result.AbstractARMAListResult;

/**
 * @author dobbelae
 *
 */
public class UserGroupMigrationListResult extends AbstractARMAListResult<UserGroupMigrationDTO> {

    private static final long serialVersionUID = 2039799545949626733L;

    @Override
    public Collection<UserGroupMigrationDTO> getValue() {
        return this.value;
    }

    @Override
    public void setValue(Collection<UserGroupMigrationDTO> aValue) {
        this.value = aValue;
    }

}
