package be.dkv.arma.migration.model;

import java.util.List;

import be.dkv.arma.model.AbstractARMADTO;

public class UserGroupMigrationDTO extends AbstractARMADTO {

    private static final long serialVersionUID = -286583002225198237L;

    String userName;
    List<String> groupsToBeAddedToLDAP;
    List<String> groupsToBeRemovedFromLDAP;

    public List<String> getGroupsToBeAddedToLDAP() {
        return groupsToBeAddedToLDAP;
    }

    public void setGroupsToBeAddedToLDAP(List<String> groupsToBeAddedToLDAP) {
        this.groupsToBeAddedToLDAP = groupsToBeAddedToLDAP;
    }

    public List<String> getGroupsToBeRemovedFromLDAP() {
        return groupsToBeRemovedFromLDAP;
    }

    public void setGroupsToBeRemovedFromLDAP(List<String> groupsToBeRemovedFromLDAP) {
        this.groupsToBeRemovedFromLDAP = groupsToBeRemovedFromLDAP;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * Checks if this entry needs migration actions.
     *
     * @return true, if there are groups to be added or removed from LDAP.
     */
    public boolean needsMigrationActions() {
        return (groupsToBeAddedToLDAP != null && !groupsToBeAddedToLDAP.isEmpty()) || (groupsToBeRemovedFromLDAP != null
                && !groupsToBeRemovedFromLDAP.isEmpty());
    }

}
