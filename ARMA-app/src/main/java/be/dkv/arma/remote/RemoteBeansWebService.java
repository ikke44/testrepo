package be.dkv.arma.remote;

import javax.ejb.Stateless;
import javax.jws.WebService;

import be.dkv.fw.remote.AbstractRemoteBeansWebService;

@WebService(serviceName = "ARMA")
@Stateless
public class RemoteBeansWebService extends AbstractRemoteBeansWebService {

}
