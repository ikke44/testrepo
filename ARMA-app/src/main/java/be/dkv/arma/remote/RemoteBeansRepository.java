package be.dkv.arma.remote;

import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

import be.dkv.fw.remote.AbstractRemoteBeansRepository;

@Startup
@Singleton
public class RemoteBeansRepository extends AbstractRemoteBeansRepository {
    @PersistenceContext(unitName = "RemoteBeans")
    private EntityManager em;

    @Resource(mappedName = "jdbc/remoteBeans")
    private DataSource dataSource;

    public EntityManager getEm() {
        return em;
    }

    public DataSource getDataSource() {
        return dataSource;
    }
}
