package be.dkv.arma.mapper;

import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;

import be.dkv.arma.exception.LDAPException;

/**
 * Utility to easily extract {@link Attributes} values from an LDAP query
 * SearchResult.
 *
 * @author vanhout
 *
 */
public final class AttributeHandler {

    private AttributeHandler() {
    }

    /**
     * Returns the value of an attribute as a String.
     *
     * @param aAttributes
     *            The attributes
     * @param aAttributeName
     *            The name of the attribute
     * @return The String value of an attribute
     * @throws LDAPException
     *             the LDAP exception
     */
    public static String valueAsString(final Attributes aAttributes, final String aAttributeName) throws LDAPException {

        String result = null;
        Attribute attribute = aAttributes.get(aAttributeName);
        if (attribute != null) {
            try {
                result = String.valueOf(attribute.get());
            } catch (NamingException e) {
                throw new LDAPException("LDAP naming exception when reading value of attribute" + aAttributeName);
            }
        }
        return result;
    }
}
