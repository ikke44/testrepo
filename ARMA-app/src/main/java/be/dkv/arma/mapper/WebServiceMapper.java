package be.dkv.arma.mapper;

import java.util.ArrayList;
import java.util.List;

import be.dkv.arma.model.AbstractARMADTO;

// TODO: Auto-generated Javadoc
/**
 * The Class WebServiceMapper.
 */
public class WebServiceMapper {

    /**
     * Instantiates a new web service mapper.
     */
    private WebServiceMapper() {
    }

    /**
     * Map to string.
     *
     * @param aDTOList the a dto list
     * @return the list
     */
    public static List<String> mapToString(List<? extends AbstractARMADTO> aDTOList) {
        List<String> mappedList = new ArrayList<String>();
        if (aDTOList != null) {
            for (AbstractARMADTO aDTO : aDTOList) {
                mappedList.add(aDTO.toString());
            }
        }
        return mappedList;
    }

}
