package be.dkv.arma.mapper;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.naming.directory.Attributes;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import be.dkv.arma.entity.User;
import be.dkv.arma.exception.LDAPException;
import be.dkv.arma.model.GroupDTO;
import be.dkv.arma.model.UserDTO;
import be.dkv.as400.common.enums.Language;
import be.dkv.fw.exception.DKVBusinessException;

/**
 * Maps the {@link Attributes} of a LDAP query SearchResult to a {@link UserDTO}
 * .
 *
 * @author vanhout
 *
 */
public final class UserMapper {

    private static final Logger LOG = Logger.getLogger(UserMapper.class);

    /** The LDAP physicalDeliveryOfficeName attribute. */
    public static final String USER_OFFICER_NUMBER = "physicalDeliveryOfficeName";
    /** The LDAP sAMAccountName attribute. */
    public static final String USER_LOGIN = "sAMAccountName";
    /** The LDAP name attribute. */
    public static final String USERNAME = "name";
    /**
     * The LDAP memberof attribute, which contains the groups to which the user
     * belongs.
     */
    public static final String MEMBER_OF = "memberof";
    /** The LDAP distinguishedName attribute. */
    public static final String DISTINGUISHED_NAME = "distinguishedName";
    /** The LDAP displayName attribute. */
    public static final String DISPLAYNAME = "displayName";
    /** The LDAP givenName attribute. */
    public static final String FIRSTNAME = "givenName";
    /** The LDAP sn attribute. */
    public static final String LASTNAME = "sn";
    /** The LDAP mail attribute. */
    public static final String MAIL = "mail";
    /** The LDAP department attribute. */
    public static final String LANGUAGE = "department";
    /** The LDAP description attribute. */
    public static final String DEPARTMENT = "description";
    /** The LDAP manager attribute. */
    public static final String MANAGER = "manager";
    /** The LDAP CommonName attribute. */
    public static final String CN = "CN";
    /** The LDAP usernumber attribute. */
    public static final String USERNUMBER = "physicalDeliveryOfficeName";

    private UserMapper() {
    }

    /**
     * Maps the {@link Attributes} of a LDAP query SearchResult to a
     * {@link UserDTO}. <br>
     * <b>WARNING:</b> this will not map the ManagerUserNumber Attribute, as it
     * does not exist within these Attributes. A seperate lookup with the
     * function
     * {@link be.dkv.arma.ldap.LDAPService#findUserNumberByDistinguishedName(String)
     * findUserNumberByDistinguishedName} is recommended to be used after this
     * function.
     *
     * @param aAttributes
     *            Attributes of an LDAP query SearchResult
     * @return A UserDTO containing only LDAP information.
     * @throws LDAPException
     *             the LDAP exception
     */
    public static UserDTO map(Attributes aAttributes) throws LDAPException {
        UserDTO user = null;
        if (aAttributes != null) {
            user = new UserDTO();
            String loginName = AttributeHandler.valueAsString(aAttributes, USER_LOGIN);
            user.setLoginName(loginName.toUpperCase(Locale.getDefault()));
            user.setUserNumber(AttributeHandler.valueAsString(aAttributes, USERNUMBER));
            user.setUserName(AttributeHandler.valueAsString(aAttributes, USERNAME));
            user.setDisplayName(AttributeHandler.valueAsString(aAttributes, DISPLAYNAME));
            user.setFirstName(AttributeHandler.valueAsString(aAttributes, FIRSTNAME));
            user.setLastName(AttributeHandler.valueAsString(aAttributes, LASTNAME));
            user.setEmail(AttributeHandler.valueAsString(aAttributes, MAIL));
            user.setDepartment(AttributeHandler.valueAsString(aAttributes, DEPARTMENT));

            String languageCode = AttributeHandler.valueAsString(aAttributes, LANGUAGE);
            if(!StringUtils.isEmpty(languageCode)){
                Language importedLdapLanguage =
                        Language.getByLanguageCode(languageCode);
                if (importedLdapLanguage != null) {
                    user.setLanguageCode(importedLdapLanguage);
                } else {
                    LOG.fatal("LDAP LanguageCode could not be mapped: '"
                            + AttributeHandler.valueAsString(aAttributes, LANGUAGE) + "'");
                }
            }
            
            String managerDistinguishedName = AttributeHandler.valueAsString(aAttributes, MANAGER);
            if (managerDistinguishedName != null) {
                int cnStart = managerDistinguishedName.indexOf(CN) + CN.length() + 1;
                int cnStop = managerDistinguishedName.indexOf(',', cnStart);
                String managerCN = managerDistinguishedName.substring(cnStart, cnStop);
                user.setManagerName(managerCN);
            }
        } else {
            throw new LDAPException("Attributes object passed to mapper should not be null");
        }
        return user;
    }

    /**
     * Maps the properties of a {@link User} to a {@link UserDTO}. <br>
     * Accepts <b>null</b> values as arguments.
     *
     * @param aUserDTO
     *            A userDTO with existing user information. (can be null)
     * @param aUser
     *            A user
     * @return The modified UserDTO
     * @throws DKVBusinessException
     *             if the user contains invalid groups.
     */
    public static UserDTO mapFromUser(UserDTO aUserDTO, User aUser) throws DKVBusinessException {
        UserDTO returnedUserDTO = null;
        if (aUserDTO == null) {
            returnedUserDTO = new UserDTO();
        } else {
            returnedUserDTO = aUserDTO;
        }
        if (aUser != null) {
            returnedUserDTO.setLoginName(aUser.getLoginName().toUpperCase());
            if (returnedUserDTO.getUserNumber() == null || returnedUserDTO.getUserNumber().isEmpty()) {
                returnedUserDTO.setUserNumber(aUser.getUserNumber());
            }
            returnedUserDTO.setFullName(aUser.getFullName());
            returnedUserDTO.setMaximumApprovalAmount(aUser.getMaximumApprovalAmount());
            returnedUserDTO.setMaximumPaymentAmount(aUser.getMaximumPaymentAmount());
            returnedUserDTO.setKulanzId(aUser.getKulanzId());
            returnedUserDTO.setVerificatorLoginName(aUser.getVerificatorLoginName());
            returnedUserDTO.setVerificatorUserNumber(aUser.getVerificatorUserNumber());

            List<GroupDTO> mappedGroups = GroupMapper.mapToDTO(aUser.getGroups());
            List<GroupDTO> mappedDTOGroups = returnedUserDTO.getGroups();
            returnedUserDTO.setGroups(GroupMapper.mergeToUniqueList(mappedGroups, mappedDTOGroups));

        }

        return returnedUserDTO;
    }

    /**
     * @return the attribute names
     */
    public static String[] getAttributeNames() {
        return new String[] { USER_OFFICER_NUMBER, USER_LOGIN, USERNAME, DISPLAYNAME, FIRSTNAME, LASTNAME, MAIL,
                MEMBER_OF, DEPARTMENT, LANGUAGE, MANAGER, USERNUMBER };
    }

    /**
     * @return the minimal attribute names
     */
    public static String[] getMinimalAttributeNames() {
        return new String[] { USER_OFFICER_NUMBER, USER_LOGIN, USERNAME, DISPLAYNAME, FIRSTNAME, LASTNAME, MAIL,
                USERNUMBER };
    }

    /**
     * Merges two lists of User related data based on their LoginName.
     *
     * @param daoUsers
     *            a List containing User entities
     * @param ldapUsers
     *            a List containing UserDTO's
     * @param commonUsersOnly
     *            if true, only the users whose login name appear in both list
     *            will be merged, others will be discarded. If false, all users
     *            will be merged into one list.
     * @return a List of UserDTOs, the result of the merge.
     * @throws DKVBusinessException see message details
     */
    public static List<UserDTO> merge(List<User> daoUsers, List<UserDTO> ldapUsers, boolean commonUsersOnly)
            throws DKVBusinessException {
        List<UserDTO> mergedResults = new ArrayList<UserDTO>();

        Set<String> uniqueLoginNames = new HashSet<String>();
        uniqueLoginNames.addAll(getLoginNamesFromEntityList(daoUsers));
        uniqueLoginNames.addAll(getLoginNamesFromDTOList(ldapUsers));

        for (String userLogin : uniqueLoginNames) {
            User daoUserReference = null;
            UserDTO ldapUserDTOReference = null;
            ldapUserDTOReference = getUserDTOFromList(ldapUsers, userLogin);
            daoUserReference = getUserFromList(daoUsers, userLogin);
            if (commonUsersOnly) {
                if (ldapUserDTOReference != null && daoUserReference != null) {
                    mergedResults.add(UserMapper.mapFromUser(ldapUserDTOReference, daoUserReference));
                }
            } else {
                mergedResults.add(UserMapper.mapFromUser(ldapUserDTOReference, daoUserReference));
            }
        }
        return mergedResults;
    }

    /**
     * @param daoUsers
     *            a list of User entities
     * @param userLogin
     *            this name of the user that needs to be found
     * @return a User
     */
    private static User getUserFromList(List<User> daoUsers, String userLogin) {
        for (User daoUser : daoUsers) {
            if (daoUser != null && userLogin.equalsIgnoreCase(daoUser.getLoginName())) {
                return daoUser;
            }
        }
        return null;
    }

    /**
     * @param userDTOList
     *            a list of UserDTO's
     * @param userLogin
     *            this name of the user that needs to be found
     * @return a UserDTO
     */
    private static UserDTO getUserDTOFromList(List<UserDTO> userDTOList, String userLogin) {
        for (UserDTO ldapUserDTO : userDTOList) {
            if (ldapUserDTO != null && userLogin.equalsIgnoreCase(ldapUserDTO.getLoginName())) {
                return ldapUserDTO;
            }
        }
        return null;
    }

    /**
     * @param users
     *            a list of User entities
     * @return a Set of unique LoginNames
     */
    public static Set<String> getLoginNamesFromEntityList(List<User> users) {
        Set<String> loginNames = new HashSet<String>();
        for (User user : users) {
            if (user != null && user.getLoginName() != null)
                loginNames.add(user.getLoginName());
        }
        return loginNames;
    }

    /**
     * @param users
     *            a list of UserDTO's
     * @return a Set of unique LoginNames
     */
    public static Set<String> getLoginNamesFromDTOList(List<UserDTO> users) {
        Set<String> loginNames = new HashSet<String>();
        for (UserDTO user : users) {
            if (user != null && user.getLoginName() != null)
                loginNames.add(user.getLoginName());
        }
        return loginNames;
    }

}
