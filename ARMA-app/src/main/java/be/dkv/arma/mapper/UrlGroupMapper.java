package be.dkv.arma.mapper;

import java.util.ArrayList;
import java.util.List;

import be.dkv.arma.entity.UrlGroup;
import be.dkv.arma.model.UrlGroupDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class UrlGroupMapper.
 */
public class UrlGroupMapper {

    /**
     * Instantiates a new url group mapper.
     */
    private UrlGroupMapper() {
    }

    /**
     * Map.
     *
     * @param aUrlGroupDTO the a url group dto
     * @return the url group
     */
    public static UrlGroup map(UrlGroupDTO aUrlGroupDTO) {
        UrlGroup urlGroup = new UrlGroup();
        urlGroup.setColor(aUrlGroupDTO.getColor());
        urlGroup.setDisplayName(aUrlGroupDTO.getDisplayName());
        urlGroup.setDisplayOrder(aUrlGroupDTO.getDisplayOrder());
        return urlGroup;
    }

    /**
     * Map.
     *
     * @param aUrlGroup the a url group
     * @return the url group dto
     */
    public static UrlGroupDTO map(UrlGroup aUrlGroup) {
        UrlGroupDTO urlGroupDTO = new UrlGroupDTO();
        urlGroupDTO.setColor(aUrlGroup.getColor());
        urlGroupDTO.setDisplayName(aUrlGroup.getDisplayName());
        urlGroupDTO.setDisplayOrder(aUrlGroup.getDisplayOrder());
        urlGroupDTO.setId(aUrlGroup.getId());
        return urlGroupDTO;
    }

    /**
     * Map all to dto.
     *
     * @param urlGroups the url groups
     * @return the list
     */
    public static List<UrlGroupDTO> mapAllToDTO(List<UrlGroup> urlGroups) {
        List<UrlGroupDTO> urlGroupDTOs = new ArrayList<UrlGroupDTO>();
        for (UrlGroup urlGroup : urlGroups) {
            urlGroupDTOs.add(map(urlGroup));
        }

        return urlGroupDTOs;
    }
}
