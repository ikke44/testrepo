package be.dkv.arma.mapper;

import java.util.ArrayList;
import java.util.List;

import be.dkv.arma.entity.ApplicationUrl;
import be.dkv.arma.model.ApplicationUrlDTO;

// TODO: Auto-generated Javadoc

/**
 * The Class ApplicationUrlMapper.
 */
public class ApplicationUrlMapper {

    /**
     * Instantiates a new application url mapper.
     */
    private ApplicationUrlMapper() {
    }

    /**
     * Map.
     *
     * @param aApplicationUrl the an ApplicationUrl
     * @return the application url dto
     */
    public static ApplicationUrlDTO map(ApplicationUrl aApplicationUrl) {
        ApplicationUrlDTO applicationUrlDTO = new ApplicationUrlDTO();
        applicationUrlDTO.setAccessRight(GroupMapper.mapToDTO(aApplicationUrl.getAccessRight()));
        applicationUrlDTO.setApplicationName(aApplicationUrl.getApplicationName());
        applicationUrlDTO.setColor(aApplicationUrl.getColor());
        applicationUrlDTO.setApplicationDescription(aApplicationUrl.getApplicationDescription());
        applicationUrlDTO.setDisplayName(aApplicationUrl.getDisplayName());
        applicationUrlDTO.setDisplayOrder(aApplicationUrl.getDisplayOrder());
        applicationUrlDTO.setUrl(aApplicationUrl.getUrl());
        applicationUrlDTO.setId(aApplicationUrl.getId());

        return applicationUrlDTO;
    }

    /**
     * Maps the DTO onto the Entity. If no ApplicationUrl was
     * provided, a new one is created. <b>Does not map the
     * AccessRight</b>
     *
     * @param aApplicationUrlDTO the source
     * @param applicationUrl     the destination
     * @return the application url
     */
    public static ApplicationUrl mapOntoEntity(ApplicationUrlDTO aApplicationUrlDTO, ApplicationUrl applicationUrl) {
        if (applicationUrl == null) {
            return new ApplicationUrl();
        }
        // applicationUrl.setAccessRight(GroupMapper.mapToEntity(aApplicationUrlDTO.getAccessRight()));
        applicationUrl.setApplicationName(aApplicationUrlDTO.getApplicationName());
        applicationUrl.setColor(aApplicationUrlDTO.getColor());
        applicationUrl.setApplicationDescription(aApplicationUrlDTO.getApplicationDescription());
        applicationUrl.setDisplayName(aApplicationUrlDTO.getDisplayName());
        applicationUrl.setDisplayOrder(aApplicationUrlDTO.getDisplayOrder());
        applicationUrl.setUrl(aApplicationUrlDTO.getUrl());
        return applicationUrl;
    }

    /**
     * Map all to dto.
     *
     * @param aApplicationUrls the a application urls
     * @return the list
     */
    public static List<ApplicationUrlDTO> mapAllToDTO(List<ApplicationUrl> aApplicationUrls) {
        List<ApplicationUrlDTO> applicationUrlDTOs = new ArrayList<ApplicationUrlDTO>();
        for (ApplicationUrl applicationUrl : aApplicationUrls) {
            applicationUrlDTOs.add(map(applicationUrl));
        }
        return applicationUrlDTOs;
    }

}
