package be.dkv.arma.mapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.naming.directory.Attributes;

import be.dkv.arma.common.ARMAErrorCodes;
import be.dkv.arma.entity.Group;
import be.dkv.arma.exception.LDAPException;
import be.dkv.arma.ldap.LDAPService;
import be.dkv.arma.model.GroupDTO;
import be.dkv.fw.exception.DKVBusinessException;

// TODO: Auto-generated Javadoc
/**
 * Maps the {@link Attributes} of a LDAP query SearchResult to a
 * {@link GroupDTO}.
 *
 * @author vanhout
 *
 */
public final class GroupMapper {

    /** The LDAP CN attribute, which functions as a group's unique identifier. */
    public static final String ID = "CN";
    /** The LDAP description attribute. */
    public static final String DESCRIPTION = "description";
    /**
     * The LDAP member attribute, which specifies which groups and user belong
     * to the group.
     */
    public static final String MEMBER = "member";

    /**
     * Instantiates a new group mapper.
     */
    private GroupMapper() {
    }

    /**
     * Maps the {@link Attributes} of a LDAP query SearchResult to a
     * {@link GroupDTO}.
     *
     * @param aAttributes
     *            Attributes of an LDAP query SearchResult
     * @param environmentPrefix
     *            the environment prefix
     * @return A GroupDTO
     * @throws LDAPException
     *             the LDAP exception
     */
    public static GroupDTO map(Attributes aAttributes, String environmentPrefix) throws LDAPException {
        GroupDTO group = null;
        if (aAttributes != null) {
            group = new GroupDTO();
            String id = AttributeHandler.valueAsString(aAttributes, ID);
            if (!id.toLowerCase().startsWith(environmentPrefix.toLowerCase())) {
                throw new LDAPException("Group does not belong to correct environment: group=" + id
                        + ", environmentPrefix=" + environmentPrefix);
            }
            id = id.substring(environmentPrefix.length());
            id =
                    id.toLowerCase().replace(LDAPService.LDAP_GROUP_NAME_SEPARATOR,
                            LDAPService.GENERAL_GROUP_NAME_SEPARATOR);
            group.setId(id);
            group.setDescription(AttributeHandler.valueAsString(aAttributes, DESCRIPTION));
        } else {
            throw new LDAPException("Attributes object passed to mapper should not be null");
        }
        return group;
    }

    /**
     * Map.
     *
     * @param group
     *            the group
     * @return the group dto
     */
    public static GroupDTO map(Group group) {
        GroupDTO groupDTO = new GroupDTO();
        groupDTO.setId(group.getName());
        groupDTO.setDescription(group.getDescription());
        return groupDTO;
    }

    /**
     * Map.
     *
     * @param groupDTO
     *            the group dto
     * @return the group
     */
    public static Group mapToEntity(GroupDTO groupDTO) {
        Group group = new Group();
        group.setName(groupDTO.getId());
        group.setDescription(groupDTO.getDescription());
        return group;
    }

    /**
     * This method maps a {@link List} of {@link GroupDTO} to a {@link List} of {@link Group}.
     *
     * @param groupDTOList
     *            a {@link List} of {@link GroupDTO}
     * @return a {@link List} of {@link Group}
     */
    public static Set<Group> mapListToEntitySet(List<GroupDTO> groupDTOList) {
        Set<Group> groupList = new HashSet<Group>();
        for (GroupDTO groupDTO : groupDTOList) {
            groupList.add(mapToEntity(groupDTO));
        }
        return groupList;
    }

    /**
     * This method maps a {@link List} of {@link Group} to a {@link List} of {@link GroupDTO}.
     *
     * @param groupList
     *            a {@link List} of {@link Group}
     * @return a {@link List} of {@link GroupDTO}
     */
    public static List<GroupDTO> mapToDTO(Set<Group> groupList) {
        List<GroupDTO> groupDTOList = new ArrayList<GroupDTO>();
        for (Group group : groupList) {
            groupDTOList.add(map(group));
        }
        return groupDTOList;
    }

    /**
     * Gets the attribute names.
     *
     * @return the attribute names
     */
    public static String[] getAttributeNames() {
        return new String[] { ID, DESCRIPTION, MEMBER };
    }

    /**
     * Takes two {@link List}s of {@link GroupDTO}s and merges them into a new
     * {@link List} based on their GroupIds. When doubles are found, this method
     * will prioritize the {@link GroupDTO} Objects found in the SQL database as
     * they are able to contain a description.
     *
     * @param ldapGroupDTOs
     *            the {@link List} of {@link GroupDTO}s from an LDAP query
     * @param sqlGroupDTOs
     *            the {@link List} of {@link GroupDTO}s from an SQL query
     * @return A {@link List}s of {@link GroupDTO}s
     * @throws DKVBusinessException
     *             the DKV Business Exception
     */
    public static List<GroupDTO> mergeToUniqueList(List<GroupDTO> ldapGroupDTOs, List<GroupDTO> sqlGroupDTOs)
            throws DKVBusinessException {
        List<GroupDTO> mergedGroupDTOs = new ArrayList<GroupDTO>();
        // Put all the Group identifiers in a unique HashSet
        Set<String> groupIdentifiers = new HashSet<String>();
        groupIdentifiers.addAll(mapToAccessRightList(ldapGroupDTOs));
        groupIdentifiers.addAll(mapToAccessRightList(sqlGroupDTOs));

        // Add the GroupDTO Objects to the merged list, based on the unique
        // HashSet "groupIdentifiers"
        if (sqlGroupDTOs != null) {
            for (GroupDTO sqlGroup : sqlGroupDTOs) {
                mergedGroupDTOs.add(sqlGroup);
                groupIdentifiers.remove(sqlGroup.getId());
            }
        }
        // Only add the GroupDTO Objects from ldapGroupDTOs when included in the
        // HashSet "groupIdentifiers" , this ensures uniqueness.
        if (ldapGroupDTOs != null) {
            for (GroupDTO ldapGroup : ldapGroupDTOs) {
                if (groupIdentifiers.contains(ldapGroup.getId())) {
                    mergedGroupDTOs.add(ldapGroup);
                    groupIdentifiers.remove(ldapGroup.getId());
                }
            }
        }
        // after the merge, the groupIdentifiers should be empty
        if (groupIdentifiers.size() != 0) {
            throw new DKVBusinessException(ARMAErrorCodes.ARMA_MERGE_FAILED);
        }

        return mergedGroupDTOs;

    }

    /**
     * Maps the GroupDTO's to an ArrayList of Strings that contain only the
     * GroupId/GroupName (also known as the Accessright).
     *
     * @param groupDTOList
     *            the group dto list
     * @return a Set with the AccesRights (= groupId's) of the GroupDTO's
     */
    public static Set<String> mapToAccessRightList(Collection<GroupDTO> groupDTOList) {

        Set<String> accessrights = new HashSet<String>();
        if (groupDTOList != null) {
            for (GroupDTO groupDTO : groupDTOList) {
                if (groupDTO != null && !accessrights.contains(groupDTO.getId())) {
                    accessrights.add(groupDTO.getId());
                }

            }
        }
        return accessrights;
    }

}
