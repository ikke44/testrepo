package be.dkv.arma.mapper;

import java.math.BigDecimal;
import java.util.List;

import be.dkv.arma.model.GroupDTO;
import be.dkv.arma.model.UserDAOSearchCriteria;
import be.dkv.arma.model.UserLDAPSearchCriteria;
import be.dkv.arma.model.UserSearchCriteriaDTO;

/**
 * This utility Class allows for a UserSearchCriteriaDTO to be deduplicated into
 * its DAO and LDAP component that are needed for distributed search execution.
 *
 * @author dobbelae
 */
public class SearchCriteriaMapper {

    /**
     * Instantiates a new search criteria mapper.
     */
    private SearchCriteriaMapper() {
    }

    /** The Constant SPACE. */
    private static final String SPACE = " ";

    /**
     * Map to dao.
     *
     * @param searchCritDTO            a UserSearchCriteriaDTO.
     * @return UserDAOSearchCriteria with its specific values mapped from a
     *         UserSearchCriteriaDTO.
     */
    public static UserDAOSearchCriteria mapToDAO(UserSearchCriteriaDTO searchCritDTO) {
        UserDAOSearchCriteria searchCritDAO = new UserDAOSearchCriteria();

        searchCritDAO.setFirstName(validate(searchCritDTO.getFirstName()));
        searchCritDAO.setLastName(validate(searchCritDTO.getLastName()));
        searchCritDAO.setLoginName(validate(searchCritDTO.getLoginName()));
        searchCritDAO.setUserNumber(validate(searchCritDTO.getUserNumber()));

        searchCritDAO.setKulanzId(validate(searchCritDTO.getKulanzId()));

        searchCritDAO.setVerificatorLoginName(validate(searchCritDTO.getVerificatorLoginName()));
        searchCritDAO.setVerificatorUserNumber(validate(searchCritDTO.getVerificatorUserNumber()));

        searchCritDAO.setFromMaximumApprovalAmount(validate(searchCritDTO.getFromMaximumApprovalAmount()));
        searchCritDAO.setToMaximumApprovalAmount(validate(searchCritDTO.getToMaximumApprovalAmount()));

        searchCritDAO.setFromMaximumPaymentAmount(validate(searchCritDTO.getFromMaximumPaymentAmount()));
        searchCritDAO.setToMaximumPaymentAmount(validate(searchCritDTO.getToMaximumPaymentAmount()));

        return searchCritDAO;
    }

    /**
     * Map to ldap.
     *
     * @param searchCritDTO            a UserSearchCriteriaDTO.
     * @return UserLDAPSearchCriteria with its specific values mapped from a
     *         UserSearchCriteriaDTO.
     */
    public static UserLDAPSearchCriteria mapToLDAP(UserSearchCriteriaDTO searchCritDTO) {
        UserLDAPSearchCriteria searchCritLDAP = new UserLDAPSearchCriteria();

        searchCritLDAP.setDepartment(validate(searchCritDTO.getDepartment()));
        searchCritLDAP.setDisplayName(validate(searchCritDTO.getLastName() + SPACE + searchCritDTO.getFirstName()));
        searchCritLDAP.setEmail(validate(searchCritDTO.getEmail()));
        searchCritLDAP.setExternal(validate(searchCritDTO.getExternal()));
        searchCritLDAP.setFirstName(validate(searchCritDTO.getFirstName()));
        searchCritLDAP.setGroups(validate(searchCritDTO.getGroups()));
        searchCritLDAP.setLanguageCode(validate(searchCritDTO.getLanguageCode()));
        searchCritLDAP.setLastName(validate(searchCritDTO.getLastName()));
        searchCritLDAP.setLoginName(validate(searchCritDTO.getLoginName()));
        searchCritLDAP.setManagerName(validate(searchCritDTO.getManagerName()));
        searchCritLDAP.setManagerUserNumber(validate(searchCritDTO.getManagerUserNumber()));
        searchCritLDAP.setUserName(validate(searchCritDTO.getUserNumber()));
        searchCritLDAP.setUserNumber(validate(searchCritDTO.getUserNumber()));

        return searchCritLDAP;
    }

    /**
     * Validate.
     *
     * @param arg            the input argument.
     * @return the validated value of the input arg.
     */
    public static String validate(String arg) {
        if (arg != null && !arg.isEmpty()) {
            return arg;
        } else {
            return null;
        }
    }

    /**
     * Validate.
     *
     * @param arg            the input argument.
     * @return the validated value of the input arg.
     */
    public static BigDecimal validate(BigDecimal arg) {
        if (arg != null) {
            return arg;
        } else {
            return null;
        }
    }

    /**
     * Validate.
     *
     * @param arg            the input argument.
     * @return the validated value of the input arg.
     */
    public static Integer validate(Integer arg) {
        if (arg != null) {
            return arg;
        } else {
            return null;
        }
    }

    /**
     * Validate.
     *
     * @param arg            the input argument.
     * @return the validated value of the input arg.
     */
    public static Boolean validate(Boolean arg) {
        if (arg != null) {
            return arg;
        } else {
            return false;
        }
    }

    /**
     * Validate.
     *
     * @param arg            the input argument.
     * @return the validated value of the input arg.
     */
    public static List<GroupDTO> validate(List<GroupDTO> arg) {
        if (arg != null) {
            return arg;
        } else {
            return null;
        }
    }

}
