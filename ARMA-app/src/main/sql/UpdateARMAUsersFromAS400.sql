-- =============================================
-- Author:		Alain Van Hout, Gwijde Geenens
-- Create date:	2013-08-20
-- Description:	Retrieves the non-AD ARMA user data from AS/400
-- =============================================

UPDATE DEV_ARMA.dbo.ARMAUser
SET DEV_ARMA.dbo.ARMAUser.loginName = LTRIM(RTRIM(s.HTPRF)),
DEV_ARMA.dbo.ARMAUser.maximumAmountApprovement = LTRIM(RTRIM(SUBSTRING(d.filler, 0, 10)))
FROM [AS400TEST].[S44BA388].[DKV_EXTDTA].[SBST] AS s, 
     [AS400TEST].[S44BA388].[DKV_EXTDTA].[DIV00] AS d,
     DEV_ARMA.dbo.ARMAUser as a
WHERE a.loginName = LTRIM(RTRIM(s.HTPRF)) AND LTRIM(RTRIM(s.HTREGKZ)) = LTRIM(RTRIM(SUBSTRING(d.[KEY], 6, 1))) 
AND d.[KEY] LIKE '10661%N'  
      AND LTRIM(RTRIM(SUBSTRING(d.filler, 20, 1))) = ''

UPDATE DEV_ARMA.dbo.ARMAUser
SET DEV_ARMA.dbo.ARMAUser.loginName = LTRIM(RTRIM(s.HTPRF)),
DEV_ARMA.dbo.ARMAUser.maximumAmountApprovement = LTRIM(RTRIM(SUBSTRING(d.filler, 0, 10))),
DEV_ARMA.dbo.ARMAUser.kulanzId = LTRIM(RTRIM(SUBSTRING(d.filler, 20, 1))),
DEV_ARMA.dbo.ARMAUser.kulanzDescription = LTRIM(RTRIM(SUBSTRING(dKulanz.filler, 0, 66)))
FROM [AS400TEST].[S44BA388].[DKV_EXTDTA].[SBST] AS s, 
     [AS400TEST].[S44BA388].[DKV_EXTDTA].[DIV00] AS d,
     [AS400TEST].[S44BA388].[DKV_EXTDTA].[DIV00] AS dKulanz,
     DEV_ARMA.dbo.ARMAUser as a
WHERE a.loginName = LTRIM(RTRIM(s.HTPRF)) AND LTRIM(RTRIM(s.HTREGKZ)) = LTRIM(RTRIM(SUBSTRING(d.[KEY], 6, 1))) 
AND d.[KEY] LIKE '10661%N'   
AND LTRIM(RTRIM(SUBSTRING(dKulanz.[KEY], 6, 1))) 
= LTRIM(RTRIM(SUBSTRING(d.filler, 20, 1))) 
AND dKulanz.[KEY] LIKE '41161%'

