package be.dkv.arma.exception;

import be.dkv.arma.common.ARMAErrorCodes;
import be.dkv.fw.exception.DKVBusinessException;

/**
 * To be thrown when a user is not found.
 * 
 * @author vanhout
 * 
 */
public class UserNotFoundException extends DKVBusinessException {

    private static final long serialVersionUID = -3197771204924891598L;

    public UserNotFoundException(String aUserName) {
        super(ARMAErrorCodes.ARMA_ARMA_USER_NOT_FOUND, "User not found: " + aUserName);
    }
}
