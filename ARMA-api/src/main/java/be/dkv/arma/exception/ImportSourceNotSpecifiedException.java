package be.dkv.arma.exception;

import be.dkv.arma.common.ARMAErrorCodes;
import be.dkv.fw.exception.DKVBusinessException;

/**
 * To be thrown when a user is not found.
 * 
 * @author vanhout
 * 
 */
public class ImportSourceNotSpecifiedException extends DKVBusinessException {

    private static final long serialVersionUID = -3197771204924891598L;

    public ImportSourceNotSpecifiedException() {
        super(ARMAErrorCodes.ARMA_NO_IMPORTSOURCE_SPECIFIED, "ImportSource not specified.");
    }
}
