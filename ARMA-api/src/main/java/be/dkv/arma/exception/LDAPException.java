package be.dkv.arma.exception;

import be.dkv.arma.common.ARMAErrorCodes;
import be.dkv.fw.exception.DKVBusinessException;

/**
 * To be thrown when an LDAP query does not behave as expected.
 * 
 * @author vanhout
 * 
 */
public class LDAPException extends DKVBusinessException {

    private static final long serialVersionUID = -5640779347295008181L;

    public LDAPException(String aMessage) {
        super(ARMAErrorCodes.GLOBAL_GLOBAL_LDAP_PROGRAM_ERROR, aMessage);
    }

    public LDAPException(String aMessage, Throwable aCause) {
        super(ARMAErrorCodes.GLOBAL_GLOBAL_LDAP_PROGRAM_ERROR, aMessage, aCause);
    }
}
