package be.dkv.arma.exception;

import be.dkv.arma.common.ARMAErrorCodes;
import be.dkv.fw.exception.DKVBusinessException;

/**
 * To be thrown when a user is not found.
 *
 * @author vanhout
 *
 */
public class ApplicationUrlNotFoundException extends DKVBusinessException {

    private static final long serialVersionUID = -3197771204924891598L;

    public ApplicationUrlNotFoundException(Long id) {
        super(ARMAErrorCodes.ARMA_APPLICATIONURL_NOT_FOUND, "ApplicationUrl not found " + id);
    }
    public ApplicationUrlNotFoundException() {
        super(ARMAErrorCodes.ARMA_APPLICATIONURL_NOT_FOUND, "ApplicationUrl not found: no argument");
    }
}
