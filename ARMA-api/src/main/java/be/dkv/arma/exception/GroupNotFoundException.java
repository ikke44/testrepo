package be.dkv.arma.exception;

import be.dkv.arma.common.ARMAErrorCodes;
import be.dkv.fw.exception.DKVBusinessException;

/**
 * To be thrown when a group is not found.
 * 
 * @author vanhout
 * 
 */
public final class GroupNotFoundException extends DKVBusinessException {

    private static final long serialVersionUID = 1169323971416570923L;

    public GroupNotFoundException(String aGroupId) {
        super(ARMAErrorCodes.ARMA_ARMA_GROUP_NOT_FOUND, "Group not found: " + aGroupId);
    }
}
