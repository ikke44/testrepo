package be.dkv.arma.exception;

import be.dkv.arma.common.ARMAErrorCodes;
import be.dkv.fw.exception.DKVBusinessException;

/**
 * Thrown when a specific group could not be found in LDAP.
 * 
 * @author mahieus
 * 
 */
public class LDAPGroupNotFoundException extends DKVBusinessException {

    private static final long serialVersionUID = -28906845574008731L;

    public LDAPGroupNotFoundException(String aGroupId) {
        super(ARMAErrorCodes.ARMA_ARMA_GROUP_NOT_FOUND, "Group '" + aGroupId + "' not found in LDAP.");
    }

}
