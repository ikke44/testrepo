package be.dkv.arma.common;

import be.dkv.customannotationprocessor.CheckDKVErrorCodes;

/**
 * final class enclosing all error codes from ARMA.
 *
 * @author ErrorCode-generator
 *
 */
@CheckDKVErrorCodes
public final class ARMAErrorCodes {

    /**
     * Error calling LDAP/Active Directory. Program not available or in
     * technical error.: 1014.
     */
    public static final int GLOBAL_GLOBAL_LDAP_PROGRAM_ERROR = 1014;
    /** The user is not found on Active Directory: 2005. */
    public static final int ARMA_ARMA_USER_NOT_FOUND = 2005;
    /** The group is not found on Active Directory: 2006. */
    public static final int ARMA_ARMA_GROUP_NOT_FOUND = 2006;
    /** the userlogin was not specified: 4226. */
    public static final int ARMA_USERLOGIN_NOT_SPECIFIED = 4226;
    /** No groups could be found on the database: 4470. */
    public static final int ARMA_NO_GROUPS_FOUND_ON_DB = 4470;
    /** No groups could be found on Active Directory: 4471. */
    public static final int ARMA_NO_GROUPS_FOUND_ON_AD = 4471;
    /** Merge Failed: 4472. */
    public static final int ARMA_MERGE_FAILED = 4472;
    /** No ImportSource was specified.: 4473. */
    public static final int ARMA_NO_IMPORTSOURCE_SPECIFIED = 4473;
    /** No ImportSource was found.: 4474. */
    public static final int ARMA_NO_IMPORTSOURCE_FOUND = 4474;
    /** the result is not unique: 4475. */
    public static final int ARMA_RESULT_NOT_UNIQUE = 4475;
    /** The Application Url could not be found: 4476. */
    public static final int ARMA_APPLICATIONURL_NOT_FOUND = 4476;

    private ARMAErrorCodes() {
        // private
    }

}
