package be.dkv.arma.task;

import javax.ejb.Remote;

import be.dkv.fw.task.Task;

/**
 *
 *
 * @author vanhout
 */
@Remote
public interface ArmaADSynchTask extends Task {

    /**
     * The JNDI name of the ARMA Active Directory Synchronization task.
     */
    String JNDI_NAME = "java:global/ARMA/ejb/ArmaADSynchTask";

}
