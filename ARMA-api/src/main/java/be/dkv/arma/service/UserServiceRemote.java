package be.dkv.arma.service;

import java.util.List;

import javax.ejb.Remote;

import be.dkv.arma.model.UserDTO;
import be.dkv.fw.exception.DKVBusinessException;

/**
 * The Interface UserServiceRemote.
 */
@Remote
public interface UserServiceRemote {

    /**
     * Find a user based on his user login.
     *
     * @param aLoginName
     *            user login
     * @return the user
     * @throws DKVBusinessException
     *             thrown when a business exception occurs
     */
    UserDTO findUserByLogin(final String aLoginName) throws DKVBusinessException;

    /**
     * Find a list of users based on the group id.
     *
     * @param aGroupId
     *            the group id
     * @return list of users users
     * @throws DKVBusinessException
     *             thrown when a business exception occurs
     */
    List<UserDTO> findUsersInGroup(final String aGroupId) throws DKVBusinessException;

    /**
     * Find a user based on the given credentials.
     *
     * @return the user details
     * @throws be.dkv.fw.exception.DKVBusinessException
     *             thrown when a business exception occurs
     */
    UserDTO findCurrentUser() throws DKVBusinessException;

    /**
     * Find a user based on his UserNumber.
     *
     * @param userNumber
     *            user number
     * @return the user
     * @throws DKVBusinessException
     *             thrown when a business exception occurs
     */
    UserDTO findUserByUserNumber(String userNumber) throws DKVBusinessException;

    /**
     * Find a list of users based on the provided list of usernames.
     *
     * @param userNameList
     *            the user name list
     * @param displayGroupInfo
     *            if true, the groups will be displayed for every user.
     * @return list of users users
     * @throws DKVBusinessException
     *             thrown when a business exception occurs
     */
    List<UserDTO> findUsersbyLoginNames(final List<String> userNameList, final Boolean displayGroupInfo)
            throws DKVBusinessException;

    /**
     * Find a list of users based on the provided list of groupIds.
     *
     * @param groupIdList
     *            the group id list
     * @param displayGroupInfo
     *            if true, the groups will be displayed for every user.
     * @return list of users users
     * @throws DKVBusinessException
     *             thrown when a business exception occurs
     */
    List<UserDTO> findUsersInGroups(final List<String> groupIdList, final Boolean displayGroupInfo)
            throws DKVBusinessException;

}
