package be.dkv.arma.model;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * The DTO for ARMA groups.
 *
 * @author vanhout
 *
 */
public class GroupDTO extends AbstractARMADTO {

    private static final long serialVersionUID = -2538499646426422365L;


    private String id;

    private String description;

    /**
     * This id equals the id on LDAP and the Groups.name field on SQL database.
     *
     * @return the id
     */
    public String getId() {
        return this.id;
    }
    
    /**
     * This id equals the id on LDAP and the Groups.name field on SQL database.
     *
     * @param aId the new id
     */
    public void setId(String aId) {
        this.id = aId;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String aDescription) {
        this.description = aDescription;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(id).append(description).toHashCode();
    }

    @Override
    public boolean equals(Object obj) {

        boolean equals = false;
        if (this == obj) {
            equals = true;
        } else if (obj == null || getClass() != obj.getClass()) {
            equals = false;
        } else {
            GroupDTO other = (GroupDTO) obj;
            equals =
                    new EqualsBuilder().append(this.id, other.id).append(this.description, other.description)
                            .isEquals();
        }
        return equals;
    }

}
