package be.dkv.arma.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import org.apache.commons.lang.ArrayUtils;

/**
 * @author vanhout
 *
 */
public class IdListRequest extends AbstractARMADTO {

    private static final long serialVersionUID = -7306497037975421210L;

    @XmlElement(required = true)
    private List<String> id;

    public List<String> getId() {
        return id;
    }

    public void setId(List<String> aId) {
        this.id = aId;
    }

    @Override
    public String toString() {
        return ArrayUtils.toString(id, "NULL");
    }

}
