package be.dkv.arma.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * The Class UserSearchCriteriaDTO is used as a data transfer object and to
 * construct SearchCriteria for LDAP and SQL.
 */
public class UserSearchCriteriaDTO implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -1428445699631169596L;
    private String loginName;
    private String userNumber;

    private String firstName;
    private String lastName;
    private String email;
    private String department;
    private String languageCode;

    private BigDecimal fromMaximumApprovalAmount;
    private BigDecimal toMaximumApprovalAmount;
    private BigDecimal fromMaximumPaymentAmount;
    private BigDecimal toMaximumPaymentAmount;

    private Integer kulanzId;

    private String verificatorLoginName;
    private String verificatorUserNumber;

    private String managerName;
    private String managerUserNumber;

    private Boolean external;

    private List<GroupDTO> groups;

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getUserNumber() {
        return userNumber;
    }

    public void setUserNumber(String userNumber) {
        this.userNumber = userNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public BigDecimal getFromMaximumApprovalAmount() {
        return fromMaximumApprovalAmount;
    }

    public void setFromMaximumApprovalAmount(BigDecimal fromMaximumApprovalAmount) {
        this.fromMaximumApprovalAmount = fromMaximumApprovalAmount;
    }

    public BigDecimal getToMaximumApprovalAmount() {
        return toMaximumApprovalAmount;
    }

    public void setToMaximumApprovalAmount(BigDecimal toMaximumApprovalAmount) {
        this.toMaximumApprovalAmount = toMaximumApprovalAmount;
    }

    public BigDecimal getFromMaximumPaymentAmount() {
        return fromMaximumPaymentAmount;
    }

    public void setFromMaximumPaymentAmount(BigDecimal fromMaximumPaymentAmount) {
        this.fromMaximumPaymentAmount = fromMaximumPaymentAmount;
    }

    public BigDecimal getToMaximumPaymentAmount() {
        return toMaximumPaymentAmount;
    }

    public void setToMaximumPaymentAmount(BigDecimal toMaximumPaymentAmount) {
        this.toMaximumPaymentAmount = toMaximumPaymentAmount;
    }

    public Integer getKulanzId() {
        return kulanzId;
    }

    public void setKulanzId(Integer kulanzId) {
        this.kulanzId = kulanzId;
    }

    public String getVerificatorLoginName() {
        return verificatorLoginName;
    }

    public void setVerificatorLoginName(String verificatorLoginName) {
        this.verificatorLoginName = verificatorLoginName;
    }

    public String getVerificatorUserNumber() {
        return verificatorUserNumber;
    }

    public void setVerificatorUserNumber(String verificatorUserNumber) {
        this.verificatorUserNumber = verificatorUserNumber;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getManagerUserNumber() {
        return managerUserNumber;
    }

    public void setManagerUserNumber(String managerUserNumber) {
        this.managerUserNumber = managerUserNumber;
    }

    public List<GroupDTO> getGroups() {
        return groups;
    }

    public void setGroups(List<GroupDTO> groups) {
        this.groups = groups;
    }

    public Boolean getExternal() {
        return external;
    }

    public void setExternal(Boolean external) {
        this.external = external;
    }

}
