package be.dkv.arma.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 * @author vanhout
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class VoidRequest extends AbstractARMADTO {

    private static final long serialVersionUID = 7502824274587001012L;
}
