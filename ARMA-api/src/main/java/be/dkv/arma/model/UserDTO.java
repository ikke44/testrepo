package be.dkv.arma.model;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import be.dkv.as400.common.enums.Language;

/**
 * The DTO for ARMA users.
 *
 * @author vanhout
 *
 */
public class UserDTO extends AbstractARMADTO {

    private static final long serialVersionUID = -5069856025515733168L;

    private String loginName;
    private String userNumber;
    private String fullName;
    private String userName;
    private String displayName;
    private String firstName;
    private String lastName;
    private String email;
    private String department;
    private Language languageCode;
    private String managerName;
    private String managerUserNumber;
    private BigDecimal maximumApprovalAmount;
    private BigDecimal maximumPaymentAmount;
    private Integer kulanzId;
    private String verificatorLoginName;
    private String verificatorUserNumber;

    private List<GroupDTO> groups;

    public List<GroupDTO> getGroups() {
        return groups;
    }

    public void setGroups(List<GroupDTO> aGroups) {
        groups = aGroups;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getUserNumber() {
        return userNumber;
    }

    public void setUserNumber(String userNumber) {
        this.userNumber = userNumber;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public Language getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(Language languageCode) {
        this.languageCode = languageCode;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getManagerUserNumber() {
        return managerUserNumber;
    }

    public void setManagerUserNumber(String managerUserNumber) {
        this.managerUserNumber = managerUserNumber;
    }

    public BigDecimal getMaximumApprovalAmount() {
        return maximumApprovalAmount;
    }

    public void setMaximumApprovalAmount(BigDecimal maximumApprovalAmount) {
        this.maximumApprovalAmount = maximumApprovalAmount;
    }

    public BigDecimal getMaximumPaymentAmount() {
        return maximumPaymentAmount;
    }

    public void setMaximumPaymentAmount(BigDecimal maximumPaymentAmount) {
        this.maximumPaymentAmount = maximumPaymentAmount;
    }

    public Integer getKulanzId() {
        return kulanzId;
    }

    public void setKulanzId(Integer kulanzId) {
        this.kulanzId = kulanzId;
    }

    public String getVerificatorLoginName() {
        return verificatorLoginName;
    }

    public void setVerificatorLoginName(String verificatorLoginName) {
        this.verificatorLoginName = verificatorLoginName;
    }

    public String getVerificatorUserNumber() {
        return verificatorUserNumber;
    }

    public void setVerificatorUserNumber(String verificatorUserNumber) {
        this.verificatorUserNumber = verificatorUserNumber;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(loginName).append(loginName).toHashCode();
    }

    @Override
    public boolean equals(Object obj) {

        boolean equals = false;
        if (this == obj) {
            equals = true;
        } else if (obj == null || getClass() != obj.getClass()) {
            equals = false;
        } else {
            UserDTO other = (UserDTO) obj;
            equals =
                    new EqualsBuilder().append(this.loginName, other.loginName)
                            .append(this.userNumber, other.userNumber).isEquals();
        }
        return equals;
    }

}
