package be.dkv.arma.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * The DTO for LDAP group data.
 *
 * @author vanhout
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class AbstractARMADTO implements Serializable {

    private static final long serialVersionUID = 2684295607492869651L;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
