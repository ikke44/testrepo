package be.dkv.arma.model;

import javax.xml.bind.annotation.XmlElement;

/**
 * @author vanhout
 * 
 */
public class IdRequest extends AbstractARMADTO {

    private static final long serialVersionUID = -7306497037975421210L;

    @XmlElement(required = true)
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String aId) {
        this.id = aId;
    }

    @Override
    public String toString() {
        return id;
    }

}
