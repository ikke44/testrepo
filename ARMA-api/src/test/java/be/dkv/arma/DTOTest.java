package be.dkv.arma;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import be.dkv.arma.model.GroupDTO;

public class DTOTest {

    private static final String ARMA_ADMIN_ROLE_DESCRIPTION = "The Arma Admin Role";
    private static final String ARMA_ADMIN_ROLE_NAME = "arma.admin";

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void EqualsTest() {
        GroupDTO groupdtoEmpty = new GroupDTO();
        GroupDTO groupdtoNullRef = null;
        assertFalse(groupdtoEmpty.equals(groupdtoNullRef));

        GroupDTO groupdto1 = new GroupDTO();
        GroupDTO groupdto2 = new GroupDTO();
        groupdto1.setId(ARMA_ADMIN_ROLE_NAME);
        groupdto2.setId(ARMA_ADMIN_ROLE_NAME);
        groupdto1.setDescription(ARMA_ADMIN_ROLE_DESCRIPTION);
        assertFalse(groupdto1.equals(groupdto2));
        groupdto2.setDescription(ARMA_ADMIN_ROLE_DESCRIPTION);
        assertTrue("Failed, but should be Equal:\nFirstGroupDTO" + groupdto1.toString() + "\nSecondGroupDTO" + groupdto2.toString(),
                groupdto1.equals(groupdto2));
        
    }

}
