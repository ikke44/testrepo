#!/bin/bash
#
# Script for configuring a domain with component specific resources
#
# Usage:
#   sh configureDKVdomain.sh [domain-name]
#
# If domain name is left out, "DKV" will be used
# Will be executed in the directory of this script
# Uses Glassfish resource XML files that should also be in the same directory
# A logfile will be created in that directory afterwards: configureDKVdomain.log

DOMAIN_NAME=$1

if [ -z "$DOMAIN_NAME" ]
  then
    DOMAIN_NAME="DKV"
fi

echo "Start the ${DOMAIN_NAME} domain"  >> configureDKVdomain.log 2>&1
${GLASSFISH_HOME}/bin/asadmin start-domain ${DOMAIN_NAME} >> configureDKVdomain.log 2>&1

echo "Add the resources" >> configureDKVdomain.log 2>&1
${GLASSFISH_HOME}/bin/asadmin --user admin --passwordfile /glassfish.passwords add-resources sqlserverarma_jdbc.xml >> configureDKVdomain.log 2>&1
${GLASSFISH_HOME}/bin/asadmin --user admin --passwordfile /glassfish.passwords add-resources sqlserverremotebeans_jdbc.xml >> configureDKVdomain.log 2>&1
${GLASSFISH_HOME}/bin/asadmin --user admin --passwordfile /glassfish.passwords add-resources sqlserversecurity_jdbc.xml >> configureDKVdomain.log 2>&1

echo "Stop ${DOMAIN_NAME} domain" >> configureDKVdomain.log 2>&1
$GLASSFISH_HOME/bin/asadmin stop-domain ${DOMAIN_NAME} >> configureDKVdomain.log 2>&1

