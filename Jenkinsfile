#!/usr/bin/groovy
import groovy.json.JsonOutput
import java.util.regex.Pattern

final ARTIFACTORY_SERVER_NAME = 'dkvartifactory'
final JENKINS_MAVEN_NAME = 'Maven-3.3.9'
final BETA_TAG_SUFFIX = '-beta'
final MAVEN_SNAPSHOT_SUFFIX = '-SNAPSHOT'
final MAVEN_DEVELOP_VERSION_PATTERN = '.*0' + MAVEN_SNAPSHOT_SUFFIX
final MAVEN_DEFAULT_GOALS = 'clean install -X -DskipTests=false -DskipITs=false -Dsurefire.rerunFailingTestsCount=3'
final BITBUCKET_NOTIFICATION_URL = 'http://bitbucket:7990/rest/build-status/1.0/commits/'
final BITBUCKET_CREDENTIAL_NAME = 'BitBucket4Jenkins'
final DEPLOY_RELEASE_REPO_NAME = 'libs-release-local'
final DEPLOY_SNAPSHOT_REPO_NAME = 'libs-snapshot-local'
final RESOLVE_RELEASE_REPO_NAME = 'libs-release'
final RESOLVE_SNAPSHOT_REPO_NAME = 'libs-snapshot'

String curVersion // communicate outside main build node
String curBranch // communicate this info outside last stage to know whether to execute the 'Develop branch' stage
String curGitCommitHash // communicate the commitId to both be able to update BitBucket with the build status and use it in version labels
String curGitRepositoryUrl // communicate the URL to BitBucket to be able to find the 'develop' branch when a newly forked Release branch is encountered
GitFlowType curGitFlowType // communicate the type that was determined by parsing the Git branch name
Boolean hasSnapshots // communicate this info outside last stage to see whether to ask approval outside node block

def latestBuildInfo // communicate info from the 'Build' stage to 'Publish' stage

enum GitFlowType {
    // ALM-65 : align the names below with the BitBucket Project settings (http://bitbucket:7990/plugins/servlet/branchmodel/projects/<projcode>)
    BUGFIX(~/.*bugfix.*/),
    FEATURE(~/.*feature.*/),
    HOTFIX(~/.*hotfix.*/),
    RELEASE(~/.*release.*\/\d\.\d.*?/),
    DEVELOP(~/.*develop/),
    MASTER(~/.*master/)

    private Pattern regexp

    GitFlowType(Pattern aRegexp) {
        regexp = aRegexp
    }

    Pattern getRegexp() { return regexp }

    static GitFlowType matchBranchName(aBranchName) {
        GitFlowType result
        switch (aBranchName) {
            case GitFlowType.BUGFIX.regexp:
                result = GitFlowType.BUGFIX
                break
            case GitFlowType.FEATURE.regexp:
                result = GitFlowType.FEATURE
                break
            case GitFlowType.HOTFIX.regexp:
                result = GitFlowType.HOTFIX
                break
            case GitFlowType.RELEASE.regexp:
                result = GitFlowType.RELEASE
                break
            case GitFlowType.DEVELOP.regexp:
                result = GitFlowType.DEVELOP
                break
            case GitFlowType.MASTER.regexp:
                result = GitFlowType.MASTER
                break
            default:
                echo "The branch $aBranchName doesn't match any of the GitFlow conventions"
                result = null
                break
        }
        result
    }
}

enum BitBucketBuildStatus {
    INPROGRESS, FAILED, SUCCESSFUL
}

def toJson = {
    input ->
        groovy.json.JsonOutput.toJson(input)
}

def notifyBitBucket = { BitBucketBuildStatus status ->
    def body = [state      : status,
                key        : env.BUILD_NUMBER,
                name       : "Jenkins CI build via Jenkinsfile",
                url        : env.BUILD_URL,
                description: "Jenkinsfile build notification representing auto build triggered by Git commit and monitored by Jenkins Multibranch project"]
    httpRequest httpMode: 'POST', contentType: 'APPLICATION_JSON', requestBody: toJson(body), url: BITBUCKET_NOTIFICATION_URL + curGitCommitHash, authentication: BITBUCKET_CREDENTIAL_NAME, consoleLogResponseBody: true
    echo "Our  status $status"
}

def mavenUsingArtifactoryLib = { String goals ->
    // assumes up to date source code in workspace folder
    // updates the 'hasDependencies' property
    // return the Artifactory 'buildInfo'

    // Get Artifactory server instance, defined in the Artifactory Plugin administration page.
    def server = Artifactory.server ARTIFACTORY_SERVER_NAME
    def descriptor = Artifactory.mavenDescriptor()
    hasSnapshots = descriptor.hasSnapshots()
    def rtMaven = Artifactory.newMavenBuild()
    // Configure Maven name, defined in the Global Tools administration page.
    rtMaven.tool = JENKINS_MAVEN_NAME
    // Set Artifactory repositories for dependencies resolution and artifacts deployment.
    rtMaven.deployer releaseRepo: DEPLOY_RELEASE_REPO_NAME, snapshotRepo: DEPLOY_SNAPSHOT_REPO_NAME, server: server
    rtMaven.resolver releaseRepo: RESOLVE_RELEASE_REPO_NAME, snapshotRepo: RESOLVE_SNAPSHOT_REPO_NAME, server: server
    rtMaven.run pom: 'pom.xml', goals: goals
}

def buildAndTestUsingArtifactoryLib = {
    latestBuildInfo = mavenUsingArtifactoryLib(MAVEN_DEFAULT_GOALS)
}

def publishLastBuildToArtifactory = {
    // Get Artifactory server instance, defined in the Artifactory Plugin administration page.
    echo 'about to publish the latest build to Artifactory'
    def server = Artifactory.server ARTIFACTORY_SERVER_NAME
    server.publishBuildInfo latestBuildInfo
}

def setDevelopVersionTo = { newVersion ->
    echo "about to upgrade the Maven version on 'develop' to $newVersion"
    bat "git clean -fd"
    bat "git reset --hard"
    bat "git checkout -f develop"
    mavenUsingArtifactoryLib('versions:set -DnewVersion=' + newVersion)
    bat "git commit -am 'Upgrading Maven version develop branch to $newVersion'"
    withCredentials([usernamePassword(credentialsId: BITBUCKET_CREDENTIAL_NAME, passwordVariable: 'GIT_PASSWORD', usernameVariable: 'GIT_USERNAME')]) {
        bat "git config user.name $GIT_USERNAME"
        bat "git config user.email 'jenkins@dkv.be'"
        bat "git push http://$GIT_USERNAME:$GIT_PASSWORD@bitbucket:7990/scm/arma/arma.git" // TODO as replacement in the curGitRepository
    }
    bat "git clean -fd"
}

def setWorkspaceVersionTo = { newVersion ->
    // assume source code of fresh Release branch in workspace folder
    // NOTE stash 'workspace' being updated to reflect the commit on the feature branch after upgrading
    echo "about to upgrade version + commit changed version on 'release'"
    bat "git checkout -f $curBranch"
    mavenUsingArtifactoryLib('versions:set -DnewVersion=' + newVersion)
    bat "git commit -am 'Upgrading Maven version from $curVersion to $newVersion'"
    withCredentials([usernamePassword(credentialsId: BITBUCKET_CREDENTIAL_NAME, passwordVariable: 'GIT_PASSWORD', usernameVariable: 'GIT_USERNAME')]) {
        bat "git config user.name $GIT_USERNAME"
        bat "git config user.email 'jenkins@dkv.be'"
        bat "git push http://$GIT_USERNAME:$GIT_PASSWORD@bitbucket:7990/scm/arma/arma.git" // TODO as replacement in the curGitRepository
    }
}

def upgradeVersionsAfterReleaseBranchFork = {
    // assume source code of fresh Release branch in workspace folder
    // NOTE stash 'workspace' being updated to reflect the commit on the feature branch after upgrading
    def mavenModel = readMavenPom file: 'pom.xml'
    if (mavenModel.version =~ /\d\.\d\.\d\.\d.*?/) {
        mavenUsingArtifactoryLib('build-helper:parse-version versions:set -DnewVersion=${parsedVersion.majorVersion}.${parsedVersion.minorVersion}.${parsedVersion.incrementalVersion}.1' + MAVEN_SNAPSHOT_SUFFIX)
        mavenModel = readMavenPom file: 'pom.xml'
        setWorkspaceVersionTo(mavenModel.version)

        mavenUsingArtifactoryLib('build-helper:parse-version versions:set -DnewVersion=${parsedVersion.majorVersion}.${parsedVersion.minorVersion}.${parsedVersion.nextIncrementalVersion}.0' + MAVEN_SNAPSHOT_SUFFIX)
        mavenModel = readMavenPom file: 'pom.xml'
        setDevelopVersionTo(mavenModel.version)
    } else {
        mavenUsingArtifactoryLib('build-helper:parse-version versions:set -DnewVersion=${parsedVersion.majorVersion}.${parsedVersion.minorVersion}.1' + MAVEN_SNAPSHOT_SUFFIX)
        mavenModel = readMavenPom file: 'pom.xml'
        setWorkspaceVersionTo(mavenModel.version)

        mavenUsingArtifactoryLib('build-helper:parse-version versions:set -DnewVersion=${parsedVersion.majorVersion}.${parsedVersion.nextMinorVersion}.0' + MAVEN_SNAPSHOT_SUFFIX)
        mavenModel = readMavenPom file: 'pom.xml'
        setDevelopVersionTo(mavenModel.version)
    }
}

def publishBetaVersion = {
    stage('Publish BETA version') {
        timeout(time: 4, unit: 'HOURS') {
            def approvalMsg = "This build of 'develop' conforms to the Maven and GitFlow conventions so it can be published as BETA version. \n"\
                           + 'Do you want to make it available as non-SNAPSHOT version (a BETA version in this case)? '
            input message: approvalMsg,
                    ok: "Create version BETA version"
        }
        node {
            unstash 'workspace'

            // create a non-SNAPSHOT version that gets precedence on SNAPSHOTS, isn't a release and contains Git commit hash
            def rcVersion = curVersion.replace(MAVEN_SNAPSHOT_SUFFIX, "")
            // we want a valid Maven version 'Number', no 'String' treatement, see <MajorVersion [> . <MinorVersion [> . <IncrementalVersion ] ] [> - <BuildNumber | Qualifier ]>
            rcVersion += BETA_TAG_SUFFIX
            rcVersion += (new Date()).format("yyyyMMdd-HHmmssSSS", TimeZone.getTimeZone('GMT+02:00'))
            rcVersion += '-' + curGitCommitHash
            echo "RC version $rcVersion"

            def descriptor = Artifactory.mavenDescriptor()
            descriptor.version = rcVersion
            descriptor.transform()

            buildAndTestUsingArtifactoryLib()
            publishLastBuildToArtifactory()
        }
    }
}

def publishNewVersion = {
    stage('Publish new (non-SNAPSHOT) version and increment SNAPSHOT') {
        timeout(time: 4, unit: 'HOURS') {
            def approvalMsg = "This Release Candidate conforms to the Maven and GitFlow conventions so it can be published. \n"
                        + 'Do you want to make it available as non-SNAPSHOT version? '
            input message: approvalMsg,
                    ok: "Create version RC version"
        }
        node {
            unstash 'workspace'

            // create an RC version
            def rcVersion = curVersion.replace(MAVEN_SNAPSHOT_SUFFIX, "")
            echo "RC version $rcVersion"

            def descriptor = Artifactory.mavenDescriptor()
            descriptor.version = rcVersion
            descriptor.transform()

            buildAndTestUsingArtifactoryLib()
            publishLastBuildToArtifactory()
            bat "git commit -am 'Finishing Maven version $rcVersion'"
            withCredentials([usernamePassword(credentialsId: BITBUCKET_CREDENTIAL_NAME, passwordVariable: 'GIT_PASSWORD', usernameVariable: 'GIT_USERNAME')]) {
                bat "git config user.name $GIT_USERNAME"
                bat "git config user.email 'jenkins@dkv.be'"
                bat "git tag -a v$rcVersion -m 'Version rcVersion '"
            }

            // set to the next SNAPSHOT version
            if (mavenModel.version =~ /\d\.\d\.\d\.\d.*?/) {
                mavenUsingArtifactoryLib('build-helper:parse-version versions:set -DnewVersion=${parsedVersion.majorVersion}.${parsedVersion.minorVersion}.${parsedVersion.incrementalVersion}.${parsedVersion.nextBuildNumber}' + MAVEN_SNAPSHOT_SUFFIX)
                mavenModel = readMavenPom file: 'pom.xml'
                setWorkspaceVersionTo(mavenModel.version)
            } else {
                mavenUsingArtifactoryLib('build-helper:parse-version versions:set -DnewVersion=${parsedVersion.majorVersion}.${parsedVersion.minorVersion}.${parsedVersion.nextIncrementalVersion}' + MAVEN_SNAPSHOT_SUFFIX)
                mavenModel = readMavenPom file: 'pom.xml'
                setWorkspaceVersionTo(mavenModel.version)
            }
            bat "git commit -am 'Set next SNAPSHOT version'"

            // push the commits
            withCredentials([usernamePassword(credentialsId: BITBUCKET_CREDENTIAL_NAME, passwordVariable: 'GIT_PASSWORD', usernameVariable: 'GIT_USERNAME')]) {
                bat "git config user.name $GIT_USERNAME"
                bat "git config user.email 'jenkins@dkv.be'"
                bat "git push http://$GIT_USERNAME:$GIT_PASSWORD@bitbucket:7990/scm/arma/arma.git" // TODO as replacement in the curGitRepository
            }
            // the next build will be triggered and will result in publishing a next SNAPSHOT version in Artifactory
        }
    }
}



node {
    try {
        stage('Get sources') {
            def scmVariables = checkout scm
            curGitCommitHash = scmVariables.GIT_COMMIT
            curGitRepositoryUrl = scmVariables.GIT_URL
            curBranch = (scm.branches =~ /\[(.*)\]/)[0][1]
            stash name: 'workspace'
        }
        stage('Notify BitBucket about this build') {
            notifyBitBucket(BitBucketBuildStatus.INPROGRESS)
        }
        stage('Check GitFlow conventions') {
            unstash 'workspace'
            curGitFlowType = GitFlowType.matchBranchName(curBranch) ?: error("Branch name is not according to GitFlow conventions!")
            echo "Detected a valid GitFlow type: $curGitFlowType"
            def mavenModel = readMavenPom file: 'pom.xml'
            curVersion = mavenModel.version
            if (!(curVersion ==~ (".*" + MAVEN_SNAPSHOT_SUFFIX))) {
                error("I will not (re-)build non-SNAPSHOT source code: " + curVersion)
            }
            switch (curGitFlowType) {
                case GitFlowType.FEATURE:
                    // same checks as for 'develop'
                case GitFlowType.DEVELOP:
                    if (!(curVersion ==~ MAVEN_DEVELOP_VERSION_PATTERN)) {
                        error "$curVersion is not a valid version for a FEATURE or the DEVELOP branch, it should match $MAVEN_DEVELOP_VERSION_PATTERN: A SNAPSHOT version that ends with a 0 as the digits are reserved for (SNAPSHOT's of) release candidates."
                    }
                    break
                case GitFlowType.RELEASE:
                    if (env.BUILD_NUMBER == '1') {
                        echo 'Detected a first build on RC branch: upgrade Maven versions in both DEVELOP and this branch to align them with the GitFlow conventions'
                        upgradeVersionsAfterReleaseBranchFork()
                        error 'Fixed the version number on this branch, look at the next build to see the result of building/testing the code on this new release branch.'
                    }
                    break
                case GitFlowType.BUGFIX:
                    break
                case GitFlowType.HOTFIX:
                    break
                case GitFlowType.MASTER:
                    break
                default:
                    error("Unrecognized branch according to our GitFlow rules, we should never arrive here...")
            }
        }
        stage('Build & Test') {
            unstash 'workspace'
            buildAndTestUsingArtifactoryLib()
            stash name: 'workspace'
        }
        stage('Reporting') {
            unstash 'workspace'
            def mavenModel = readMavenPom file: 'pom.xml'
            echo "Modules = $mavenModel.modules"
            for (module in mavenModel.modules) {
                echo "Reporting for module $module"
                echo "Surefire test reports present = ${fileExists(module + '/target/surefire-reports')}"
                if (fileExists(module + '/target/surefire-reports')) {
                    junit testResults: module + '/target/surefire-reports/*.xml', healthScaleFactor: 10d
                }
                echo "Failsafe test reports present = " + fileExists(module + '/target/failsafe-reports')
                if (fileExists(module + '/target/failsafe-reports')) {
                    junit testResults: module + '/target/failsafe-reports/*.xml', healthScaleFactor: 10d, allowEmptyResults: true
                }
                archiveArtifacts artifacts: module + '/target/**/*', allowEmptyArchive: true
            }
        }
        if ((curGitFlowType != GitFlowType.FEATURE) && (curGitFlowType != GitFlowType.BUGFIX)) {
            stage('Publish to Artifactory') {
                publishLastBuildToArtifactory()
            }
        }
    } catch (err) {
        notifyBitBucket(BitBucketBuildStatus.FAILED)
        throw err
    } finally {
        step([$class                  : 'Mailer',
              notifyEveryUnstableBuild: true,
              sendToIndividuals       : true,
              recipients              : emailextrecipients([[$class: 'CulpritsRecipientProvider'],
                                                            [$class: 'RequesterRecipientProvider']])])
    }
    stage('Communicate OK build to BitBucket') {
        notifyBitBucket(BitBucketBuildStatus.SUCCESSFUL)
    }
}

// AfterCare:
// look to publish non-SNAPSHOT version or do other GitFlow branch type specific things
// NOTE that the following is outside a 'node' block (and should stay so!), the respective action do allocate a node themselves
// This follows the Best Practices (see https://www.cloudbees.com/blog/top-10-best-practices-jenkins-pipeline-plugin, esp. 7 and 8 and uses 'stashing' (10))
switch (curGitFlowType) {
    case GitFlowType.FEATURE:
        break
    case GitFlowType.DEVELOP:
        publishBetaVersion()
        break
    case GitFlowType.BUGFIX:
        break
    case GitFlowType.RELEASE:
        // both 'feature' and 'hotfix' branches can result in new RC versions
    case GitFlowType.HOTFIX:
        if (!hasSnapshots) {
            publishNewVersion()
        }
        break
    case GitFlowType.MASTER:
        break
    default:
        error("Unrecognized branch according to our GitFlow rules, we should never arrive here...")
}
